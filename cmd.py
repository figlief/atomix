#!/usr/bin/python

import sys
from myatomix import atomix
from bunch import Bunch
static_paths = (
    "css",
    "js",
    "xjs",
    "images",
    "levels"
)
sub = "/submit-solution"
form = Bunch(
    levelSet="katomic",
    level="1",
    history="cccbcbfbfbfefebebebjfhfgfgbgbgbihbhfhfgfgfgjgjfjfjfgfgbgbgbh"
)


def main():
    url = sys.argv[1]
    url = url or sub
    page = atomix.main(url, form)
    headers, content = page.compose()

    print page.httpStatus

    with open('x.html', 'wb') as fp:
        fp.write(content.encode('utf-8'))
if __name__ == "__main__":
    main()
