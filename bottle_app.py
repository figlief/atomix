#!/usr/bin/python

from bottle import (
    Bottle,
    response,
    request,
    #  redirect,
    debug,
    static_file
)
from myatomix import atomix
debug(1)

app = Bottle()


@app.route('/hello')
def hello():
    return (
        "Hello World!<br />",
        str({k: v for k, v in request.query.items()})
    )

static_paths = (
    "css",
    "js",
    "xjs",
    "images",
    "levels"
)


@app.route('/')
def index_page():
    return '<h1><a href="/atomix/">Atomix</a></h1>'


@app.route('/atomix/<url:path>')
@app.route('/atomix/')
def atomix_app(url=''):
    apath = url.split('/')
    if len(apath) > 1:
        d = apath[0]
        if d in static_paths:
            return static_file(url, root='./')
    params = {k: v for k, v in request.query.items()}
    page = atomix.main('/' + url, form=params)
    headers, content = page.compose()
    response.content_type = page.contentType
    for k, v in headers:
        response.set_header(k, v)

    return "%s" % content
