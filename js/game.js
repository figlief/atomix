
KP_ATOMIX.Game = function (image_path) {

  var log = function() {}

    , item_kind = {
      '1': 'atom-h', // hydrogen
      '2': 'atom-c', // carbon
      '3': 'atom-o', // oxygen
      '4': 'atom-n', // nitrogen
      '5': 'atom-s', // sulphur
      '6': 'atom-f', // fluorine
      '7': 'atom-cl', // chlorine
      '8': 'atom-br', // bromine
      '9': 'atom-p', // phosphorus
      'o': 'atom-crystal',
      'A': 'connector-horizontal',
      'B': 'connector-slash',
      'C': 'connector-vertical',
      'D': 'connector-backslash',

      'E': 'crystal-E',
      'F': 'crystal-F',
      'G': 'crystal-G',
      'H': 'crystal-H',
      'I': 'crystal-I',
      'J': 'crystal-J',
      'K': 'crystal-K',
      'L': 'crystal-L',
      'M': 'crystal-M'
    },
    bond_kind = {
      'a': 'bond-top',
      'b': 'bond-top-right',
      'c': 'bond-right',
      'd': 'bond-bottom-right',
      'e': 'bond-bottom',
      'f': 'bond-bottom-left',
      'g': 'bond-left',
      'h': 'bond-top-left',
      'A': 'bond-top-double',
      'B': 'bond-right-double',
      'C': 'bond-bottom-double',
      'D': 'bond-left-double',
      'E': 'bond-top-triple',
      'F': 'bond-right-triple',
      'G': 'bond-bottom-triple',
      'H': 'bond-left-triple',
      '1': 'bond-top-left-double',
      '2': 'bond-top-right-double',
      '3': 'bond-bottom-right-double',
      '4': 'bond-bottom-left-double'
    },

    moveEncoder = 'abcdefghijklmnopqrstuvwxyz'.split(''),
    moveDecoder = {},

    gMoveFlag = false,
    gCurrent = null,
    gItems = null,
    gArrows = null,
    sArrows = ['arrow-left', 'arrow-right', 'arrow-up', 'arrow-down'],

    gAnimationTime = 100,
    gA = null,
    gM = null,

    gCellWidth = 41,
    gCellHeight = 39,
    gMoleculeCellWidth = 41,
    gMoleculeCellHeight = 39,

    idArena = '',
    idMol = '',
    gLevel = null,

    noEvents = false,

    self = Object.create(KP_ATOMIX.GameClass),

    $ = xGetElementById
  ;

    function init(arena, mol, level, history) {

        self.idArena = idArena = arena;
        self.idMol = idMol = mol;

        self.onInit(arena, mol, level, history||null);

        gLevel = level;
        init_data(history);
        reset_level(level, self.gg);

    }

    function init_data(history) {

        var gg = {};

        self.gg = gg;

        gg.grid = copy_grid(gLevel.arena);

        gg.molecule = gLevel.molecule.join(
            gg.grid[0].replace(/./g, '.').substring(gLevel.molecule[0].length)
        );
        gg.history = [];
        gg.redo = [];

        if (history) {
          history = decode_history(history);
          replay_moves(history, gg.grid);
        }
        gg.history = history || [];

    }

    function reset_level(level, data) {

        gLevel = level;

        if (data) {
            self.gg = data;
        } else {
            init_data();
        }

        gItems = [];
        gArrows = [];

        gA = gridSpec(idArena, gCellWidth, gCellHeight);
        gA.clear_container();

        if (idMol && $(idMol)) {
          gM = gridSpec(idMol,
              gMoleculeCellWidth, gMoleculeCellHeight
          );
          gM.clear_container();
        }

        gCurrent = null;

        draw_arena();
        show_arrows();

        self.onShowData(self.gg);

    }

    function foreach(c, f) {
        for (var i = 0; i < c.length; i += 1) {
            f(c[i], i, c);
        }
    }

    function copy_grid(grid) {
        return grid.join('\n').split('\n');
    }

    function end_animation(testForSuccess) {

        self.onEndAnimation(gCurrent);
        gMoveFlag = false;
        if (testForSuccess) {
            test_for_success();
        } else {
            show_arrows();
        }
    }

    function show_arrow(arrow, row, col) {
        if (self.gg.grid[row].charAt(col) === '.') {
            xMoveTo(arrow, gA.xpos(col), gA.ypos(row));
        } else {
            xLeft(arrow, -1000);
        }
    }

    function show_arrows() {

        var row = gCurrent.row, col = gCurrent.col;

        show_arrow(gArrows[0], row, col - 1);
        show_arrow(gArrows[1], row, col + 1);
        show_arrow(gArrows[2], row - 1, col);
        show_arrow(gArrows[3], row + 1, col);
    }

    function hide_arrows() {
        foreach(gArrows, function (a, i) {
            xLeft(a, -1000);
        });
    }

    function create_arrows() {
        var arrows = ''
          , idArrow
        ;
        foreach(sArrows, function (dir) {
            idArrow = dir + '`' + idArena;
            arrows += gA.new_img('arrow',  dir, -1, idArrow);
        });
        return arrows;
    }

    function get_item_rowcol(row, col) {
        var i, item;
        for (i = 0; i < gItems.length; i += 1) {
            item = gItems[i];
            if (item.row === row && item.col === col) {
                return item;
            }
        }
        return null;
    }

    function set_current(oAtom) {
        gCurrent = oAtom;
        show_arrows(oAtom);
    }

    function encode_history(history) {
        var i, m, e, s = '';
        e = moveEncoder;
        for (i = 0; i < history.length; i += 1) {
            m = history[i];
            s = s + e[m[0]] + e[m[1]] + e[m[2]] + e[m[3]];
        }
        return s;
    }
    function decode_history(s) {
        var i, d, history = [];
        d = moveDecoder;
        for (i = 0; i < s.length;) {
            history.push([d[s[i++]], d[s[i++]], d[s[i++]], d[s[i++]]]);
        }
        return history;
    }

    function create_move_decoder(i) {
        foreach(moveEncoder, function (s) {
            moveDecoder[s] = i++;
        });
    }
    create_move_decoder(0);


    // Modify a grid to reflect a sequence of moves.
    // Throws an Error if the move sequence is invalid.
    // Returns the modified grid otherwise.

    var InvalidMoveError = function (msg) {
        this.prototype = Error.prototype;
        this.name = "InvalidMoveError";
        this.message = "Invalid Move: " + msg + '.';
    };

    function _assert(p, msg) {
        if (p) {
            return;
        }
        throw new InvalidMoveError(msg);
    }
    function replay_moves(moveList, grid) {

        var endCol
          , endRow
          , startCol
          , startRow
          , row
          , col
          , data
          , atom
          , move
          , moveIdx
        ;

        for (moveIdx = 0; moveIdx < moveList.length; moveIdx += 1) {

            move = moveList[moveIdx];
            startRow = row = move[0];
            startCol = col = move[1];
            endRow = move[2];
            endCol = move[3];

            atom = grid[startRow][startCol];
            _assert(atom !==  '.' && atom !== '#', 'no atom here');

            data = grid[row];

            if (startRow === endRow) {
                _assert(startCol !== endCol, 'no movement');
                if (endCol < startCol) {
                    while (data[col - 1] === '.') col -= 1;
                } else {
                    while (data[col + 1] === '.') col += 1;
                }
            } else {
                _assert(startCol === endCol, 'diagonal move');
                if (endRow < startRow) {
                    while (grid[row - 1][col] === '.') row -= 1;
                } else {
                    while (grid[row + 1][col] === '.') row += 1;
                }
            }
            _assert(row === endRow && col === endCol, 'path blocked');
            move_in_grid(grid, startRow, startCol, endRow, endCol);
        }
        return grid;
    }

    function isSolution(testGrid) {
        var grid = testGrid.join('').replace(/#/g, '.');
        return (grid.indexOf(self.gg.molecule) !== -1);
    }

    function onClickAtom(oAtom) {
        set_current(oAtom);
    }

    function onClickArrow(dir) {

        if (gMoveFlag || self.onArrowClicked(gCurrent, dir.split('-')[1]) ) {
            return;
        }

        var row = gCurrent.row
          , col = gCurrent.col
          , cr = row
          , cc = col
          , grid = self.gg.grid
          , data = grid[row]
        ;

        if (!xStr(dir)) {
          dir = $(dir + '`' + idArena).id;
        }

        switch (dir) {

        case 'arrow-left':
            while (data.charAt(col - 1) === '.') {
                col -= 1;
            }
            break;
        case 'arrow-right':
            while (data.charAt(col + 1) === '.') {
                col += 1;
            }
            break;
        case 'arrow-up':
            while (grid[row - 1].charAt(col) === '.') {
                row -= 1;
            }
            break;
        case 'arrow-down':
            while (grid[row + 1].charAt(col) === '.') {
                row += 1;
            }
            break;

        default:
            break;
        }

        if (row !== gCurrent.row || col !== gCurrent.col) {
            self.gg.history.push([cr, cc, row, col]);
            self.gg.redo = [];
            move_current_atom(row, col, true);
        }
    }

    function move_in_grid(grid, sr, sc, er, ec) {
        var atom = grid[sr].charAt(sc);
        grid[sr] = grid[sr].slice(0, sc) + '.' + grid[sr].slice(sc + 1);
        grid[er] = grid[er].slice(0, ec) + atom + grid[er].slice(ec + 1);
    }

    function move_current_atom(row, col, testForSuccess, repeat) {

        var grid = self.gg.grid
          , startCol = gCurrent.col
          , startRow = gCurrent.row
          , animationTime
        ;

        gMoveFlag = true;

        hide_arrows();

        move_in_grid(grid, startRow, startCol, row, col);
        gCurrent.row = row;
        gCurrent.col = col;

        self.onShowData(self.gg);

        animationTime = gAnimationTime * Math.abs(startRow - row + startCol - col);

        if (repeat) {
            xAniLine(gCurrent.atom,
                gA.xpos(col),
                gA.ypos(row),
                10, 1,
                function () {
                    end_animation(false);
                    setTimeout(repeat, 10);
                }
            );
        }
        else {
            xAniLine(gCurrent.atom,
                gA.xpos(col),
                gA.ypos(row),
                animationTime, 1,
                function () {
                    end_animation(testForSuccess);
                }
            );
        }
    }

    function gridSpec(parentName, cellWidth, cellHeight) {

        var parent = $(parentName)
            , atomCount = -1
            , wh = 'width:' + cellWidth + 'px;height:' + cellHeight + 'px;'
        ;


        function xpos(col) {
            return col * cellWidth;
        }

        function ypos(row) {
            return row * cellHeight;
        }

        function new_img(cls, image, col, row) {

            var id;

            if (col < 0) {
                id = row ? ' id="' + row + '"' : '';
                col = 0;
                row = 0;
            } else {
                id = '';
                col = xpos(col || 0);
                row = ypos(row || 0);
            }

            return '<img'
                + id +  ' src="'
                + image_path
                + image  + '.png" class="'
                + cls + '" style="left:'
                + col + 'px;top:'
                + row  + 'px;'
                + wh  + '" />'
            ;
        }

        function atom_factory(atom_type, col, row) {

            var spec = gLevel.atoms[atom_type]
              , sAtom
              , bonds
              , bond
            ;

            atomCount += 1;
            sAtom = '<div id="atom-'
                + parentName + '-' + atomCount
                + '" class="atom" style="left:'
                + xpos(col) + 'px;top:'
                + ypos(row) + 'px;'
                + wh + '">';

            bonds = spec[1];
            for (bond = 0; bond < bonds.length; bond += 1) {
                sAtom += new_img('bond', bond_kind[bonds.charAt(bond)], -1);
            }
            sAtom += new_img('atom', item_kind[spec[0]], -1);

            return sAtom + '</div>';
        }

        function set_container_size(col, row) {
            xResizeTo(parent,
                col * cellWidth,
                row * cellHeight
            );
        }

        function clear_container() {
            parent.innerHTML = '&nbsp;';
        }

        return {
            parent: parent,
            xpos: xpos,
            ypos: ypos,
            cellWidth: cellWidth,
            cellHeight: cellHeight,
            atom_factory: atom_factory,
            new_img: new_img,
            set_container_size: set_container_size,
            clear_container: clear_container
        };
    }

    function onAtomSelected(e, target, data) {
        if (!gMoveFlag && !self.onAtomSelected(data)){
            target(data);
        }
    }

    function addClickAtom(e, target, oAtom) {

        if (noEvents) return;

        function onAtomSelected() {
            if (!gMoveFlag && !self.onAtomSelected(oAtom)){
                set_current(oAtom);
                xCancel();
            }
        }

        xAddEventListener(e, 'mouseover', onAtomSelected, false);
        xAddEventListener(e, 'click', onAtomSelected, false);
    }

    function addClickArrow(e, target, data) {
        if (noEvents) return;
        xAddEventListener(e, 'mousedown', function (evt) {
            target(data);
        }, false);
    }

    function draw_arena() {

        var item
          , mol
          , row
          , col
          , sArena = []
          , sMolecule = []
          , grid = self.gg.grid
        ;

        sArena.push(create_arrows());

        for (row = 0 ; row < grid.length; row += 1) {
            item = grid[row];
            for (col = 0; col < item.length; col += 1) {

                switch (item.charAt(col)) {
                case '#':
                    sArena.push(gA.new_img('wall', 'wall', col, row));
                    break;
                case '.':
                    break;
                default:
                    sArena.push(gA.atom_factory(item.charAt(col), col, row));
                    gItems.push({'row': row, 'col': col});
                }
            }
        }
        gA.parent.innerHTML = sArena.join('');
        gA.set_container_size(col, row);

        foreach(gItems, function (oAtom, i) {
            oAtom.atom = $('atom-' + idArena + '-' + i);
            addClickAtom(oAtom.atom, onClickAtom, oAtom);
        });

        gArrows = [];
        foreach(sArrows, function (arrow) {
            oArrow = $(arrow + '`' + idArena);
            gArrows.push(oArrow);
            addClickArrow(oArrow, onClickArrow, arrow);
        });

        if (idMol) {
          mol = gLevel.molecule;
          for (row = 0 ; row < mol.length; row += 1) {
              item = mol[row];
              for (col = 0; col < item.length; col += 1) {
                  if (item.charAt(col) !== '.') {
                      sMolecule.push(gM.atom_factory(item.charAt(col), col, row));
                 }
             }
          }
          gM.parent.innerHTML = sMolecule.join('');
          gM.set_container_size(col, row);
        }
        set_current(gItems[0]);
        show_arrows();

    }

    function do_history_undo(repeat) {

        var gg = self.gg;

        if (!gg.history.length || gMoveFlag) {
            return;
        }
        var m = gg.history.pop();
        self.gg.redo.push(m);
        //m = decode_move(m);
        gCurrent = get_item_rowcol(m[2], m[3]);
        move_current_atom(m[0], m[1], false, repeat);
    }

    function do_history_redo(repeat) {

        var gg = self.gg;

        if (!gg.redo.length || gMoveFlag) {
            return;
        }
        var m = gg.redo.pop();
        gg.history.push(m);
        gCurrent = get_item_rowcol(m[0], m[1]);
        move_current_atom(m[2], m[3], false, repeat);
    }

    function do_history_first() {
        if (!self.gg.history.length || gMoveFlag) {
            return;
        }
        do_history_undo(do_history_first);
    }

    function do_history_last() {

        if (!self.gg.redo.length || gMoveFlag) {
            return;
        }
        do_history_redo(do_history_last);
    }

    function test_for_success() {
        if (isSolution(self.gg.grid)) {
            self.onCompleteLevel();
        }
        else {
            show_arrows();
        }

    }
    self.init = init;
    self.reset_level = reset_level;

    self.getEncodedHistory = function () {
        return encode_history(self.gg.history);
    };

    self.historyUndo = do_history_undo;
    self.historyRedo = do_history_redo;
    self.historyFirst = do_history_first;
    self.historyLast = do_history_last;

    self.setCellSize = function(width, height) {
      gCellWidth = width;
      gCellHeight = height;
    };
    self.noEvents= function() {noEvents = true};

    self.hideArrows = hide_arrows;

    self.log = log;

    return self;

};

KP_ATOMIX.GameClass = {

    onInit: function(arena, mol, level, history) {
        //this.log('atomix.onInit %s, %s, %o, %o', this.idArena, this.idMol, level||null, history||null);
    },

    onAtomSelected: function(oAtom) {
        //this.log('atomix.atomSelected(%s): %o', this.idArena, oAtom);
    },

    onArrowClicked: function(oAtom, direction) {
        //this.log('atomix.arrowClicked(%s): %o, %s', this.idArena, oAtom, direction);
    },

    onCompleteLevel:  function() {
        //this.log('atomix.onCompleteLevel(%s): ', this.idArena);
    },

    onEndAnimation: function(oAtom) {
        //this.log('atomix.endAnimation(%s): %o', this.idArena, oAtom);
    },

    onShowData: function(gg) {
        //this.log('atomix.showData(%s): %o', this.idArena, gg);
    }
};
