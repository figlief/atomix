if(!KP_ATOMIX){var KP_ATOMIX={}}

(function () {

    var CELL_HEIGHT = 39,
        CELL_WIDTH = 41,

        gCellHeight = CELL_HEIGHT,
        gCellWidth = CELL_WIDTH,

        MOLECULE_CELL_HEIGHT = 39,
        MOLECULE_CELL_WIDTH = 41,

        gMoleculeCellHeight = MOLECULE_CELL_HEIGHT,
        gMoleculeCellWidth = MOLECULE_CELL_WIDTH,

        // time for atom to move one square (in milliseconds)
        ANIMATION_TIME = 100,
        gAnimationTime = ANIMATION_TIME,

        browserTitle = 'kpAtomix Online',
        gControls = null,
        gUserName = 'anonymous',
        gAjaxRequest,

        gLevelSets = {}, // list of LevelSet objects derived from KP_ATOMIX.levelSets
        gLevelSetNames = [],
        gLevelSet = null,  // currently active LevelSet object
        gGame = null,

        dlgSuccess,
        dlgBookmark,
        dlgAjax,
        log = function() {},

        gameList = [],

        site_prefix = '',

        //
        $;

    function foreach(c, f) {
        for (var i = 0; i < c.length; i += 1) {
            f(c[i], i, c);
        }
    }
    function forkey(c, f) {
        for (var k in c) {
            if (c.hasOwnProperty(k)) {
                f(c[k], k, c);
            }
        }
    }
    function keys(c) {
        var k, a = [];
        for (k in c) {
            if (c.hasOwnProperty(k)) {
                a.push(k);
            }
        }
        return a;
    }
    function format(s) {
        var count = 0, args = arguments;
        return s.replace(/\f/g, function () {
            count += 1;
            return (count < args.length) ? args[count] : '';
        });
    }

    function parse_location_search() {

        var items = location.search
            , result = {}
        ;

        if (!(items.length)) {
            return result;
        }
        foreach(items.substring(1).split('&'), function (item) {
            var value = item.split('=', 2)
                , name = decodeURIComponent(value[0])
            ;
            if (value.length === 2) {
                value = decodeURIComponent(value[1]);
            } else {
                value = '';
            }
            result[name] = value;
        });
        return result;
    }

    function addClickLink(cmd) {
        xAddEventListener($(cmd), 'click', function (e) {
            xCancel(e);
            gControls[cmd]();
        }, false);
        xAddEventListener($(cmd), 'dblclick', function (e) {
            xCancel(e);
            gControls[cmd]();
        }, false);
    }

    function onNextAtom(dir) {

        switch (dir) {

        case 'down':
            break;
        case 'up':
            break;
        case 'left':
            break;
        case 'right':
            break;
        }
    }

    function onKeydown(evt)
    {
        var e = new xEvent(evt),
            doneKey = true;

        if (kp_Shield.isUp()) {
            return;
        }

        function dok(a, b) {
            if (e.shiftKey && b) {
                gControls[b]();
            }
            else {
                gControls[a]();
            }
        }

        function doa(d) {
            if (e.shiftKey) {
                //onNextAtom(d);
            } else {
                onClickArrow('arrow-' + d);
            }
        }

        switch (e.keyCode) {

            case 85: //u
                dok('history-undo', 'history-redo');
                break;
            case 82: //r
                dok('history-redo', '');
                break;
            case 78: //n
                dok('next-level', '');
                break;
            case 80: //p
                dok('prev-level', '');
                break;
            case 66: //b
                dok('bigger-link', '');
                break;
            case 83: //s
                dok('smaller-link', 'bookmark-link');
                break;

            case 74: //j
                doa('left');
                break;
            case 75: //k
                doa('down');
                break;
            case 76:  //l
                doa('right');
                break;
            case 73:  //i
                doa('up');
                break;

            default:
               doneKey = false;
        }
        if (doneKey) {
            xPreventDefault(evt);
        }
    }

    function create_levelset_selectors() {

        $('levelset-selector-span').innerHTML  =
              '<select id="levelset-select"><option>'
            + gLevelSetNames.join('</option><option>')
            + '</option></select>';

        xAddEventListener($('levelset-select'), 'change', function () {
           setTimeout(onLevelSetSelect, 100);
        }, false);
        return;
    }
    function onLevelSetSelect() {

        var sel = $('levelset-select')
          , name = sel.options[sel.selectedIndex].text
        ;
        select_levelSet(name);
        start_level(gLevelSet.iLevel);
    }

    function create_query_string() {
        var sData = []
            , enc = encodeURIComponent
            , user
        ;
        function add(key, data) {
            if (data.length) {
                sData.push(enc(key) + '=' + enc(data));
            }
        }
        user = dlgSuccess.get('user').value;
        user = (user.toLowerCase().indexOf('anon') === 0) ? '' : user;

        add('user', user);
        add('levelSet', gLevelSet.name);
        add('level', '' + gLevelSet.level.id);
        add('history', gGame.getEncodedHistory());
        //add('undo', encode_history(gg.redo));

        return sData.join('&');
    }

    function update_bookmark_link() {

        var bm = $('bookmark-link')
            , href
        ;
        href = location.protocol + '//' + location.host + location.pathname;
        href += '?' + create_query_string();
        document.title = format('\f \f: \f', gLevelSet.name, gLevelSet.iLevel + 1, browserTitle);
        bm.href = href;
    }

    function send_solution_home(fnResponse) {
        // a solution will be saved via ajax

        var sData, response;
        sData = create_query_string();

        response = gAjaxRequest.send(
            'GET',
            site_prefix + 'submit-solution',
            sData,
            60000, // uTimeout milliseconds
            '', // sData +  '&' + sRndVar + '=' + a_random_number.
            false, // bXml
            null, // a user data object
            function (req, status, data) {
                var text = req.responseText;
                //alert(text);
                fnResponse(text, status);
            }
        );
    }

    function on_complete_level() {
        show_success_dialog(
            format(
                "You completed this level <br /> in <b>\f</b> moves.",
                gGame.gg.history.length
            )
        );
    }

    function setup_controls() {
        gControls = {
            'next-level': do_next_level
          , 'prev-level': do_prev_level
          , 'history-reset': do_history_reset
          , 'history-undo': do_history_undo
          , 'history-redo': do_history_redo
          , 'history-first': do_history_first
          , 'history-last': do_history_last
          , 'bookmark-link': do_bookmark_link

          , 'bigger-link': do_bigger_link
          , 'smaller-link': do_smaller_link
        };
        foreach(keys(gControls), addClickLink);
    }

        function do_bigger_link() {
            gCellHeight += 2;
            gCellWidth += 2;
            gMoleculeCellHeight += 2;
            gMoleculeCellWidth += 2;
            start_level(gLevelSet.iLevel);
        }

        function do_smaller_link() {
            gCellHeight = Math.max(6, gCellHeight - 2);
            gCellWidth = Math.max(6, gCellHeight - 2);
            gMoleculeCellHeight = Math.max(6, gMoleculeCellHeight - 2);
            gMoleculeCellWidth = Math.max(6, gMoleculeCellWidth -2);
            start_level(gLevelSet.iLevel);
        }

        function do_bookmark_link() {
            show_bookmark_dialog();
        }

        function do_next_level() {
            var l = gLevelSet.levels.length - 1;
            if (gLevelSet.iLevel < l) {
                $('level-select-' + gLevelSet.name).selectedIndex = gLevelSet.iLevel + 1;
                start_level(gLevelSet.iLevel + 1);
            }
        }

        function do_prev_level() {
            if (gLevelSet.iLevel > 0) {
                $('level-select-' + gLevelSet.name).selectedIndex = gLevelSet.iLevel - 1;
                start_level(gLevelSet.iLevel - 1);
            }
        }

        function do_history_reset() {
            start_level(gLevelSet.iLevel, true);
        }

        function do_history_undo(repeat) {
            gGame.historyUndo();
        }
        function do_history_redo(repeat) {
            gGame.historyRedo();
        }

        function do_history_first() {
            gGame.historyFirst();
        }

        function do_history_last() {
            gGame.historyLast();
        }

    function show_level_data() {
        gLevelSet.level.gameData = gGame.gg;
        $('move-no').innerHTML = format(
            '<b>(Move: \f )</b>',
            gGame.gg.history.length
        );
        update_bookmark_link();
    }

    function select_levelSet(name, setControl) {

        var i, select, options;

        if (setControl === true) {
            select = $('levelset-select');
            options = select.options;
            for (i = 0; i < options.length; i++) {
                if (options[i].text === name) {
                    select.selectedIndex = i;
                    break;
                }
            }
        }

        if (!(name in gLevelSets)) {
            name = gLevelSetNames[0];
            $('levelset-select').selectedIndex = 0;
        }
        gLevelSet = gLevelSets[name];

        $('levelset-credits-credit').innerHTML = gLevelSet.credits;
        $('levelset-credits-license').innerHTML = gLevelSet.license;
        $('levelset-credits-name').innerHTML = format(
            '<a href="\flevels/\f.json">\f</a>',
            site_prefix,
            gLevelSet.name,
            gLevelSet.name
        );

        forkey(gLevelSets, function (levelSet) {
            levelSet.show_selector(name);
        });
    }

    function start_level(lvl, reset) {

        lvl = lvl || 0;

        var oLevel = gLevelSet.select_level(lvl, reset)
          , gg = null
        ;
        if (!reset && oLevel.gameData)  {
            gg = oLevel.gameData;
        }
        gGame.reset_level(gLevelSet.level, gg);

    }

    function show_bookmark_dialog(fnCallback) {

        var dlg = dlgBookmark
          , get = function (s) {return dlg.get(s);}
          , link = get('link')
          , blink = $('bookmark-link')
          , btnClose = get('button-close')
        ;

        if (btnClose) {
            btnClose.onclick = function () {
                dlg.hide();
            };
            link.href = blink.href;
            link.innerHTML = gLevelSet.name + ' ' + (gLevelSet.iLevel + 1);
            dlg.show();
        }
    }

    function show_success_dialog(sMsgHtml) {

        var dlg = dlgSuccess
          , get = function (s) {return dlg.get(s);}
          , btnSave = get('button-save')
          , btnClose = get('button-close')
        ;

        if (btnSave && btnClose) {
            get('message').innerHTML = sMsgHtml;
            btnSave.onclick = function () {
                dlg.hide();
                show_ajax_dialog();
            };
            btnClose.onclick = function () {
                dlg.hide();
            };
            dlg.show();
        }
    }

    function show_ajax_dialog() {

        var dlg = dlgAjax
          , get = function (s) {return dlg.get(s);}
          , btnClose = get('button-close')
          , oTitle = get('title')
          , oMsg = get('message')
        ;

        btnClose.onclick = function () {
            dlg.hide();
        };

        oMsg.innerHTML = 'Contacting server ...';
        oTitle.innerHTML = 'Submitting Solution';

        dlg.show();

        send_solution_home(function (text, status) {
            if (status) {
                text = '<p><b>Sorry</b>. Failed to contact server</p>';
            }
            oTitle.innerHTML = 'Solution Accepted';
            oMsg.innerHTML =  text;
            dlg.center();
        });

    }

    function parse_query() {

        var query = parse_location_search()
            , level
        ;

        if (!('levelSet' in query && query.levelSet in gLevelSets)) {
            query.levelSet = 'katomic';
        }
        level = query.level;

        query.level = 0;
        if (level && /^\d+$/.test(level)) {
            query.level = parseInt(level, 10) - 1;
        }
        return query;
    }

    // LevelSetClass

    function LevelSet(levelSet) {

        var self = Object.create(LevelSetClass);

        self.name = levelSet.name;
        self.credits = levelSet.credits;
        self.license = levelSet.license;
        self.levels = levelSet.levels;
        self.iLevel = 0;

        self.create_selector();
        gLevelSetNames.push(self.name);
        gLevelSets[self.name] = self;

        return self;
    }
    var LevelSetClass = {

        keys: [],
        selector: null,
        myid: '',

        getName: function () {
            return this.name;
        },

        create_selector: function () {

            if (this.selector) {
                return;
            }

            var level
              , select
            ;
            select = ['<select class="hide-selector" id="level-select-' + this.name + '">'];

            for (level = 0; level < this.levels.length;) {
                level += 1;
                select.push(
                    '<option value="' + level
                    + '">Level ' + level
                    + ': '
                    + this.levels[level - 1].name
                );
            }
            this.selector = 'level-select-' + this.name;
            $('level-selector-span').innerHTML += select.join('') + "</select>";

            return;
        },

        bind_selector: function (self) {

            xAddEventListener($(self.selector), 'change', function () {
                setTimeout(function () {
                    self.onLevelSelect();
                }, 100);
            }, false);
        },

        onLevelSelect: function () {
            start_level($(this.selector).selectedIndex);
        },

        show_selector: function (name) {

            var selector = $(this.selector);
            if (selector) {
                if (name && (this.name === name || this === name)) {
                    xRemoveClass(selector, 'hide-selector');
                    xAddClass(selector, 'show-selector');
                }
                else {
                    xRemoveClass(selector, 'show-selector');
                    xAddClass(selector, 'hide-selector');
                }
            }
        },

        select_level: function (lvl, reset) {

            lvl = lvl || 0;
            if (lvl >= this.levels.length) {
                lvl = 0;
            }
            this.iLevel = lvl;
            this.level = this.levels[lvl];

            $(this.selector).selectedIndex = lvl;

            return this.level;
        }
    };


    function init_endpoints(config) {

        site_prefix = config.site_prefix || '';

        $ = xGetElementById;

        var e = xGetElementsByTagName('div')
          , m
          , i
          , ii
          , gGame
          , games = []
          , eplevel = $('endpoints-level').innerHTML.split('`')
          , levelSet = eplevel[0]
          , level = parseInt(eplevel[1], 10) - 1
        ;

        foreach(e, function(ee) {
            if (ee.id.match(/arena-../)) {
                games.push(ee);
            }
        });

        i = 0;
        ii = games.length;

        function draw_arenas() {
            var ee = games[i];
            var gGame = KP_ATOMIX.Game(site_prefix + 'images/');
            gameList.push(gGame);
            gGame.setCellSize(27, 27);
            gGame.noEvents();

            gGame.init(ee.id, '',
                KP_ATOMIX.levelSets[levelSet].levels[level],
                $('ep-' + ee.id).innerHTML
            );
            gGame.hideArrows();
            i += 1;
            if (i < ii) {
                setTimeout(draw_arenas, 10);
            }
        }
        setTimeout(draw_arenas, 10);
    }

    function init(config) {

        site_prefix = config.site_prefix || '';
        browserTitle = config.browserTitle || browserTitle;

        $ = xGetElementById;

        xEnableDrag('molecule');
        xEnableDrag('arena', xCancel, xCancel, xCancel);

        dlgSuccess = kp_ModalDialog('success-dialog');
        dlgBookmark = kp_ModalDialog('bookmark-dialog');
        dlgAjax = kp_ModalDialog('ajax-dialog');

        gAjaxRequest = new xHttpRequest();
        dlgSuccess.get('user').value = gUserName;

        forkey(KP_ATOMIX.levelSets, function (levelSet) {
            LevelSet(levelSet);
        });

        forkey(gLevelSets, function (levelSet) {
            levelSet.bind_selector(levelSet);
        });

        setup_controls();
        create_levelset_selectors();

        gGame = KP_ATOMIX.Game(site_prefix + 'images/');

        gGame.onCompleteLevel = on_complete_level;
        gGame.onShowData = show_level_data;

        var query = parse_query();

        select_levelSet(query.levelSet, true);
        var oLevel = gLevelSet.select_level(query.level, true);

        gGame.init('arena', 'molecule', oLevel, query.history);


        xAddEventListener(document, 'keydown', onKeydown, false);

        start_level(query.level);

        xHeight('loading', 0);
    }

    (function() {
      if ((typeof Object.create) !== 'undefined') return;
      Object.create = function (o) {
        function F() {}
        F.prototype = o;
        return new F();
      };
    }());

    // Code in this section is automatically generated
    // Any changes will be lost on the next build

    //INCLUDE_XLIB

    KP_ATOMIX.init = init;
    KP_ATOMIX.init_endpoints = init_endpoints;

}());
