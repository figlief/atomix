    /* My extensions to xlib */

function xBottom(e) {
    return xTop(e) + xHeight(e);
}

function xRight(e) {
    return xLeft(e) + xWidth(e);
}

function xCancel(e) {
    xStopPropagation(e)
    xPreventDefault(e)
}

var kp_Shield = (function(zMin, zIncr)  {

  zMin = zMin || 10000;
  zIncr = zIncr || 100;

  var zList = [0]
    , _shield = null
  ;

  function isUp() {
    return zList.length  > 1;
  }

  function shield() {
    return _shield || create();
  }

  function create() {
    if (_shield) return _shield;
    _shield = document.createElement('div');
    _shield.className = 'xShieldElement';
    document.body.appendChild(_shield);
    zIndex(zMin);
    return _shield;
  }

  function show(z) {
    var e = shield()
      , ds = xDocSize()
    ;
    zIndex(z);
    xMoveTo(e, 0, 0);
    xResizeTo(e,
      Math.max(ds.w, xClientWidth()),
      Math.max(ds.h, xClientHeight())
    );
  }

  function hide() {
    var e = shield();
    xResizeTo(e, 10, 10);
    xMoveTo(e, -10, -10);
  }

  function zNext(z) {
    return Math.max(zMin, z || 0, zList[0]) + zIncr;
  }

  function zIndex(z) {
    return kp_zIndex(shield(), z);
  }

  function grab(z) {
    z = zNext(z);
    zList.unshift(z);
    show(z);
    return z;
  }

  function release() {
    switch (zList.length) {
      case 0:
        zList = [0];
        break;
      case 1:
        break;
      default:
        (zList.shift());
        show(zList[0]);
    }
    if (zList.length < 2) {
      hide();
    }
    return zList.length - 1;
  }

  return {
    grab: grab,
    release: release,
    isUp: isUp
  };

}());

function kp_ModalDialog(sId) {

  addClass('xxModalDialog');

  function get(s) {
    return xGetElementById(sId + (s ? '-' + s : ''));
  }

  function addClass(c, s) {
    xAddClass(get(s), c);
  }

  function show() {
    zIndex(kp_Shield.grab() + 1);
    return center();
  }

  function hide() {
    var dialog = get();
    if (dialog) {
      xMoveTo(dialog, -xWidth(dialog), 0);
    }
    kp_Shield.release();
  }

  function center () {
    var dialog = get();
    dialog.style.height = 'auto';
    xCenter(dialog);
    return this;
  }

  function zIndex(z) {
    kp_zIndex(get(), z);
  }

  return {
    show: show,
    hide: hide,
    get: get,
    center: center
  };

}

function kp_zIndex(e, z) {
  if (!(
    (e = xGetElementById(e)) &&
    xDef(e.style, e.style.zIndex)
  )) {
    return 0;
  }
  if (xDef(z) && xNum(z)) {
      e.style.zIndex = z;
  }
  z = xGetComputedStyle(e, 'zIndex', 1);
  return isNaN(z) ? 0 : z;
}
