from __future__ import unicode_literals

from cStringIO import StringIO
import csv


class PgBaseClass(object):

    def send_html(self, ctx):
        self.log('send_html not implemented')
        return []

    def send_xml(self, ctx):
        self.log('send_xml not implemented')
        return []

    def send_json(self, ctx):
        self.log('send_json not implemented')
        return []

    def send_txt(self, ctx):
        self.asText = True
        return self.send_json(ctx)

    def send_csv(self, ctx):

        buffer = StringIO()
        writer = csv.writer(buffer, dialect=csv.excel)
        writer.writerow(ctx.csvHeaders)

        for r in ctx.rows:
            row = (r[h] for h in ctx.csvHeaders)
            writer.writerow([unicode(xr).encode('utf-8') for xr in row])

        return buffer.getvalue().decode('utf-8')

    def send_kind(self, kind, ctx):
        ctx.page = getattr(ctx.app, kind + 'Page')()
        text = getattr(self, 'send_' + kind)(ctx)
        ctx.page.appendBody(text)
        return ctx.page

    def log(self, *args, **kw):
        pass
