from __future__ import unicode_literals

import datetime
import re
import json

from .atomix import Ctx as Bunch


class Uid:

    s = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

    def __call__(self, n=8):
        import random
        return ''.join(random.sample(self.s, n))

uid = Uid()

moveEncoder = list('abcdefghijklmnopqrstuvwxyz')
moveDecoder = {}
for i, c in enumerate(moveEncoder):
    moveDecoder[c] = i


msgBest = """
<p><b>Congratulations!</b></p>

<p>You solved this level in <b>%s Moves</b>.</p>

<p>This is the best solution we have recieved so far!</p>
<p>You have been awarded a %s.
If someone submits a better solution, you heart will turn from red to grey.</p>
%s
"""

msgBestEp = """
<p><b>Well Done!</b></p>

<p>You solved this level in <b>%s Moves</b>.</p>

<p>Although this is not the best solution for this level,
it <b>IS</b> the best solution for this endpoint.</p>
<p>You have been awarded a %s</p>
<p>You will lose the star if someone submits a better solution.</p>
%s
"""

msgFirst = """
<p>This is the first solution for this endpoint!</p>
<p>You have been awarded a %s</p>
"""

msgNotBest = """

<p>You solved this level in <b>%s Moves</b>.<p>

<p>However we already have a solution which
is as good or better than the one you submitted, so
you have no awards this time.<p>

<p>Never mind, try again!</p>
"""

msgInvalid = """
<p><b>Sorry</b>, an unexpected error occured on our server.</p>
<p>However, your solution has been recorded in our logs and will be
recovered by an administrator shortly.

<p><b>Error Report:</b> %s</p>
"""


class InvalidError(Exception):
    pass


class Action_Submit(object):

    def getBestLevel(self, ctx):

        db = ctx.db
        q = "select * from solutions"
        q += " where levelSet=:levelSet and level=:level"
        q += " and nextBest='L'"

        db.execute(q, ctx)

        row = db.fetchone()

        if row is not None:
            row = Bunch(row)

        return row

    def getBestEp(self, ctx):

        db = ctx.db

        q = """select * from solutions
where levelSet=:levelSet
 and level=:level
 and endpoint=:endpoint
 and nextEp='L'"""

        db.execute(q, ctx)

        row = db.fetchone()

        if row is not None:
            row = Bunch(row)

        return row

    def saveSolution(self, ctx):

        db = ctx.db

        q = ":uid, :date, :levelSet, :level, :user, :nMoves"
        q += ", :history, :prevBest, :nextBest, :endpoint"
        q += ", :prevEp, :nextEp"

        qSaveSolution = "insert into solutions values (%s)" % q

        qUpdateBest = """update
 solutions set nextBest=:nextBest,
 nextEp=:nextEp where uid=:uid"""

        qUpdateEp = 'update solutions set nextEp=:nextEp where uid=:uid'

        ctx.nMoves = len(ctx.history) // 4
        ctx.endpoint = self.verify_solution(ctx)

        best = self.getBestLevel(ctx)

        if best and ctx.endpoint == best.endpoint:
            bestEp = best
        else:
            bestEp = self.getBestEp(ctx)

        ctx.prevBest = 'X'
        ctx.nextBest = 'X'
        if best is None:
            ctx.prevBest = 'F'
            ctx.nextBest = 'L'
            best = ctx
        else:
            if best.nMoves > ctx.nMoves:
                best.nextBest = ctx.uid
                ctx.prevBest = best.uid
                ctx.nextBest = 'L'
            else:
                best = None

        ctx.prevEp = 'X'
        ctx.nextEp = 'X'
        if bestEp is None:
            ctx.prevEp = 'F'
            ctx.nextEp = 'L'
            bestEp = ctx
        else:
            if bestEp.nMoves > ctx.nMoves:
                bestEp.nextEp = ctx.uid
                ctx.prevEp = bestEp.uid
                ctx.nextEp = 'L'
                bestEp = ctx
            else:
                bestEp = None

        if best:
            db.execute(qUpdateBest, best)

        if bestEp and bestEp != best:
            db.execute(qUpdateEp, bestEp)

        db.execute(qSaveSolution, ctx)

        emblem = ctx.app.emblem
        _first = ''

        if ctx.prevEp == 'F':
            _first = msgFirst % emblem('First')

        if ctx.nextBest == 'L':
            msg = msgBest % (ctx.nMoves, emblem('Best'), _first)

        elif ctx.nextEp == 'L' and ctx.nextBest != 'L':
            msg = msgBestEp % (ctx.nMoves, emblem('Ep'), _first)

        else:
            msg = msgNotBest % ctx.nMoves

        return msg

    def collect_params(self, ctx, form):
        """Validate and translate the query string into a python dictionary."""

        def _assert(p, msg='Invalid Parameters'):
            if not (p):
                raise Exception(
                    'FAILED to validate cgi parameters (%s).' % msg)

        app = ctx.app

        allowed = app.allowedParams
        debugCode = app.debugCode
        debug = False

        params = Bunch()

        if debugCode and debugCode in form:
            debug = True
            allowed = app.allowedDebugParams

        for k in form.keys():

            if debugCode and k == debugCode and app.isMe():
                continue

            _assert(k in allowed, 'Unexpected parameter')

            v = form.get(k)
            # _assert(len(v) < 2, 'Parameter "%s" appears more than once.' % k)

            if k == 'history':
                msg = 'Too much history (%s)!'
                _assert(len(v) <= app.maxHistoryLength, msg % len(v))
                _assert(app.reHistory.match(v), 'Malformed History')

            if k == 'levelSet':
                msg = 'levelSet is not known'
                _assert(v in app.allowedLevelSets, msg)

            params[k] = v

        _assert('history' in params, 'No history!')

        _assert('level' in params, 'level is missing')

        if 'user' not in params:
            params.user = ''
        else:
            params.user = params.user.decode('utf-8)')

        if 'levelSet' not in params:
            params.levelSet = 'katomic'

        if 'sRand' not in params:
            params.sRand = ''

        # sanitize user
        user = params.user[:app.maxUserLength]
        user = re.sub(ur"""[\\%<>/'":&]|\s+""", ' ', params.user)
        # normalzie spaces
        user = re.sub(ur"\s+", ' ', user)
        user = user.strip()
        if not user or user.lower().startswith('anon'):
            user = ''
        params.user = user

        if not debug or 'date' not in params:
            params.date = str(datetime.datetime.utcnow())[:19]

        if not debug:
            # params.ip = cgi.escape(os.environ["REMOTE_ADDR"])
            pass

        if not debug or 'uid' not in params:
            params.uid = uid()

        ctx.update(params)
        return params

    def verify_solution(self, ctx):

        def _assert(p, msg='Invalid'):
            if not (p):
                raise Exception('FAILED to veryify solution (%s).' % msg)

        history = ctx.history
        level = self.load_level_data(ctx)
        # ctx.levelSet, ctx.level)

        arena = level['arena']
        arenaWidth = len(arena[0])

        atoms = level['atoms'].keys()

        molecule = level['molecule']
        moleculeWidth = len(molecule[0])

        pad = '.' * (arenaWidth - moleculeWidth)
        molecule = pad.join(molecule)

        grid = [list(row) for row in arena]

        h = history
        hlen = len(h) - 4

        i = -4
        while i < hlen:
            i += 4
            args = (moveDecoder[xi] for xi in h[i:i+4])
            startRow, startCol, endRow, endCol = args
            row, col = startRow, startCol

            atom = grid[startRow][startCol]
            _assert(atom in atoms)

            data = grid[row]

            if startRow == endRow:
                _assert(startCol != endCol)

                if startCol > endCol:
                    while data[col - 1] == '.':
                        col -= 1
                else:
                    while data[col + 1] == '.':
                        col += 1

            else:
                _assert(startCol == endCol)

                if startRow > endRow:
                    while grid[row - 1][col] == '.':
                        row -= 1
                else:
                    while grid[row + 1][col] == '.':
                        row += 1

            _assert(row == endRow and col == endCol)

            grid[startRow][startCol] = '.'
            grid[endRow][endCol] = atom

        grid = ''.join(''.join(row) for row in grid).replace('#', '.')

        endpoint = grid.find(molecule)
        _assert(endpoint >= 0, 'incomplete')

        row, col = endpoint // arenaWidth, endpoint % arenaWidth
        return moveEncoder[row] + moveEncoder[col]

    def load_level_data(self, ctx):
        # levelSet, level):

        db = ctx.db

        q = "select * from levels where levelSet=:levelSet and level=:level"

        db.execute(q, ctx)

        r = db.fetchone()
        if not r:
            return None
        r = Bunch(r)
        r.arena = r.arena.split('\n')
        r.molecule = str(r.molecule).split('\n')
        r.atoms = json.loads(r.atoms)
        r.endpoints = r.endpoints.split('\n')

        return r

    def __call__(self, app, form):

        page = app.txtPage()

        ctx = Bunch(
            app=app,
            page=page,
        )
        self.collect_params(ctx, form)
        print ('Params: %s' % ctx)

        ctx.db = db = app.DB()
        msg = self.saveSolution(ctx)
        db.commit()
        db.close()

        page.appendBody(msg)
        return page

_instance = Action_Submit()


def run(*args):
    return _instance(*args)
