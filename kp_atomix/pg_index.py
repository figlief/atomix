from __future__ import unicode_literals

from .atomix import (
    Ctx,
    PgBaseClass,
)


class PgIndex(PgBaseClass):

    index_tpl = """
    <form>

      <div class="center" id="controls">

        <div id="selectors">

        <p>
          <span id="levelset-selector-span"></span>
          <span id="level-selector-span"></span>
        </p>
        <p>

          &nbsp;&nbsp;&nbsp;
          <a href="#" id="prev-level">Prev Level</a>

          &nbsp;&nbsp;&nbsp;
          <a href="#" id="next-level">Next Level</a>

          &nbsp;&nbsp;&nbsp;
          <a href="#" id="history-reset">Reset Level</a>

          &nbsp;&nbsp;&nbsp;
          <a href="#" id="bookmark-link"
            title="Bookmark This Link To Save Level"
          >Save Level</a>
        </p>
        </div>
      </div>
    </form>

    <hr />

    <div id="loading">
      Atomix is loading ...
      <noscript><br />... sorry, &nbsp;javascript is disabled so you can't play
      :-(</noscript>
      <hr />
    </div>
    <table ><tr>

      <td>
        <div class="arena-container arena-border">
          <div id="arena" class="arena"></div>
          <div id="move-controls">
            <a href="#" id="history-first">First</a>
              &nbsp;&nbsp;&nbsp;
            <a href="#" id="history-undo">Undo</a>
              &nbsp;&nbsp;&nbsp;
            <span id="move-no">&nbsp;</span>
              &nbsp;&nbsp;&nbsp;
            <a href="#" id="history-redo">Redo</a>
              &nbsp;&nbsp;&nbsp;
            <a href="#" id="history-last">Last</a>
          </div>
        </div>
      </td>

      <td style="vertical-align:top;padding:20px;">
        <div class="molecule-container molecule-border">
          <div id="molecule" class="molecule"></div>
        </div>
      </td>


    </tr></table>

"""

    pre_footer_tpl = """
<hr />
<div id="levelset-credits">
  LevelSet: <span id="levelset-credits-name">&nbsp;</span>
    &nbsp;&nbsp;
  Credits: <span id="levelset-credits-credit">&nbsp;</span>
    &nbsp;&nbsp;
  License: <span id="levelset-credits-license">&nbsp;</span>
</div>

<hr />
<!-- start blurb -->
<div id="blurb">
<h3> The Atomix Sliding Puzzle Game </h3>

<p>The object of this game is to assemble a molecule from the atoms on the
playing area by sliding them around.</p>

<p>Beware! Once an atom starts moving it will not stop until it
hits another atom or the wall.</p>

<p>To move an atom, first select it by clicking on it with the mouse,
then click on one of the golden arrows that will appear around it.</p>

<p>An image of the molecule to be constructed is shown to the right of the
playing area. This image is transparent and can be dragged over the
playing area to see where it fits, and so help you decide where to build your
molecule. The molecule may be built anywhere, but it is easier to build in some
places than in others.</p>

<p>Once you have built the molecule you will be invited to submit your solution
to this website.  If your solution is better than any other solution we have
recieved then your solution will appear in our solutions tables.</p>

<p>The best solution for each
level is the one that uses the least amount of moves
to complete the molecule.</p>


</div>
<!-- end blurb -->
"""

    dialogs_tpl = """
<!-- start dialogs -->

    <div id="success-dialog">

      <h4>Well Done!</h4>

      <form id="success-dialog-form" onsubmit="return false;">

          <p id="success-dialog-message">&nbsp;</p>
          <p>
             <b>Please Enter Your</b><br />
             <b>Name or Nickname</b>
             <br />
             <input type="text" value='Me Is Name' class="center"
               id="success-dialog-user"
             />
          </p>

          <p class="buttons">
            <input
              id="success-dialog-button-close"
              type="button"
              value="Close"
            />
            <input
              id="success-dialog-button-save"
              type="button"
              value="Submit"
            />
          </p>

      </form>
    </div>

     <div id="ajax-dialog">

      <h4 id='ajax-dialog-title'>Submiting Solution</h4>

      <form id="ajax-dialog-form" onsubmit="return false;">

          <div id="ajax-dialog-message">
            <p>Waiting for reply from server ...</p>
          </div>

          <p class="buttons">
            <input id="ajax-dialog-button-close" type="button" value="Close" />
          </p>

      </form>

    </div>

    <div id="bookmark-dialog">

      <h4>Bookmark This Level</h4>

      <form id="bookmark-dialog-form" onsubmit="return false;">

        <p><a id="bookmark-dialog-link">&nbsp;</a></p>

          <p id="bookmark-dialog-message">
            <b>Save this level by<br />bookmarking the above link.</b>
            <br /><br />
            <b>Either:</b> right click the link and choose the
            'bookmark link' option from the menu.
            <br /><br />
            <b>Or:</b> click on the link and then bookmark the page.
          </p>

          <p class="buttons">
            <input id="bookmark-dialog-button-close"
              type="button"
              value="Close"
            />
          </p>

      </form>
    </div>
<!-- end dialogs -->
"""

    head_scripts_tpl = """
<script>
    window.onload = function () {
      KP_ATOMIX.init({
        site_prefix: '%s/',
        browserTitele: '%s'
      });
   }
   window.onunload = function(){}
</script>
"""

    def send_html(self, ctx):
        app = ctx.app

        page = ctx.page

        page.head_scripts = self.head_scripts_tpl % (app.link_prefix, '')
        page.pre_footer = self.pre_footer_tpl

        page.appendBody(self.index_tpl)

        tail_scripts = app.getTailScripts(ctx)

        page.tail_scripts = self.dialogs_tpl + tail_scripts

        return ''

    def __call__(self, app, form, levelSet='', level='', history=''):

        data = Ctx(
            app=app,
            asText=False,
            levelSet=levelSet,
            level=level,
            history=history,
            kind='html'
        )
        return self.send_kind('html', data)

_instance = PgIndex()


def run(*args):
    return _instance(*args)
