from __future__ import unicode_literals

from .atomix import (
    Ctx,
    PgBaseClass,
)


class PgBackup(PgBaseClass):

    csvHeaders = ('uid', 'date', 'levelSet', 'level', 'user', 'history')

    def dbGetData(self, ctx):

        db = ctx.app.DB()

        sql = "SELECT uid, date, levelSet, level, user, history FROM solutions"

        if ctx.start:
            sql += " where rowid>(select rowid from solutions where uid=:start)"
        if ctx.limit:
            sql += " LIMIT :limit"

        db.execute(sql, ctx)

        items = []

        for r in db.c:

            r = Ctx(r)
            r.formatedDate = r.date

            items.append(r)

        db.close()
        ctx.rows = items

    def __call__(self, app, form):

        ctx = Ctx(
            app=app,
            asText=False,
            kind='csv',
            start=form.get('start', ''),
            limit=form.get('limit', ''),
            csvHeaders=self.csvHeaders,
        )
        self.dbGetData(ctx),

        return self.send_kind('csv', ctx)

_instance = PgBackup()


def run(*args):
    return _instance(*args)
