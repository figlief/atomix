from __future__ import unicode_literals

from .atomix import (
    fmt,
    Ctx,
    Error404,
    PgBaseClass,
    toUrl64,
)

_instance = None


class PgSolutions(PgBaseClass):

    csvHeaders = ['levelSet', 'level', 'date', 'user', 'nMoves', 'history']

    table_tpl = """

<h1 class="center">best solutions for <br />{levelSet} levels</h1>
<p class="center"><i>Thank you for submitting your solutions!</i></p>

<div class="center">

    <p>These solutions are the best that have been submitted to this site,
    <br />they are not necessarily the best possible solutions.</p>

    <p><b>Remember to enter your NAME or a NICKNAME<br />
    so we can properly credit your efforts</b></p>

</div>

<table class="solutions-table"><tr>
  <td style="text-align:center">
  <p style="font-size:smaller;padding:0;margin:0;">
  <i>Alternative formats are availiable:&nbsp;&nbsp;</i>
  <a href="{app.link_prefix}/solutions/{levelSet}.txt">TXT</a>&nbsp;&nbsp;
  <a href="{app.link_prefix}/solutions/{levelSet}.json">JSON</a>&nbsp;&nbsp;
  <a href="{app.link_prefix}/solutions/{levelSet}.xml">XML</a>&nbsp;&nbsp;
  <a href="{app.link_prefix}/solutions/{levelSet}.csv">CSV</a></p>
</td></tr>
</table>

<table class="solutions-table">
<tr>
  <th>Level</th>
  <th>Solution</th>
  <th>EP</th>
  <th>User</th>
  <th>Date</th>
</tr>

{html}

</table>
<p id="lastupdated">Last updated on {lastDate} at {lastTime} GMT
<br /><br /></p>
"""

    row_tpl = """
       <tr>
          <td>{prefix_tpl}">{row.level}</a></td>
          <td>{nMoves_tpl}</td>
          <td>{endpoint_tpl}</td>
          <td>{user_tpl}</td>
          <td>{row.formatedDate}</td>
        </tr>
"""

    json_row_tpl = """
  {{
    "level": "{row.level}",
    "user": "{row.user}",
    "moves": {row.nMoves},
    "history": "{row.history}",
    "date": "{row.date}"
  }}"""

    json_leadin_tpl = """[
  {{
    "levelSet": "{levelSet}",
    "lastModified": "{lastModified}",
    "source": "{app.siteURL}/solutions/{levelSet}.json",
    "usage": "Use as you wish but a link to https://code.google.com/p/kp-atomix would be welcome."
  }},"""

    xml_leadin_tpl = """
<!-- Source: {app.siteURL}/solutions/{levelSet}.xml -->
<!-- Usage: Use as you wish but a link to https://code.google.com/p/kp-atomix would be welcome. -->

<solutions levelSet="{levelSet}" lastModified="{lastModified}">
"""

    def dbGetData(self, ctx):

        app = ctx.app
        db = app.DB()

        q = "SELECT level from levels where levelSet=:levelSet order by `order_`"
        db.execute(q, ctx)

        levels = [r[0] for r in db.c]

        q = "SELECT * FROM solutions"
        q += " WHERE levelSet=:levelSet"
        q += " and nextBest='L' "

        db.execute(q, ctx)
        items = [Ctx(r) for r in db.c]
        db.close()

        best = {}
        for r in items:
            r.formatedDate = r.date[:10]
            # r.user = r.user or 'anonymous'
            if r.user:
                r.user64 = toUrl64(r.user)

            best[r.level] = r

        for i, level in enumerate(levels):

            r = best.get(level, None)
            if r is None:
                r = Ctx()
                r.level = level
                r.levelSet = ctx.levelSet
                r.endpoint = ''
                r.history = ''
                r.nMoves = 0
                r.user = ''
                r.date = ''
                r.formatedDate = ''
            levels[i] = r

        ctx.rows = levels

        dates = [rr.date for rr in ctx.rows]
        lm = ''
        if len(dates):
            lm = max(dates)
        ctx.lastModified = lm

    def send_json(self, ctx):

        txt = fmt(ctx, self.json_leadin_tpl)
        lst = []
        for ctx.row in ctx.rows:
            item = fmt(ctx, self.json_row_tpl)
            lst.append(item)
        return txt + ','.join(lst) + "\n]"

    def send_xml(self, ctx):

        txt = []
        for ctx.row in ctx.rows:

            txt.append(fmt(ctx, """
  <solution level="{row.level}">
    <user>{row.user}</user>
    <moves>{row.nMoves}</moves>
    <history>{row.history}</history>
    <date>{row.date}</date>
  </solution>
"""))
        txt.append("</solutions>\n")

        return fmt(ctx, self.xml_leadin_tpl) + '\n'.join(txt)

    def send_html(self, ctx):

        self.log(ctx.lastModified)

        ctx.lastDate = ctx.lastModified[:10]
        ctx.lastTime = ctx.lastModified[10:]

        ctx.page.title = "best solutions for %s levels" % ctx.levelSet

        html = []
        for row in ctx.rows:
            ctx.row = row

            ctx.prefix_tpl = fmt(ctx, '<a rel="nofollow" href="{app.link_prefix}/{app.indexFile}?levelSet={levelSet}&amp;level={row.level}')

            ctx.nMoves_tpl = '&nbsp;'
            if row.nMoves:
                ctx.nMoves_tpl = fmt(ctx, '{prefix_tpl}&amp;history={row.history}">{row.nMoves} Moves</a>')

            ctx.user_tpl = '&nbsp;'
            if row.user:
                ctx.user_tpl = fmt(ctx, """\
<a rel="nofollow" href="{app.link_prefix}/recent/{row.user64}.html">{row.user}</a>""")

            ctx.endpoint_tpl = fmt(ctx, """\
<a rel="nofollow" href="{app.link_prefix}/endpoints/{levelSet}/{row.level}.html">{row.endpoint}</a>""")

            html.append(fmt(ctx, self.row_tpl))
        ctx.html = '\n'.join(html)

        return fmt(ctx, self.table_tpl)

    def __call__(self, app, form,  levelSet, kind):
        if levelSet not in app.allowedLevelSets:
            raise Error404()

        ctx = Ctx(
            app=app,
            asText=False,
            levelSet=levelSet,
            kind=kind,
            csvHeaders=self.csvHeaders,

        )
        self.dbGetData(ctx),

        return self.send_kind(kind, ctx)


_instance = PgSolutions()


def run(*args):
    return _instance(*args)
