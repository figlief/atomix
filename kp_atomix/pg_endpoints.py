from __future__ import unicode_literals

from .atomix import (
    fmt,
    Ctx,
    PgBaseClass,
    toUrl64,
)
from collections import defaultdict


class PgEndpoints(PgBaseClass):

    page_tpl = """
<h1 class="center">Endpoints for {levelSet}:{level}</h1>
<p id="endpoints-level" class="hidden">{levelSet}`{level}</p>

{html}

<p id="lastupdated">Last updated on {lastDate} at {lastTime} GMT
<br /><br /></p>
"""

    table_tpl = """
<tr>
<td>
  <div class="arena-container arena-border">
    <div id="arena-{ep}" class="arena">Loading ...</div>
  </div>
</td>

<td style="vertical-align:middle">
  <table class="solutions-table">
  <tr><td colspan="4"><h3 class="center">{ep}</h3></td></tr>
  <tr>
    <th>Level</th>
    <th>Solution</th>
    <th>User</th>
    <th>Date</th>
  </tr>

  {rows}

  </table>
</td>
</tr>
"""

    row_tpl = """
       <tr>
          <td>{prefix_tpl}">{level}</a></td>
          <td>{nMoves_tpl}</td>
          <td>{user_tpl}</td>
          <td>{row.formatedDate}</td>
        </tr>
"""
    head_scripts_tpl = """
<script>
    window.onload = function () {
      KP_ATOMIX.init_endpoints({
        site_prefix: '%s/'
      });
   }
   window.onunload = function(){}
</script>
"""

    def dbGetData(self, ctx):

        db = ctx.app.DB()

        # Get a Set of endpoints for this levelSet and level

        q = "select * from levels where levelSet=:levelSet and level=:level"
        db.execute(q, ctx)

        epr = db.c.fetchone()

        endpoints = set(epr[str('endpoints')].split('\n'))

        # Get Solutions for this levelSet and level

        q = "SELECT date, user, nMoves, history, nextBest, nextEp, endpoint"
        q += " FROM solutions"
        q += " WHERE levelSet=:levelSet"
        q += " and level=:level"
        q += " and nextEp!='X'"

        if ctx.ep:
            q += " and endpoint=:ep"

        q += " ORDER BY date"

        db.execute(q, ctx)
        items = [Ctx(r) for r in db.c]
        db.close()

        # dictionary keys=endpoints values= list of best solutions in reverse
        # order of time recieved

        best = defaultdict(list)

        for item in items:
            best[item.endpoint].insert(0, item)

        epKeys = set(best.iterkeys())
        endpoints -= epKeys

        bestOfBest = epBestOfBest = ''
        for ep, lst in best.iteritems():
            if not lst:
                continue

            if lst[0].nextBest == 'L':
                bestOfBest = lst
                epBestOfBest = ep
                break

        if epBestOfBest:
            del best[epBestOfBest]

        best = sorted(best.iteritems())

        best.insert(0,  (epBestOfBest, bestOfBest))

        modified = [v[0].date for k, v in best if v]
        lastModified = max(modified) if modified else ''

        ctx.update(dict(
            best=best,
            endpoints=sorted(endpoints),
            lastModified=lastModified,
            lastDate=lastModified[:10],
            lastTime=lastModified[10:]
        ))

        return ctx

    def html_ep_table(self, ctx, ep, lst):
        if not lst:
            return ''
        epdivs = ctx.epdivs
        rows = []
        ctx.prefix_tpl = fmt(ctx, """\
<a rel="nofollow"
href="{app.link_prefix}/?levelSet={levelSet}&amp;level={level}""")
        for row in lst:
            ctx.row = row

            ctx.nMoves_tpl = '{prefix_tpl}&amp;history={row.history}">{row.nMoves} Moves</a>'

            ctx.user_tpl = '&nbsp;'
            if row.user:
                ctx.user64 = toUrl64(row.user)
                ctx.user_tpl = '<a rel="nofollow" href="{app.link_prefix}/recent/{user64}.html">{row.user}</a>'

            row.formatedDate = row.date[:10]

            rows.append(fmt(ctx, fmt(ctx, self.row_tpl)))

            if ep not in epdivs:
                epdivs[ep] = row.history

        ctx.rows = '\n'.join(rows)
        ctx.ep = ep
        return fmt(ctx, self.table_tpl)

    def html_tables(self, ctx):

        tables = []
        ctx.epdivs = {}
        for ep, lst in ctx.best:
            ep_table = self.html_ep_table(ctx, ep, lst)
            if ep_table:
                tables.append(ep_table)
        if not tables:
            return tables

        html = ['<table style="margin:auto;">']
        html = html + tables + ['</table>']
        return html

    def send_html(self, ctx):

        app = ctx.app
        page = ctx.page

        page.title = "endpoints for %s:%s " % (ctx.levelSet, ctx.level)

        html = self.html_tables(ctx)

        if not html:
            return fmt(ctx, '<h1>No solutions for {levelSet} level {level}')

        if ctx.endpoints:
            html.append("<p class=\"center\">Solutions needed for: %s<br />" % ' '.join(ctx.endpoints))
            html.append("Some of these endpoints may have no valid solution.</p>")

        for item in ctx.epdivs.iteritems():
            html.append('<p class="hidden" id="ep-arena-%s">%s</p>' % item)

        ctx.html = '\n'.join(html)

        page.head_scripts = self.head_scripts_tpl % app.link_prefix

        page.tail_scripts = app.getTailScripts(ctx)

        return fmt(ctx, self.page_tpl)

    def __call__(self, app, form, levelSet, level, ep, kind):

        ctx = Ctx(
            app=app,
            asText=False,
            levelSet=levelSet,
            level=level,
            ep=ep,
            kind=kind
        )
        self.dbGetData(ctx)

        return self.send_kind(kind, ctx)


_instance = PgEndpoints()


def run(*args):
    return _instance(*args)
