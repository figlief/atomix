from __future__ import unicode_literals

__all__ = """

    toUrl64
    fromUrl64
    formatDate

""".strip().split()


import base64


def toUrl64(s):
    return base64.urlsafe_b64encode(s.encode('utf-8'))


def fromUrl64(user64):
    if not user64:
        return ''
    try:
        user = base64.urlsafe_b64decode(user64.encode('ascii'))
        return user.decode('utf-8')
    except Exception:
        return ''

months_long = {
    '01': 'January',
    '02': 'Febuary',
    '03': 'March',
    '04': 'April',
    '05': 'May',
    '06': 'June',
    '07': 'July',
    '08': 'August',
    '09': 'September',
    '10': 'October',
    '11': 'November',
    '12': 'December'
}
months_short = {
    '01': 'Jan',
    '02': 'Feb',
    '03': 'Mar',
    '04': 'Apr',
    '05': 'May',
    '06': 'Jun',
    '07': 'Jul',
    '08': 'Aug',
    '09': 'Sep',
    '10': 'Oct',
    '11': 'Nov',
    '12': 'Dec'
}

day_exts = {'1': 'st', '2': 'nd', '3': 'rd'}


def formatDate(s):
    """reformat date 's' from yy-mm-dd to Month dd[st,nd,rd,th], yyyy"""

    d = s[:10].split('-')
    if len(d) != 3:
        return s[:10]
    y, m, d = d

    val = int(d, 10)
    # http://www.wellho.net/mouth/959_It-s-the-1st-not-the-1nd-1rd-or-1th-.html
    ding = (["st", "nd", "rd"] + ["th"] * 7)[(val - 1) % 10]
    if val/10 == 1:
        ding = "th"

    df = '%d%s' % (val, ding)
    # m = months_short[m]

    tpl = """\
<span title="%s %s, %s">
<span style="font-size: smaller">%s</span>&nbsp;%d</span>"""
    return tpl % (months_long[m], df, y, months_short[m], int(d))
