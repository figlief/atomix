from __future__ import unicode_literals

import base64

from operator import attrgetter

from .atomix import (
    fmt,
    Ctx,
    PgBaseClass,
)

class PgUsers(PgBaseClass):

    table_tpl = """
<h1 class="center">Top Contributors to<br />Atomix Online</h1>
<p class="center"><i>Thank you for submitting your solutions!</i></p>


<table class="solutions-table">
<tr><td style="text-align:center">
<p style="font-size:smaller;padding:0;margin:0;">
  <i>Alternative formats are availiable:&nbsp;&nbsp;</i>
  <a href="{app.link_prefix}/users.txt">TXT</a>&nbsp;&nbsp;
  <a href="{app.link_prefix}/users.json">JSON</a>&nbsp;&nbsp;
  <a href="{app.link_prefix}/users.xml">XML</a>&nbsp;&nbsp;
  <a href="{app.link_prefix}/users.csv">CSV</a>
</td></tr>
</table>

<table class="solutions-table">
{html}
</table>
"""

    row_tpl = """
       <tr>
        <td>{rank_tpl}</td>
        <td>{user_tpl}</td>
        <td>{best_tpl}</td>
        <td>{bestEp_tpl}</td>
        <td>{prev_tpl}</td>
        <td>{first_tpl}</td>
        <td>{nTotal_tpl}</td>
        </tr>
"""
    json_leadin_tpl = """[{{
 :"source": "{app.siteURL}/users.json",
  "usage": "Use as you wish but a backlink to {app.siteURL}/ would be welcome."
"""

    xml_leadin_tpl = """
<!-- Source: {app.siteURL}/users.xml -->

<users>
"""

    MIN_COUNT = 5
    xml_tags = 'rank name best bestEp prev first solved'.split(' ')

    class User:

        def __init__(self, user):
            self.user = user
            self.best = 0
            self.bestEp = 0
            self.prev = 0
            self.first = 0
            self.solved = 0

    class Data:

        def __init__(self):
            self.best = []
            # self.bestEp = []
            self.prev = []
            self.first = []
            self.other = []
            self.fry = PgUsers.User('-')
            self.anon = PgUsers.User('')
            self.anonymous = [self.anon, self.fry]
            self.lists = [self.best, self.prev, self.other, self.anonymous]
            self.lowUsers = 0

    def dbGetData(self, ctx):

        db = ctx.app.DB()

        d = ctx.dbData = self.Data()

        sql = "select user, prevBest, nextBest, prevEp, nextEp from solutions"

        db.execute(sql)

        users = {'': d.anon}
        userlist = []

        LX = ('L', 'X')
        for r in db.c:

            r = Ctx(r)

            isFirst = (r.prevEp == 'F')

            isBest = (r.nextBest == 'L')
            isPrev = (r.nextBest not in LX) and not (r.prevBest == 'F')

            isEpBest = (r.nextEp == 'L') and (not isBest)

            ou = users.get(r.user, None)

            if ou is None:
                users[r.user] = ou = PgUsers.User(r.user)
                userlist.append(ou)

            if isBest:
                ou.best += 1
            elif isPrev:
                ou.prev += 1

            if isFirst:
                ou.first += 1

            if isEpBest:
                ou.bestEp += 1

            ou.solved += 1

        db.close()

        for u in userlist:
            if u.best:
                d.best.append(u)
            elif u.solved < self.MIN_COUNT:
                d.lowUsers += 1
                d.fry.solved += u.solved
                d.fry.first += u.first
                d.fry.prev += u.prev
            elif u.prev:
                d.prev.append(u)
            else:
                d.other.append(u)

        d.best[:] = sorted(
            d.best,
            key=attrgetter(
                'best',
                'bestEp',
                'prev',
                'first',
                'solved'
            ),
            reverse=True
        )
        d.prev[:] = sorted(
            d.prev,
            key=attrgetter(
                'bestEp',
                'prev',
                'first',
                'solved'
            ),
            reverse=True
        )
        d.other[:] = sorted(
            d.other,
            key=attrgetter(
                'bestEp',
                'first',
                'solved',
            ), reverse=True
        )

        rank = 1
        for rows in d.lists:
            for r in rows:
                r.rank = rank
                rank += 1

        d.fry.user = '-- %s other users --' % d.lowUsers
        d.fry.rank = -1
        d.anon.user = '-- anonymous --'
        d.anon.rank = -1

    def send_json(self, ctx):

        tpl = []
        for k in self.xml_tags:
            tpl.append('"%s": {row.%s}' % (k, k))
        tpl = ','.join(tpl)

        txt = fmt(ctx, self.json_leadin_tpl)
        lst = [txt]
        for rows in ctx.dbData.lists:
            for row in rows:
                ctx.row = row
                row.name = '"%s"' % row.user
                item = fmt(ctx, tpl)
                lst.append(item)
        return '},\n{'.join(lst) + "}\n]\n"

    def send_csv(self, ctx):
        rows = ctx.dbData

        from cStringIO import StringIO
        import csv

        buffer = StringIO()
        writer = csv.writer(buffer, dialect=csv.excel)
        writer.writerow(self.xml_tags)

        for rows in ctx.dbData.lists:
            for r in rows:
                r.name = r.user
                row = [getattr(r, x) for x in self.xml_tags]
                writer.writerow([unicode(xr).encode('utf-8') for xr in row])
        return buffer.getvalue().decode('utf-8')

    def send_xml(self, ctx):

        text = [fmt(ctx, self.xml_leadin_tpl)]
        for rows in ctx.dbData.lists:

            for row in rows:
                ctx.row = row
                row.name = row.user
                t = ['  <user>']
                for k in self.xml_tags:
                    t.append('    <%s>{row.%s}</%s>' % (k, k, k))
                t.append('  </user>')
                text.append(fmt(ctx, '\n'.join(t)))

        text = '\n'.join(text)
        return text + '\n</users>'

    def urlsafe64(self, user):
        return base64.urlsafe_b64encode(user.encode('utf-8'))

    def send_html(self, ctx):

        ctx.page.title = "Top Contributors"

        ctx.html = []
        second = True
        for lst in ctx.dbData.lists:
            ctx.rows = lst
            self.doBlock(ctx)
            if second:
                second = False
                ctx.html.append("""
<tr class="no-border" ><td colspan="7">&nbsp;</td></tr>
<tr class="no-border" ><td colspan="7" class="center">
<b>Users must submit FIVE solutions before they appear below.</b></td></tr>""")

        ctx.html = '\n'.join(ctx.html)
        return fmt(ctx, self.table_tpl)

    def doBlock(self, ctx):

        app = ctx.app
        ctx.html.append("""
<tr class="no-border" ><td colspan="7">&nbsp;</td></tr>
<tr><th>Rank</th><th>User</th><th>Best</th>
<th>BestEp</th><th>Prev</th><th>First</th><th>Solved</th></tr>""")

        for row in ctx.rows:

            user = row.user
            if user:
                ctx.user_target = ''.join((
                    app.link_prefix,
                    '/recent/',
                    self.urlsafe64(user)
                ))

            ctx.status_tpl = "%s, %s, %s" % (row.best, row.prev, row.first)
            ctx.nTotal_tpl = "%s" % row.solved

            ctx.user_tpl = '&nbsp;'
            if user:
                ctx.user = user
                ctx.user_tpl = fmt(ctx, """
<a rel="nofollow" href="{user_target}.html">{user}</a>""")

            if ctx.rows is ctx.dbData.anonymous:
                ctx.user_tpl = '<b>%s</b>' % user
                ctx.rank_tpl = "&nbsp;"
            else:
                ctx.rank_tpl = '%d' % row.rank

            ctx.best_tpl = "&nbsp;"
            ctx.prev_tpl = "&nbsp;"
            ctx.bestEp_tpl = "&nbsp;"
            ctx.first_tpl = "&nbsp;"

            if row.best:
                ctx.best_tpl = app.emblem('Best') + "&nbsp;%s" % row.best
            if row.prev:
                ctx.prev_tpl = app.emblem('Prev') + "&nbsp;%s" % row.prev
            if row.bestEp:
                ctx.bestEp_tpl = app.emblem('Ep') + "&nbsp;%s" % row.bestEp
            if row.first:
                ctx.first_tpl = app.emblem('First') + "&nbsp;%s" % row.first

            ctx.html.append(fmt(ctx, self.row_tpl))

        return

    def __call__(self, app, form, kind):
        ctx = Ctx(
            app=app,
            asText=False,
            kind=kind
        )
        self.dbGetData(ctx)

        return self.send_kind(kind, ctx)

_instance = PgUsers()


def run(*args):
    return _instance(*args)
