from __future__ import unicode_literals

from atomix import (
    fmt,
    Ctx,
    PgBaseClass,
    toUrl64,
    fromUrl64,
    formatDate,
)


def isint(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


class PgRecent(PgBaseClass):

    RECENT_LIMIT = 60

    csvHeaders = (
        'date',
        'user',
        'levelSet',
        'level',
        'nMoves',
        'isBest',
        'isBestEp',
        'isPrev',
        'isPrevEp',
        'isFirst',
        'history')

    table_tpl = """

<h1 class="center">Recent Activity for {user} <br />Atomix Online</h1>
<p class="center"><i>Thank you for submitting your solutions!</i></p>

<div class="center">

    <p>{all_or_best_link}<p>

</div>

<table class="solutions-table">
<tr><td align="center">
 <p style="font-size:smaller;padding:0;margin:0;">
  <i>Alternative formats are availiable:&nbsp;&nbsp;</i>
  <a href="{user_target}.txt?limit={limit}">TXT</a>&nbsp;&nbsp;
  <a href="{user_target}.json?limit={limit}">JSON</a>&nbsp;&nbsp;
  <a href="{user_target}.xml?limit={limit}">XML</a>&nbsp;&nbsp;
  <a href="{user_target}.csv?limit={limit}">CSV</a>
 </p>
</td></tr>
</table>

<table class="solutions-table">
<tr>
  <th>Date</th>
  <th>User</th>
  <th>Level</th>
  <th>Moves</th>
  <th>EP</th>
  <th>Status</th>
</tr>

{html}
</table>

<table class="solutions-table">
<tr><td align="center">
<img
  src="{app.link_prefix}/images/emblem-first.png"
  alt="[First]" class="emblem"
/>
&nbsp;<b>First Solution</b>
<img
  src="{app.link_prefix}/images/emblem-best.png"
  alt="[Best]" class="emblem"
/>
&nbsp;<b>Best Solution</b>
<img
  src="{app.link_prefix}/images/emblem-prev.png"
  alt="[Previous]" class="emblem"
/>
&nbsp;<b>Previous Best</b>
</td></tr>
<tr><td colspan="3" align="center">
<img
  src="{app.link_prefix}/images/emblem-ep.png"
  alt="[BestEP]" class="emblem"
/>
&nbsp;<b>Best for this endpoint</b>
</table>
"""

    row_tpl = """
       <tr>
          <td>{row.formatedDate}</td>
          <td>{user_html}</td>
          <td>{prefix_tpl}">{row.levelSet}: {row.level}</a></td>
          <td>{nMoves_tpl}</td>
          <td>{endpoint_html}</td>
          <th>{row.status_html}</th>
        </tr>
"""

    json_row_tpl = """\
  {{
    "levelSet": "{row.levelSet}",
    "level": "{row.level}",
    "user": "{row.user}",
    "moves": "{row.nMoves}",
    "history": "{row.history}",
    "date": "{row.date}",
    "status": {json_status}
  }}"""

    json_leadin_tpl = """[
  {{
    "source": "{app.siteURL}/{best}recent{url_user64}.json",
    "usage": "Use as you wish but a backlink to {app.siteURL}/ would be welcome."
  }}"""

    def dbGetData(self, ctx):

        app = ctx.app
        db = app.DB()

        sql = ''

        if ctx.user:
            sql += " user=:user"

        if ctx.bestOnly:
            if sql:
                sql += ' and'

            sql += """ (nextBest=="L") """

        if sql:
            sql = ' where ' + sql

        sql = "SELECT * FROM solutions" + sql + " order by date desc"

        if ctx.start:
            sql += " where rowid>(select rowid from solutions where uid=:start)"
        sql += " LIMIT :limit"

        db.execute(sql, ctx)
        items = [Ctx(r) for r in db.c]
        db.close()

        LX = ('L', 'X')
        for r in items:
            r.formatedDate = formatDate(r.date)
            r.user64 = toUrl64(r.user)
            r.isFirst = int(r.prevEp == 'F')
            r.isBest = int(r.nextBest == 'L')
            r.isPrev = int((not r.isFirst) and (r.nextBest not in LX))
            r.isBestEp = int((r.nextEp == 'L') and (not r.isBest))
            r.isPrevEp = int(not (r.isFirst or r.isPrev or (r.nextEp in LX)))

            r.status = []

            if r.isBest:
                r.status.append('best')

            if r.isPrev:
                r.status.append('prev')

            if r.isBestEp:
                r.status.append('ep')

            if r.isPrevEp:
                r.status.append('xep')

            if r.isFirst:
                r.status.append('first')

            r.status_html = '&nbsp;&nbsp;'.join(
                app.emblem(status) for status in r.status
            ) or '&nbsp;'

        ctx.rows = items

    def send_json(self, ctx):

        txt = fmt(ctx, self.json_leadin_tpl)
        lst = [txt]
        for row in ctx.rows:
            ctx.row = row
            if row.status:
                ctx.json_status = ('["' + '", "'.join(row.status) + '"]') \
                    .replace('"ep"', '"bestEp"') \
                    .replace('"xep"', '"prevEp"')
            else:
                ctx.json_status = '[]'
            item = fmt(ctx, self.json_row_tpl)
            lst.append(item)
        return ',\n'.join(lst) + "\n]\n"

    xml_leadin_tpl = """
<!-- Source: {app.siteURL}/recent.xml -->
<!-- Usage: Use as you wish but a backlink to {app.siteURL}/ would be welcome. -->

<recent>
"""

    def send_xml(self, ctx):

        text = []
        for ctx.row in ctx.rows:
            ctx.status = ', '.join(ctx.row.status)
            text.append(fmt(ctx, """
  <solution levelSet="{row.levelSet}" level="{row.level}" endpoint="{row.endpoint}">
    <user>{row.user}</user>
    <moves>{row.nMoves}</moves>
    <history>{row.history}</history>
    <date>{row.date}</date>
    <status>{status}</status>
  </solution>
"""))

        text.append("</recent>\n")

        return fmt(ctx, self.xml_leadin_tpl) + '\n'.join(text)

    def send_html(self, ctx):

        app = ctx.app
        page = ctx.page

        limit = ctx.limit

        if ctx.bestOnly:
            page.title = "best of recent solutions"
        else:
            page.title = "recent solutions"

        ctx.html = []

        if ctx.user64:
            user64 = '/' + ctx.user64
        else:
            user64 = ''

        target_t = app.link_prefix + '%s/recent' + user64

        allTarget = target_t % ''
        bestTarget = target_t % '/best'

        if ctx.bestOnly:
            prefix = app.link_prefix + 'best/recent/'
            ctx.user_target = bestTarget
            ctx.all_or_best_link = '<a href="%s.html?limit=%s">Show ALL recent solutions</a>' % (allTarget, limit)

        else:
            prefix = app.link_prefix + '/recent/'
            ctx.user_target = allTarget
            ctx.all_or_best_link = '<a href="%s.html?limit=%s">Show only BEST recent solutions</a>' % (bestTarget, limit)

        if not ctx.user:
            if ctx.bestOnly:
                prefix = app.link_prefix + '/best/recent/'
            else:
                prefix = app.link_prefix + '/recent/'

        for ctx.row in ctx.rows:

            if ctx.user:
                ctx.user_target = prefix + ctx.user64
            ctx.prefix_tpl = fmt(ctx, '<a rel="nofollow" href="{app.link_prefix}/?levelSet={row.levelSet}&amp;level={row.level}')
            ctx.nMoves_tpl = '{prefix_tpl}&amp;history={row.history}">{row.nMoves} Moves</a>'

            ctx.user_html = '&nbsp;'
            if ctx.row.user:
                ctx.user_html = '<a rel="nofollow" href="{app.link_prefix}/recent/{row.user64}.html?limit={limit}">{row.user}</a>'
            ctx.endpoint_html = '<a rel="nofollow" href="{app.link_prefix}/endpoints/{row.levelSet}/{row.level}.html">{row.endpoint}</a>'
            ctx.html.append(fmt(ctx, fmt(ctx, self.row_tpl)))

        ctx.html = '\n'.join(ctx.html)

        return fmt(ctx, self.table_tpl)

    def __call__(self, app, form, bestOnly, user64, kind):

        ctx = Ctx(
            app=app,
            asText=False,
            bestOnly=bestOnly,
            kind=kind,
            user64=user64,
            user='',
            start=form.get('start', ''),
            limit=form.get('limit', ''),
            csvHeaders=self.csvHeaders,
        )
        if bestOnly:
            ctx.best = 'best/'
        else:
            ctx.best = ''
        if not isint(ctx.limit):
            ctx.limit = self.RECENT_LIMIT
        if ctx.user64:
            ctx.url_user64 = '/' + ctx.user64
            ctx.user = fromUrl64(user64)
        else:
            ctx.url_user64 = ''

        self.dbGetData(ctx)

        return self.send_kind(kind, ctx)


_instance = PgRecent()


def run(*args):
    return _instance(*args)
