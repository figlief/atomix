from __future__ import unicode_literals

import re

import cgi

import sqlite3
from .figlief import (
    fmt,
    Ctx,
    Template,
)


class AtomixException(Exception):
    pass


class Error404(AtomixException):
    pass


from .utils import *  # noqa

from .pgbaseclass import PgBaseClass  # noqa

from . import (
    pg_solutions,
    pg_endpoints,
    pg_recent,
    pg_users,
    pg_index,
    pg_backup,
    action_submit,
)


pageTemplate = Template("""\
<!doctype html>

<html>
  <head>
    <meta charset="utf-8" />
    <title>{app.site_title_prefix} - {title}</title>
    <link
       href="{app.link_prefix}/css/atomix.css?{app.version}"
       rel="stylesheet" type="text/css" />
    {head_scripts}
    {extra_head_content}
  </head>
  <body>
    {pre_headbar}
    <!-- start headbar -->
    {headbar}
    <!-- end headbar -->

    <!-- start contents -->
    {contents}
    <!-- end contents -->

    <!-- start after-main-->
    <div id="after-main">
        {pre_footer}
        <!-- start footer -->
        {footer}
        <!-- end footer -->
        {post_footer}
    </div>
    <!-- end after-main -->

    <!-- start tail_scripts -->
    {tail_scripts}
    <!-- end tail scripts -->

  </body>
</html>
""")

solutionPageLink = Template("""\
<a href="{app.link_prefix}/solutions/{level}.html"
id="show-solutions-{level}">{level}</a>""")

headbarTemplate = Template("""
<div id="headbar">

  <a href="{app.link_prefix}/">Home</a>
  &nbsp;&nbsp;

  <a href="{app.link_prefix}/users.html">Users</a>
  &nbsp;&nbsp;

  <a href="{app.link_prefix}/recent.html">Recent</a>

  &nbsp;&nbsp;&nbsp;
  Solutions for: {links}
</div>
""")


footerTemplate = """
<div class="center" id="footer">
  <hr />
  <a href="http://code.google.com/p/kp-atomix/downloads/list">kp-atomix</a>,
  a free javascript version of the
  <a href="http://kde.org/"> kde</a>
  game
  <a href="http://games.kde.org/game.php?game=katomic">
    katomic
  </a>

  <br />
  Hosted by
  <a href="https://www.pythonanywhere.com/details/hosting">
  <b>Python Anywhere</b></a>
  &nbsp;&nbsp;&nbsp;
  Inspired by
  <a href="http://cross-browser.com/">Cross-Browser.com</a>
  <br />
</div>
"""


class DB:

    app = None

    def __init__(self, db='', log=None):
        self.db = self.app.dbName
        self.con = None
        self.c = None
        self._log = log
        self.log('Init %s' % self)

    def log(self, msg):
        self._log and self._log('%s <DB %s>' % (msg, self.db))

    def connect(self):
        if self.con is None:
            self.log('connecting to {'+self.db+'}')
        else:
            self.log('Already conected.')
            return

        self.con = sqlite3.connect(self.db)
        self.con.row_factory = sqlite3.Row
        self.c = self.con.cursor()
        self.log('Connected')
        return

    def execute(self, *args):
        self.connect()
        self.log('Execute')
        return self.c.execute(*args)

    def commit(self):
        self.log('Commit')
        return self.con.commit()

    def fetchone(self, *args):
        self.connect()
        return self.c.fetchone(*args)

    def close(self):
        if self.con is None:
            self.log('Try to close ')
            return
        self.log('Close')
        retval = self.con.close()
        self.con = None
        return retval


class Page(object):

    def __contains__(self, key):
        return hasattr(self, str(key))

    def __getitem__(self, key):
        return getattr(self, str(key))

    def __init__(self, app, contentType='text/plain'):

        self.app = app

        self.httpStatus = 200
        self.contentType = contentType

        self.httpHeaders = []
        self.body = []

        self.contents = ''
        self.heads = ''

    def composePage(self):
        self.contents = '\n'.join(self.body) + '\n'

    def compose(self):

        self.composePage()

        return self.httpHeaders, self.contents

    def appendBody(self, *args):
        self.body.extend(args)


class JsonPage(Page):
    def __init__(self, app):
        Page.__init__(self, app, 'application/json')


class TxtPage(Page):
    pass


class XmlPage(Page):
    def __init__(self, app):
        Page.__init__(self, app, 'text/xml')


class CsvPage(Page):
    pass


class HtmlPage(Page):

    def __init__(self, app):

        Page.__init__(self, app)
        self.contentType = 'text/html'

        self.title = ''
        self.head_scripts = ''
        self.extra_head_content = ''

        self.pre_headbar = ''
        self.headbar = ''

        self.pre_footer = ''
        self.footer = footerTemplate
        self.post_footer = ''

        self.tail_scripts = ''

    def composePage(self):

        self.headbar = self.composeHeadbar()

        self.contents = '\n'.join(self.body)
        self.contents = fmt(self, pageTemplate)

    def composeHeadbar(self):

        app = self.app
        links = []

        for level in sorted(app.allowedLevelSets):
            links.append(fmt(locals(), solutionPageLink))
        links = '&nbsp;&nbsp;'.join(links)

        return fmt(locals(), headbarTemplate)

default_cfg = Ctx(
    DB=DB,
    allowedDebugParams=['date', 'uid', 'n', 'ip'],
    allowedLevelSets=('katomic', 'original', 'pack1', 'mystery', 'draknek'),
    allowedParams=['user', 'history', 'levelSet', 'level', 'sRand'],
    browserTitle='',
    dbName='',
    debugCode='',
    indexFile='',
    link_prefix='',
    log=None,
    maxHistoryLength=4000,
    maxUserLength=32,
    reHistory=re.compile('^(?:[a-z][a-z][a-z][a-z])+$'),
    siteURL='',
    site_root='',
    site_title_prefix='Atomix Online',
    useCombined=1,
    useCompressed=0,
    version='666',
)


class Atomix(object):

    def __init__(self, config):

        cfg = default_cfg.copy()
        cfg.update(config)

        for k, v in cfg.items():
            setattr(self, k, v)

        self.DB.app = self

    def send(self, page):
        heads, content = page.compose()
        # sys.stdout.write('\r\n'.join([heads, content]).encode('utf8'))
        return heads, content

    def appendBody(self, page, *args):
        page.body.extend(args)

    def isMe(self):
        return False

    def jsonPage(self):
        return JsonPage(self)

    def htmlPage(self):
        return HtmlPage(self)

    def xmlPage(self):
        return XmlPage(self)

    def txtPage(self):
        return TxtPage(self)

    def csvPage(self):
        return CsvPage(self)

    def json_or_text(self, asText):
        if asText:
            return self.txtPage()
        return self.jsonPage()

    def error404(self, url, msg=''):
        msg = 'Not Found: %s' % url
        page = self.htmlPage()
        page.httpStatus = 404
        page.appendBody('<h1>%s</h1>' % self.escape(msg))
        return page

    def escape(self, s=''):
        return cgi.escape(s).encode('ascii', 'xmlcharrefreplace')

    def main(self, url=None, form=None):
        self.url = url
        if form is None:
            form = {}
        try:
            page = self.do_url(url, form)
        except Error404:
            page = self.error404(url)

        return page

    def do_url(self, url, form):

        def match(s):
            # self.log('search: %s\nurl: %s\n\n'%(s, self.url))
            return re.match(s, url)

        m = match('/$')
        if m:
            return pg_index.run(self, form)

        m = match(ur'/backup\.csv$')
        if m:
            return pg_backup.run(self, form)

        m = match('/(submit-solution|node/save-move)$')
        if m:
            return action_submit.run(self, form)

        m = match(ur'/users\.(html|json|xml|txt|csv)$')
        if m:
            return pg_users.run(self, form, m.group(1))

        m = match(ur'(/best)?/recent\.(html|json|xml|txt|csv)$')
        if m:
            # bestOnly, usr64, kind
            return pg_recent.run(self, form, m.group(1), '', m.group(2))

        m = match(ur'(/best)?/recent/([^.]+)\.(html|json|xml|txt|csv)$')
        if m:
            # bestOnly, user64, kind
            return pg_recent.run(self, form, *m.groups())

        m = match(ur'/solutions-katomic\.html$')
        if m:
            return m
            self.do_301(self.siteURL + 'solutions/katomic.html')
            return

        m = match(ur'/solutions/([^/\.]+)\.(html|json|xml|txt|csv)$')
        if m:
            name, kind = m.groups()
            return pg_solutions.run(self, form, name, kind)

        m = match(ur'/endpoints/([^/]+)/([^/]+)(?:/([a-z][a-z]))?\.(html)$')
        if m:
            # levelSet, level, ep, kind = m.groups()
            return pg_endpoints.run(self, form, *m.groups())

        return self.error404(url)

    def getTailScripts(self, ctx):
        app = ctx.app

        script_tpl = """\
<script src="{0.app.link_prefix}/{1}.js?{0.app.version}"></script>"""

        if app.useCombined or app.useCompressed:
            if app.useCompressed:
                s = 'xjs/combined.min'
            else:
                s = 'xjs/combined'

            return script_tpl.format(ctx, s)

        scripts = ['js/atomix', 'js/game', 'js/dialog', 'xjs/xlib']
        scripts.extend(list('levels/' + l for l in app.allowedLevelSets))

        return '\n'.join([
            script_tpl.format(app, ss)
            for ss in scripts
        ])

    def emblem(self, emblem, title=False):
        tpl = """<img src="%s/images/emblem-%s.png" alt="%s" />"""
        return tpl % (self.link_prefix, emblem.lower(), emblem)
