from os import (
    listdir,
)
from os.path import (
    join,
    isdir,
)


class RelPath(list):
    def __str__(self):
        ls = len(self)
        if ls == 0:
            return ''
        elif ls == 1:
            return self[0]
        else:
            return join(*self)


def onerror(err):
    raise err


def walk_tree(top, _path=None, name='', onerror=onerror, signal=None):
    """Directory tree generator."""
    if _path is None:
        path = RelPath()
    else:
        path = RelPath(_path)

    if name:
        path.append(name)

    target = join(top, str(path))
    try:
        names = listdir(target)
    except StandardError as err:
        onerror(err)
        return

    dirs, files = [], []
    for name in names:
        if isdir(join(top, str(path), name)):
            dirs.append(name)
        else:
            files.append(name)

    args = (path, dirs, files)
    if signal:
        signal.send('enter', args=args)
    else:
        yield 'enter', args

    for name in files:
        yield 'file', (path, name)

    for name in dirs:
        for x in walk_tree(top, path, name):
            yield x

    args = (path,)
    if signal is not None:
        signal.send('exit', args=args)
    else:
        yield 'exit', args


if __name__ == "__main__":
    for x in walk_tree('.'):
        print x
