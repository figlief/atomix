from __future__ import unicode_literals

from collections import namedtuple

Field = namedtuple('Field', 'first, rest, spec, cSpec, bang, filters')


def _test_bang(v):
    return '<test>%s</test>' % str(v)


class Template(object):

    convertors = {
        '`': _test_bang
    }

    def __init__(self, template):

        self._compiled = self.compile(template)

    def compile(self, tpl):
        result = []
        for text, name, spec, bang in \
                tpl._formatter_parser():
            if text:
                result.append(text)
            if name:
                if not spec:
                    spec = ''
                res = self.compile_field(name, spec, bang)
                result.append(res)

        return result

    def compile_field(self, name, spec, bang):

        first, rest = name._formatter_field_name_split()
        rest = list(rest)
        filters = spec.split('::')
        cSpec = filters[0]
        if cSpec.startswith(':'):
            filters[0] = cSpec[1:]
            cSpec = ''
        else:
            filters = filters[1:]
        for idx, filter in enumerate(filters):
            tpl = Template('{%s}' % filter)
            filters[idx] = tpl._compiled[0]
        return Field._make((first, rest, spec, cSpec, bang, filters))

    def compose(self, ctx):

        result = self._compiled[:]

        for idx, field in enumerate(result):
            if not isinstance(field, Field):
                continue

            if field.first not in ctx:
                continue

            obj = self.get_field(ctx, field)
            result[idx] = unicode(obj)

        return result

    def squash(self):
        cp = self._compiled
        res = []
        tmp = []
        for i in cp:
            if isinstance(i, Field):
                if tmp:
                    res.append(''.join(tmp))
                    res.append(i)
                    tmp = []

            else:
                tmp.append(i)
        if tmp:
            res.append(''.join(tmp))
        cp[:] = res

        return self

    def partial(self, ctx):
        result = self.compose(ctx)
        tpl = Template('')
        tpl._compiled = result
        tpl.squash()
        return tpl

    def format_field(self, ctx, value, field):
        for sFilter in field.filters:
            filter = ctx[sFilter]
            value = filter(ctx, value)
        return value

    def convert_field(self, value, field):
        if field.bang == 'r':
            return repr(value)
        elif field.bang == 's':
            return str(value)
        elif field.bang in self.convertors:
            return self.convertors[field.bang](value)
        elif field.bang is None:
            return value
        raise ValueError("Unknown bang specifier {0!s}".format(field.bang))

    def get_field(self, ctx, field):

        obj = ctx[field.first]

        for is_attr, i in field.rest:
            if is_attr:
                obj = getattr(obj, i)
            else:
                obj = obj[i]

        if field.bang:
            obj = self.convert_field(obj, field)
        if field.cSpec:
            obj = format(obj, field.cSpec)
        for f in field.filters:
            obj = self.get_filtered(ctx, f, obj)
        return obj

    def get_filtered(self, ctx, field, value):

        obj = ctx[field.first]

        for is_attr, i in field.rest:
            if is_attr:
                obj = getattr(obj, i)
            else:
                obj = obj[i]
        obj = obj(ctx, value)
        if field.bang:
            obj = self.convert_field(obj, field)
        if field.cSpec:
            obj = format(obj, field.cSpec)
        return obj


def partial(ctx, template):
    if isinstance(template, Template):
        return template.partial()
    return Template(template).squash()


def fmt(ctx, template):
    if not isinstance(template, Template):
        template = Template(template)
    res = template.compose(ctx)
    # for i in res:
    #     if isinstance(i, unicode):
    #         continue
    #     print i.first
    return ''.join(res)
