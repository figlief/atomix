class Ctx(dict):

    def copy(self):
        return self.__class__(self)

    def __getattr__(self, k):

        try:
            return object.__getattribute__(self, k)
        except AttributeError:
            return dict.__getitem__(self, k)

    __getitem__ = __getattr__

    def __setattr__(self, k, v):
        dict.__setitem__(self, k, v)

    def __repr__(self):
        keys = sorted(self.keys())
        args = ', '.join('%s=%r' % (key, self[key]) for key in keys)
        return '%s(%s)' % (self.__class__.__name__, args)
