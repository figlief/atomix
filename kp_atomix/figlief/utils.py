
def bRead(path):
    with open(path, 'rb') as fp:
        return fp.read()


def uRead(path):
    return unicode(bRead(path))


def bWrite(path, text):
    with open(path, 'wb') as fp:
        fp.write(text)


def uWrite(path, text):
    bWrite(path, text.encode('utf-8'))
