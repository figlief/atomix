if(!KP_ATOMIX){var KP_ATOMIX={}}

(function () {

    var CELL_HEIGHT = 39,
        CELL_WIDTH = 41,

        gCellHeight = CELL_HEIGHT,
        gCellWidth = CELL_WIDTH,

        MOLECULE_CELL_HEIGHT = 39,
        MOLECULE_CELL_WIDTH = 41,

        gMoleculeCellHeight = MOLECULE_CELL_HEIGHT,
        gMoleculeCellWidth = MOLECULE_CELL_WIDTH,

        // time for atom to move one square (in milliseconds)
        ANIMATION_TIME = 100,
        gAnimationTime = ANIMATION_TIME,

        browserTitle = 'kpAtomix Online',
        gControls = null,
        gUserName = 'anonymous',
        gAjaxRequest,

        gLevelSets = {}, // list of LevelSet objects derived from KP_ATOMIX.levelSets
        gLevelSetNames = [],
        gLevelSet = null,  // currently active LevelSet object
        gGame = null,

        dlgSuccess,
        dlgBookmark,
        dlgAjax,
        log = function() {},

        gameList = [],

        site_prefix = '',

        //
        $;

    function foreach(c, f) {
        for (var i = 0; i < c.length; i += 1) {
            f(c[i], i, c);
        }
    }
    function forkey(c, f) {
        for (var k in c) {
            if (c.hasOwnProperty(k)) {
                f(c[k], k, c);
            }
        }
    }
    function keys(c) {
        var k, a = [];
        for (k in c) {
            if (c.hasOwnProperty(k)) {
                a.push(k);
            }
        }
        return a;
    }
    function format(s) {
        var count = 0, args = arguments;
        return s.replace(/\f/g, function () {
            count += 1;
            return (count < args.length) ? args[count] : '';
        });
    }

    function parse_location_search() {

        var items = location.search
            , result = {}
        ;

        if (!(items.length)) {
            return result;
        }
        foreach(items.substring(1).split('&'), function (item) {
            var value = item.split('=', 2)
                , name = decodeURIComponent(value[0])
            ;
            if (value.length === 2) {
                value = decodeURIComponent(value[1]);
            } else {
                value = '';
            }
            result[name] = value;
        });
        return result;
    }

    function addClickLink(cmd) {
        xAddEventListener($(cmd), 'click', function (e) {
            xCancel(e);
            gControls[cmd]();
        }, false);
        xAddEventListener($(cmd), 'dblclick', function (e) {
            xCancel(e);
            gControls[cmd]();
        }, false);
    }

    function onNextAtom(dir) {

        switch (dir) {

        case 'down':
            break;
        case 'up':
            break;
        case 'left':
            break;
        case 'right':
            break;
        }
    }

    function onKeydown(evt)
    {
        var e = new xEvent(evt),
            doneKey = true;

        if (kp_Shield.isUp()) {
            return;
        }

        function dok(a, b) {
            if (e.shiftKey && b) {
                gControls[b]();
            }
            else {
                gControls[a]();
            }
        }

        function doa(d) {
            if (e.shiftKey) {
                //onNextAtom(d);
            } else {
                onClickArrow('arrow-' + d);
            }
        }

        switch (e.keyCode) {

            case 85: //u
                dok('history-undo', 'history-redo');
                break;
            case 82: //r
                dok('history-redo', '');
                break;
            case 78: //n
                dok('next-level', '');
                break;
            case 80: //p
                dok('prev-level', '');
                break;
            case 66: //b
                dok('bigger-link', '');
                break;
            case 83: //s
                dok('smaller-link', 'bookmark-link');
                break;

            case 74: //j
                doa('left');
                break;
            case 75: //k
                doa('down');
                break;
            case 76:  //l
                doa('right');
                break;
            case 73:  //i
                doa('up');
                break;

            default:
               doneKey = false;
        }
        if (doneKey) {
            xPreventDefault(evt);
        }
    }

    function create_levelset_selectors() {

        $('levelset-selector-span').innerHTML  =
              '<select id="levelset-select"><option>'
            + gLevelSetNames.join('</option><option>')
            + '</option></select>';

        xAddEventListener($('levelset-select'), 'change', function () {
           setTimeout(onLevelSetSelect, 100);
        }, false);
        return;
    }
    function onLevelSetSelect() {

        var sel = $('levelset-select')
          , name = sel.options[sel.selectedIndex].text
        ;
        select_levelSet(name);
        start_level(gLevelSet.iLevel);
    }

    function create_query_string() {
        var sData = []
            , enc = encodeURIComponent
            , user
        ;
        function add(key, data) {
            if (data.length) {
                sData.push(enc(key) + '=' + enc(data));
            }
        }
        user = dlgSuccess.get('user').value;
        user = (user.toLowerCase().indexOf('anon') === 0) ? '' : user;

        add('user', user);
        add('levelSet', gLevelSet.name);
        add('level', '' + gLevelSet.level.id);
        add('history', gGame.getEncodedHistory());
        //add('undo', encode_history(gg.redo));

        return sData.join('&');
    }

    function update_bookmark_link() {

        var bm = $('bookmark-link')
            , href
        ;
        href = location.protocol + '//' + location.host + location.pathname;
        href += '?' + create_query_string();
        document.title = format('\f \f: \f', gLevelSet.name, gLevelSet.iLevel + 1, browserTitle);
        bm.href = href;
    }

    function send_solution_home(fnResponse) {
        // a solution will be saved via ajax

        var sData, response;
        sData = create_query_string();

        response = gAjaxRequest.send(
            'GET',
            site_prefix + 'submit-solution',
            sData,
            60000, // uTimeout milliseconds
            '', // sData +  '&' + sRndVar + '=' + a_random_number.
            false, // bXml
            null, // a user data object
            function (req, status, data) {
                var text = req.responseText;
                //alert(text);
                fnResponse(text, status);
            }
        );
    }

    function on_complete_level() {
        show_success_dialog(
            format(
                "You completed this level <br /> in <b>\f</b> moves.",
                gGame.gg.history.length
            )
        );
    }

    function setup_controls() {
        gControls = {
            'next-level': do_next_level
          , 'prev-level': do_prev_level
          , 'history-reset': do_history_reset
          , 'history-undo': do_history_undo
          , 'history-redo': do_history_redo
          , 'history-first': do_history_first
          , 'history-last': do_history_last
          , 'bookmark-link': do_bookmark_link

          , 'bigger-link': do_bigger_link
          , 'smaller-link': do_smaller_link
        };
        foreach(keys(gControls), addClickLink);
    }

        function do_bigger_link() {
            gCellHeight += 2;
            gCellWidth += 2;
            gMoleculeCellHeight += 2;
            gMoleculeCellWidth += 2;
            start_level(gLevelSet.iLevel);
        }

        function do_smaller_link() {
            gCellHeight = Math.max(6, gCellHeight - 2);
            gCellWidth = Math.max(6, gCellHeight - 2);
            gMoleculeCellHeight = Math.max(6, gMoleculeCellHeight - 2);
            gMoleculeCellWidth = Math.max(6, gMoleculeCellWidth -2);
            start_level(gLevelSet.iLevel);
        }

        function do_bookmark_link() {
            show_bookmark_dialog();
        }

        function do_next_level() {
            var l = gLevelSet.levels.length - 1;
            if (gLevelSet.iLevel < l) {
                $('level-select-' + gLevelSet.name).selectedIndex = gLevelSet.iLevel + 1;
                start_level(gLevelSet.iLevel + 1);
            }
        }

        function do_prev_level() {
            if (gLevelSet.iLevel > 0) {
                $('level-select-' + gLevelSet.name).selectedIndex = gLevelSet.iLevel - 1;
                start_level(gLevelSet.iLevel - 1);
            }
        }

        function do_history_reset() {
            start_level(gLevelSet.iLevel, true);
        }

        function do_history_undo(repeat) {
            gGame.historyUndo();
        }
        function do_history_redo(repeat) {
            gGame.historyRedo();
        }

        function do_history_first() {
            gGame.historyFirst();
        }

        function do_history_last() {
            gGame.historyLast();
        }

    function show_level_data() {
        gLevelSet.level.gameData = gGame.gg;
        $('move-no').innerHTML = format(
            '<b>(Move: \f )</b>',
            gGame.gg.history.length
        );
        update_bookmark_link();
    }

    function select_levelSet(name, setControl) {

        var i, select, options;

        if (setControl === true) {
            select = $('levelset-select');
            options = select.options;
            for (i = 0; i < options.length; i++) {
                if (options[i].text === name) {
                    select.selectedIndex = i;
                    break;
                }
            }
        }

        if (!(name in gLevelSets)) {
            name = gLevelSetNames[0];
            $('levelset-select').selectedIndex = 0;
        }
        gLevelSet = gLevelSets[name];

        $('levelset-credits-credit').innerHTML = gLevelSet.credits;
        $('levelset-credits-license').innerHTML = gLevelSet.license;
        $('levelset-credits-name').innerHTML = format(
            '<a href="\flevels/\f.json">\f</a>',
            site_prefix,
            gLevelSet.name,
            gLevelSet.name
        );

        forkey(gLevelSets, function (levelSet) {
            levelSet.show_selector(name);
        });
    }

    function start_level(lvl, reset) {

        lvl = lvl || 0;

        var oLevel = gLevelSet.select_level(lvl, reset)
          , gg = null
        ;
        if (!reset && oLevel.gameData)  {
            gg = oLevel.gameData;
        }
        gGame.reset_level(gLevelSet.level, gg);

    }

    function show_bookmark_dialog(fnCallback) {

        var dlg = dlgBookmark
          , get = function (s) {return dlg.get(s);}
          , link = get('link')
          , blink = $('bookmark-link')
          , btnClose = get('button-close')
        ;

        if (btnClose) {
            btnClose.onclick = function () {
                dlg.hide();
            };
            link.href = blink.href;
            link.innerHTML = gLevelSet.name + ' ' + (gLevelSet.iLevel + 1);
            dlg.show();
        }
    }

    function show_success_dialog(sMsgHtml) {

        var dlg = dlgSuccess
          , get = function (s) {return dlg.get(s);}
          , btnSave = get('button-save')
          , btnClose = get('button-close')
        ;

        if (btnSave && btnClose) {
            get('message').innerHTML = sMsgHtml;
            btnSave.onclick = function () {
                dlg.hide();
                show_ajax_dialog();
            };
            btnClose.onclick = function () {
                dlg.hide();
            };
            dlg.show();
        }
    }

    function show_ajax_dialog() {

        var dlg = dlgAjax
          , get = function (s) {return dlg.get(s);}
          , btnClose = get('button-close')
          , oTitle = get('title')
          , oMsg = get('message')
        ;

        btnClose.onclick = function () {
            dlg.hide();
        };

        oMsg.innerHTML = 'Contacting server ...';
        oTitle.innerHTML = 'Submitting Solution';

        dlg.show();

        send_solution_home(function (text, status) {
            if (status) {
                text = '<p><b>Sorry</b>. Failed to contact server</p>';
            }
            oTitle.innerHTML = 'Solution Accepted';
            oMsg.innerHTML =  text;
            dlg.center();
        });

    }

    function parse_query() {

        var query = parse_location_search()
            , level
        ;

        if (!('levelSet' in query && query.levelSet in gLevelSets)) {
            query.levelSet = 'katomic';
        }
        level = query.level;

        query.level = 0;
        if (level && /^\d+$/.test(level)) {
            query.level = parseInt(level, 10) - 1;
        }
        return query;
    }

    // LevelSetClass

    function LevelSet(levelSet) {

        var self = Object.create(LevelSetClass);

        self.name = levelSet.name;
        self.credits = levelSet.credits;
        self.license = levelSet.license;
        self.levels = levelSet.levels;
        self.iLevel = 0;

        self.create_selector();
        gLevelSetNames.push(self.name);
        gLevelSets[self.name] = self;

        return self;
    }
    var LevelSetClass = {

        keys: [],
        selector: null,
        myid: '',

        getName: function () {
            return this.name;
        },

        create_selector: function () {

            if (this.selector) {
                return;
            }

            var level
              , select
            ;
            select = ['<select class="hide-selector" id="level-select-' + this.name + '">'];

            for (level = 0; level < this.levels.length;) {
                level += 1;
                select.push(
                    '<option value="' + level
                    + '">Level ' + level
                    + ': '
                    + this.levels[level - 1].name
                );
            }
            this.selector = 'level-select-' + this.name;
            $('level-selector-span').innerHTML += select.join('') + "</select>";

            return;
        },

        bind_selector: function (self) {

            xAddEventListener($(self.selector), 'change', function () {
                setTimeout(function () {
                    self.onLevelSelect();
                }, 100);
            }, false);
        },

        onLevelSelect: function () {
            start_level($(this.selector).selectedIndex);
        },

        show_selector: function (name) {

            var selector = $(this.selector);
            if (selector) {
                if (name && (this.name === name || this === name)) {
                    xRemoveClass(selector, 'hide-selector');
                    xAddClass(selector, 'show-selector');
                }
                else {
                    xRemoveClass(selector, 'show-selector');
                    xAddClass(selector, 'hide-selector');
                }
            }
        },

        select_level: function (lvl, reset) {

            lvl = lvl || 0;
            if (lvl >= this.levels.length) {
                lvl = 0;
            }
            this.iLevel = lvl;
            this.level = this.levels[lvl];

            $(this.selector).selectedIndex = lvl;

            return this.level;
        }
    };


    function init_endpoints(config) {

        site_prefix = config.site_prefix || '';

        $ = xGetElementById;

        var e = xGetElementsByTagName('div')
          , m
          , i
          , ii
          , gGame
          , games = []
          , eplevel = $('endpoints-level').innerHTML.split('`')
          , levelSet = eplevel[0]
          , level = parseInt(eplevel[1], 10) - 1
        ;

        foreach(e, function(ee) {
            if (ee.id.match(/arena-../)) {
                games.push(ee);
            }
        });

        i = 0;
        ii = games.length;

        function draw_arenas() {
            var ee = games[i];
            var gGame = KP_ATOMIX.Game(site_prefix + 'images/');
            gameList.push(gGame);
            gGame.setCellSize(27, 27);
            gGame.noEvents();

            gGame.init(ee.id, '',
                KP_ATOMIX.levelSets[levelSet].levels[level],
                $('ep-' + ee.id).innerHTML
            );
            gGame.hideArrows();
            i += 1;
            if (i < ii) {
                setTimeout(draw_arenas, 10);
            }
        }
        setTimeout(draw_arenas, 10);
    }

    function init(config) {

        site_prefix = config.site_prefix || '';
        browserTitle = config.browserTitle || browserTitle;

        $ = xGetElementById;

        xEnableDrag('molecule');
        xEnableDrag('arena', xCancel, xCancel, xCancel);

        dlgSuccess = kp_ModalDialog('success-dialog');
        dlgBookmark = kp_ModalDialog('bookmark-dialog');
        dlgAjax = kp_ModalDialog('ajax-dialog');

        gAjaxRequest = new xHttpRequest();
        dlgSuccess.get('user').value = gUserName;

        forkey(KP_ATOMIX.levelSets, function (levelSet) {
            LevelSet(levelSet);
        });

        forkey(gLevelSets, function (levelSet) {
            levelSet.bind_selector(levelSet);
        });

        setup_controls();
        create_levelset_selectors();

        gGame = KP_ATOMIX.Game(site_prefix + 'images/');

        gGame.onCompleteLevel = on_complete_level;
        gGame.onShowData = show_level_data;

        var query = parse_query();

        select_levelSet(query.levelSet, true);
        var oLevel = gLevelSet.select_level(query.level, true);

        gGame.init('arena', 'molecule', oLevel, query.history);


        xAddEventListener(document, 'keydown', onKeydown, false);

        start_level(query.level);

        xHeight('loading', 0);
    }

    (function() {
      if ((typeof Object.create) !== 'undefined') return;
      Object.create = function (o) {
        function F() {}
        F.prototype = o;
        return new F();
      };
    }());

    // Code in this section is automatically generated
    // Any changes will be lost on the next build

    
    /* My extensions to xlib */

function xBottom(e) {
    return xTop(e) + xHeight(e);
}

function xRight(e) {
    return xLeft(e) + xWidth(e);
}

function xCancel(e) {
    xStopPropagation(e)
    xPreventDefault(e)
}

var kp_Shield = (function(zMin, zIncr)  {

  zMin = zMin || 10000;
  zIncr = zIncr || 100;

  var zList = [0]
    , _shield = null
  ;

  function isUp() {
    return zList.length  > 1;
  }

  function shield() {
    return _shield || create();
  }

  function create() {
    if (_shield) return _shield;
    _shield = document.createElement('div');
    _shield.className = 'xShieldElement';
    document.body.appendChild(_shield);
    zIndex(zMin);
    return _shield;
  }

  function show(z) {
    var e = shield()
      , ds = xDocSize()
    ;
    zIndex(z);
    xMoveTo(e, 0, 0);
    xResizeTo(e,
      Math.max(ds.w, xClientWidth()),
      Math.max(ds.h, xClientHeight())
    );
  }

  function hide() {
    var e = shield();
    xResizeTo(e, 10, 10);
    xMoveTo(e, -10, -10);
  }

  function zNext(z) {
    return Math.max(zMin, z || 0, zList[0]) + zIncr;
  }

  function zIndex(z) {
    return kp_zIndex(shield(), z);
  }

  function grab(z) {
    z = zNext(z);
    zList.unshift(z);
    show(z);
    return z;
  }

  function release() {
    switch (zList.length) {
      case 0:
        zList = [0];
        break;
      case 1:
        break;
      default:
        (zList.shift());
        show(zList[0]);
    }
    if (zList.length < 2) {
      hide();
    }
    return zList.length - 1;
  }

  return {
    grab: grab,
    release: release,
    isUp: isUp
  };

}());

function kp_ModalDialog(sId) {

  addClass('xxModalDialog');

  function get(s) {
    return xGetElementById(sId + (s ? '-' + s : ''));
  }

  function addClass(c, s) {
    xAddClass(get(s), c);
  }

  function show() {
    zIndex(kp_Shield.grab() + 1);
    return center();
  }

  function hide() {
    var dialog = get();
    if (dialog) {
      xMoveTo(dialog, -xWidth(dialog), 0);
    }
    kp_Shield.release();
  }

  function center () {
    var dialog = get();
    dialog.style.height = 'auto';
    xCenter(dialog);
    return this;
  }

  function zIndex(z) {
    kp_zIndex(get(), z);
  }

  return {
    show: show,
    hide: hide,
    get: get,
    center: center
  };

}

function kp_zIndex(e, z) {
  if (!(
    (e = xGetElementById(e)) &&
    xDef(e.style, e.style.zIndex)
  )) {
    return 0;
  }
  if (xDef(z) && xNum(z)) {
      e.style.zIndex = z;
  }
  z = xGetComputedStyle(e, 'zIndex', 1);
  return isNaN(z) ? 0 : z;
}


KP_ATOMIX.Game = function (image_path) {

  var log = function() {}

    , item_kind = {
      '1': 'atom-h', // hydrogen
      '2': 'atom-c', // carbon
      '3': 'atom-o', // oxygen
      '4': 'atom-n', // nitrogen
      '5': 'atom-s', // sulphur
      '6': 'atom-f', // fluorine
      '7': 'atom-cl', // chlorine
      '8': 'atom-br', // bromine
      '9': 'atom-p', // phosphorus
      'o': 'atom-crystal',
      'A': 'connector-horizontal',
      'B': 'connector-slash',
      'C': 'connector-vertical',
      'D': 'connector-backslash',

      'E': 'crystal-E',
      'F': 'crystal-F',
      'G': 'crystal-G',
      'H': 'crystal-H',
      'I': 'crystal-I',
      'J': 'crystal-J',
      'K': 'crystal-K',
      'L': 'crystal-L',
      'M': 'crystal-M'
    },
    bond_kind = {
      'a': 'bond-top',
      'b': 'bond-top-right',
      'c': 'bond-right',
      'd': 'bond-bottom-right',
      'e': 'bond-bottom',
      'f': 'bond-bottom-left',
      'g': 'bond-left',
      'h': 'bond-top-left',
      'A': 'bond-top-double',
      'B': 'bond-right-double',
      'C': 'bond-bottom-double',
      'D': 'bond-left-double',
      'E': 'bond-top-triple',
      'F': 'bond-right-triple',
      'G': 'bond-bottom-triple',
      'H': 'bond-left-triple',
      '1': 'bond-top-left-double',
      '2': 'bond-top-right-double',
      '3': 'bond-bottom-right-double',
      '4': 'bond-bottom-left-double'
    },

    moveEncoder = 'abcdefghijklmnopqrstuvwxyz'.split(''),
    moveDecoder = {},

    gMoveFlag = false,
    gCurrent = null,
    gItems = null,
    gArrows = null,
    sArrows = ['arrow-left', 'arrow-right', 'arrow-up', 'arrow-down'],

    gAnimationTime = 100,
    gA = null,
    gM = null,

    gCellWidth = 41,
    gCellHeight = 39,
    gMoleculeCellWidth = 41,
    gMoleculeCellHeight = 39,

    idArena = '',
    idMol = '',
    gLevel = null,

    noEvents = false,

    self = Object.create(KP_ATOMIX.GameClass),

    $ = xGetElementById
  ;

    function init(arena, mol, level, history) {

        self.idArena = idArena = arena;
        self.idMol = idMol = mol;

        self.onInit(arena, mol, level, history||null);

        gLevel = level;
        init_data(history);
        reset_level(level, self.gg);

    }

    function init_data(history) {

        var gg = {};

        self.gg = gg;

        gg.grid = copy_grid(gLevel.arena);

        gg.molecule = gLevel.molecule.join(
            gg.grid[0].replace(/./g, '.').substring(gLevel.molecule[0].length)
        );
        gg.history = [];
        gg.redo = [];

        if (history) {
          history = decode_history(history);
          replay_moves(history, gg.grid);
        }
        gg.history = history || [];

    }

    function reset_level(level, data) {

        gLevel = level;

        if (data) {
            self.gg = data;
        } else {
            init_data();
        }

        gItems = [];
        gArrows = [];

        gA = gridSpec(idArena, gCellWidth, gCellHeight);
        gA.clear_container();

        if (idMol && $(idMol)) {
          gM = gridSpec(idMol,
              gMoleculeCellWidth, gMoleculeCellHeight
          );
          gM.clear_container();
        }

        gCurrent = null;

        draw_arena();
        show_arrows();

        self.onShowData(self.gg);

    }

    function foreach(c, f) {
        for (var i = 0; i < c.length; i += 1) {
            f(c[i], i, c);
        }
    }

    function copy_grid(grid) {
        return grid.join('\n').split('\n');
    }

    function end_animation(testForSuccess) {

        self.onEndAnimation(gCurrent);
        gMoveFlag = false;
        if (testForSuccess) {
            test_for_success();
        } else {
            show_arrows();
        }
    }

    function show_arrow(arrow, row, col) {
        if (self.gg.grid[row].charAt(col) === '.') {
            xMoveTo(arrow, gA.xpos(col), gA.ypos(row));
        } else {
            xLeft(arrow, -1000);
        }
    }

    function show_arrows() {

        var row = gCurrent.row, col = gCurrent.col;

        show_arrow(gArrows[0], row, col - 1);
        show_arrow(gArrows[1], row, col + 1);
        show_arrow(gArrows[2], row - 1, col);
        show_arrow(gArrows[3], row + 1, col);
    }

    function hide_arrows() {
        foreach(gArrows, function (a, i) {
            xLeft(a, -1000);
        });
    }

    function create_arrows() {
        var arrows = ''
          , idArrow
        ;
        foreach(sArrows, function (dir) {
            idArrow = dir + '`' + idArena;
            arrows += gA.new_img('arrow',  dir, -1, idArrow);
        });
        return arrows;
    }

    function get_item_rowcol(row, col) {
        var i, item;
        for (i = 0; i < gItems.length; i += 1) {
            item = gItems[i];
            if (item.row === row && item.col === col) {
                return item;
            }
        }
        return null;
    }

    function set_current(oAtom) {
        gCurrent = oAtom;
        show_arrows(oAtom);
    }

    function encode_history(history) {
        var i, m, e, s = '';
        e = moveEncoder;
        for (i = 0; i < history.length; i += 1) {
            m = history[i];
            s = s + e[m[0]] + e[m[1]] + e[m[2]] + e[m[3]];
        }
        return s;
    }
    function decode_history(s) {
        var i, d, history = [];
        d = moveDecoder;
        for (i = 0; i < s.length;) {
            history.push([d[s[i++]], d[s[i++]], d[s[i++]], d[s[i++]]]);
        }
        return history;
    }

    function create_move_decoder(i) {
        foreach(moveEncoder, function (s) {
            moveDecoder[s] = i++;
        });
    }
    create_move_decoder(0);


    // Modify a grid to reflect a sequence of moves.
    // Throws an Error if the move sequence is invalid.
    // Returns the modified grid otherwise.

    var InvalidMoveError = function (msg) {
        this.prototype = Error.prototype;
        this.name = "InvalidMoveError";
        this.message = "Invalid Move: " + msg + '.';
    };

    function _assert(p, msg) {
        if (p) {
            return;
        }
        throw new InvalidMoveError(msg);
    }
    function replay_moves(moveList, grid) {

        var endCol
          , endRow
          , startCol
          , startRow
          , row
          , col
          , data
          , atom
          , move
          , moveIdx
        ;

        for (moveIdx = 0; moveIdx < moveList.length; moveIdx += 1) {

            move = moveList[moveIdx];
            startRow = row = move[0];
            startCol = col = move[1];
            endRow = move[2];
            endCol = move[3];

            atom = grid[startRow][startCol];
            _assert(atom !==  '.' && atom !== '#', 'no atom here');

            data = grid[row];

            if (startRow === endRow) {
                _assert(startCol !== endCol, 'no movement');
                if (endCol < startCol) {
                    while (data[col - 1] === '.') col -= 1;
                } else {
                    while (data[col + 1] === '.') col += 1;
                }
            } else {
                _assert(startCol === endCol, 'diagonal move');
                if (endRow < startRow) {
                    while (grid[row - 1][col] === '.') row -= 1;
                } else {
                    while (grid[row + 1][col] === '.') row += 1;
                }
            }
            _assert(row === endRow && col === endCol, 'path blocked');
            move_in_grid(grid, startRow, startCol, endRow, endCol);
        }
        return grid;
    }

    function isSolution(testGrid) {
        var grid = testGrid.join('').replace(/#/g, '.');
        return (grid.indexOf(self.gg.molecule) !== -1);
    }

    function onClickAtom(oAtom) {
        set_current(oAtom);
    }

    function onClickArrow(dir) {

        if (gMoveFlag || self.onArrowClicked(gCurrent, dir.split('-')[1]) ) {
            return;
        }

        var row = gCurrent.row
          , col = gCurrent.col
          , cr = row
          , cc = col
          , grid = self.gg.grid
          , data = grid[row]
        ;

        if (!xStr(dir)) {
          dir = $(dir + '`' + idArena).id;
        }

        switch (dir) {

        case 'arrow-left':
            while (data.charAt(col - 1) === '.') {
                col -= 1;
            }
            break;
        case 'arrow-right':
            while (data.charAt(col + 1) === '.') {
                col += 1;
            }
            break;
        case 'arrow-up':
            while (grid[row - 1].charAt(col) === '.') {
                row -= 1;
            }
            break;
        case 'arrow-down':
            while (grid[row + 1].charAt(col) === '.') {
                row += 1;
            }
            break;

        default:
            break;
        }

        if (row !== gCurrent.row || col !== gCurrent.col) {
            self.gg.history.push([cr, cc, row, col]);
            self.gg.redo = [];
            move_current_atom(row, col, true);
        }
    }

    function move_in_grid(grid, sr, sc, er, ec) {
        var atom = grid[sr].charAt(sc);
        grid[sr] = grid[sr].slice(0, sc) + '.' + grid[sr].slice(sc + 1);
        grid[er] = grid[er].slice(0, ec) + atom + grid[er].slice(ec + 1);
    }

    function move_current_atom(row, col, testForSuccess, repeat) {

        var grid = self.gg.grid
          , startCol = gCurrent.col
          , startRow = gCurrent.row
          , animationTime
        ;

        gMoveFlag = true;

        hide_arrows();

        move_in_grid(grid, startRow, startCol, row, col);
        gCurrent.row = row;
        gCurrent.col = col;

        self.onShowData(self.gg);

        animationTime = gAnimationTime * Math.abs(startRow - row + startCol - col);

        if (repeat) {
            xAniLine(gCurrent.atom,
                gA.xpos(col),
                gA.ypos(row),
                10, 1,
                function () {
                    end_animation(false);
                    setTimeout(repeat, 10);
                }
            );
        }
        else {
            xAniLine(gCurrent.atom,
                gA.xpos(col),
                gA.ypos(row),
                animationTime, 1,
                function () {
                    end_animation(testForSuccess);
                }
            );
        }
    }

    function gridSpec(parentName, cellWidth, cellHeight) {

        var parent = $(parentName)
            , atomCount = -1
            , wh = 'width:' + cellWidth + 'px;height:' + cellHeight + 'px;'
        ;


        function xpos(col) {
            return col * cellWidth;
        }

        function ypos(row) {
            return row * cellHeight;
        }

        function new_img(cls, image, col, row) {

            var id;

            if (col < 0) {
                id = row ? ' id="' + row + '"' : '';
                col = 0;
                row = 0;
            } else {
                id = '';
                col = xpos(col || 0);
                row = ypos(row || 0);
            }

            return '<img'
                + id +  ' src="'
                + image_path
                + image  + '.png" class="'
                + cls + '" style="left:'
                + col + 'px;top:'
                + row  + 'px;'
                + wh  + '" />'
            ;
        }

        function atom_factory(atom_type, col, row) {

            var spec = gLevel.atoms[atom_type]
              , sAtom
              , bonds
              , bond
            ;

            atomCount += 1;
            sAtom = '<div id="atom-'
                + parentName + '-' + atomCount
                + '" class="atom" style="left:'
                + xpos(col) + 'px;top:'
                + ypos(row) + 'px;'
                + wh + '">';

            bonds = spec[1];
            for (bond = 0; bond < bonds.length; bond += 1) {
                sAtom += new_img('bond', bond_kind[bonds.charAt(bond)], -1);
            }
            sAtom += new_img('atom', item_kind[spec[0]], -1);

            return sAtom + '</div>';
        }

        function set_container_size(col, row) {
            xResizeTo(parent,
                col * cellWidth,
                row * cellHeight
            );
        }

        function clear_container() {
            parent.innerHTML = '&nbsp;';
        }

        return {
            parent: parent,
            xpos: xpos,
            ypos: ypos,
            cellWidth: cellWidth,
            cellHeight: cellHeight,
            atom_factory: atom_factory,
            new_img: new_img,
            set_container_size: set_container_size,
            clear_container: clear_container
        };
    }

    function onAtomSelected(e, target, data) {
        if (!gMoveFlag && !self.onAtomSelected(data)){
            target(data);
        }
    }

    function addClickAtom(e, target, oAtom) {

        if (noEvents) return;

        function onAtomSelected() {
            if (!gMoveFlag && !self.onAtomSelected(oAtom)){
                set_current(oAtom);
                xCancel();
            }
        }

        xAddEventListener(e, 'mouseover', onAtomSelected, false);
        xAddEventListener(e, 'click', onAtomSelected, false);
    }

    function addClickArrow(e, target, data) {
        if (noEvents) return;
        xAddEventListener(e, 'mousedown', function (evt) {
            target(data);
        }, false);
    }

    function draw_arena() {

        var item
          , mol
          , row
          , col
          , sArena = []
          , sMolecule = []
          , grid = self.gg.grid
        ;

        sArena.push(create_arrows());

        for (row = 0 ; row < grid.length; row += 1) {
            item = grid[row];
            for (col = 0; col < item.length; col += 1) {

                switch (item.charAt(col)) {
                case '#':
                    sArena.push(gA.new_img('wall', 'wall', col, row));
                    break;
                case '.':
                    break;
                default:
                    sArena.push(gA.atom_factory(item.charAt(col), col, row));
                    gItems.push({'row': row, 'col': col});
                }
            }
        }
        gA.parent.innerHTML = sArena.join('');
        gA.set_container_size(col, row);

        foreach(gItems, function (oAtom, i) {
            oAtom.atom = $('atom-' + idArena + '-' + i);
            addClickAtom(oAtom.atom, onClickAtom, oAtom);
        });

        gArrows = [];
        foreach(sArrows, function (arrow) {
            oArrow = $(arrow + '`' + idArena);
            gArrows.push(oArrow);
            addClickArrow(oArrow, onClickArrow, arrow);
        });

        if (idMol) {
          mol = gLevel.molecule;
          for (row = 0 ; row < mol.length; row += 1) {
              item = mol[row];
              for (col = 0; col < item.length; col += 1) {
                  if (item.charAt(col) !== '.') {
                      sMolecule.push(gM.atom_factory(item.charAt(col), col, row));
                 }
             }
          }
          gM.parent.innerHTML = sMolecule.join('');
          gM.set_container_size(col, row);
        }
        set_current(gItems[0]);
        show_arrows();

    }

    function do_history_undo(repeat) {

        var gg = self.gg;

        if (!gg.history.length || gMoveFlag) {
            return;
        }
        var m = gg.history.pop();
        self.gg.redo.push(m);
        //m = decode_move(m);
        gCurrent = get_item_rowcol(m[2], m[3]);
        move_current_atom(m[0], m[1], false, repeat);
    }

    function do_history_redo(repeat) {

        var gg = self.gg;

        if (!gg.redo.length || gMoveFlag) {
            return;
        }
        var m = gg.redo.pop();
        gg.history.push(m);
        gCurrent = get_item_rowcol(m[0], m[1]);
        move_current_atom(m[2], m[3], false, repeat);
    }

    function do_history_first() {
        if (!self.gg.history.length || gMoveFlag) {
            return;
        }
        do_history_undo(do_history_first);
    }

    function do_history_last() {

        if (!self.gg.redo.length || gMoveFlag) {
            return;
        }
        do_history_redo(do_history_last);
    }

    function test_for_success() {
        if (isSolution(self.gg.grid)) {
            self.onCompleteLevel();
        }
        else {
            show_arrows();
        }

    }
    self.init = init;
    self.reset_level = reset_level;

    self.getEncodedHistory = function () {
        return encode_history(self.gg.history);
    };

    self.historyUndo = do_history_undo;
    self.historyRedo = do_history_redo;
    self.historyFirst = do_history_first;
    self.historyLast = do_history_last;

    self.setCellSize = function(width, height) {
      gCellWidth = width;
      gCellHeight = height;
    };
    self.noEvents= function() {noEvents = true};

    self.hideArrows = hide_arrows;

    self.log = log;

    return self;

};

KP_ATOMIX.GameClass = {

    onInit: function(arena, mol, level, history) {
        //this.log('atomix.onInit %s, %s, %o, %o', this.idArena, this.idMol, level||null, history||null);
    },

    onAtomSelected: function(oAtom) {
        //this.log('atomix.atomSelected(%s): %o', this.idArena, oAtom);
    },

    onArrowClicked: function(oAtom, direction) {
        //this.log('atomix.arrowClicked(%s): %o, %s', this.idArena, oAtom, direction);
    },

    onCompleteLevel:  function() {
        //this.log('atomix.onCompleteLevel(%s): ', this.idArena);
    },

    onEndAnimation: function(oAtom) {
        //this.log('atomix.endAnimation(%s): %o', this.idArena, oAtom);
    },

    onShowData: function(gg) {
        //this.log('atomix.showData(%s): %o', this.idArena, gg);
    }
};

// xAddClass r3, Copyright 2005-2007 Daniel Frechette - modified by Mike Foster
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xAddClass(e, c)
{
  if ((e=xGetElementById(e))!=null) {
    var s = '';
    if (e.className.length && e.className.charAt(e.className.length - 1) != ' ') {
      s = ' ';
    }
    if (!xHasClass(e, c)) {
      e.className += s + c;
      return true;
    }
  }
  return false;
}

// xAddEventListener r8, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xAddEventListener(e,eT,eL,cap)
{
  if(!(e=xGetElementById(e)))return;
  eT=eT.toLowerCase();
  if(e.addEventListener)e.addEventListener(eT,eL,cap||false);
  else if(e.attachEvent)e.attachEvent('on'+eT,eL);
  else {
    var o=e['on'+eT];
    e['on'+eT]=typeof o=='function' ? function(v){o(v);eL(v);} : eL;
  }
}

// xAniLine r1, Copyright 2006-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xAniLine(e, x, y, t, a, oe)
{
  if (!(e=xGetElementById(e))) return;
  var x0 = xLeft(e), y0 = xTop(e); // start positions
  x = Math.round(x); y = Math.round(y);
  var dx = x - x0, dy = y - y0; // displacements
  var fq = 1 / t; // frequency
  if (a) fq *= (Math.PI / 2);
  var t0 = new Date().getTime(); // start time
  var tmr = setInterval(
    function() {
      var et = new Date().getTime() - t0; // elapsed time
      if (et < t) {
        var f = et * fq; // constant velocity
        if (a == 1) f = Math.sin(f); // sine acceleration
        else if (a == 2) f = 1 - Math.cos(f); // cosine acceleration
        f = Math.abs(f);
        e.style.left = Math.round(f * dx + x0) + 'px'; // instantaneous positions
        e.style.top = Math.round(f * dy + y0) + 'px';
      }
      else {
        clearInterval(tmr);
        e.style.left = x + 'px'; // target positions
        e.style.top = y + 'px';
        if (typeof oe == 'function') oe(); // 'onEnd' handler
        else if (typeof oe == 'string') eval(oe);
      }
    }, 10 // timer resolution
  );
}

// xCamelize r1, Copyright 2007-2009 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xCamelize(cssPropStr)
{
  var i, c, a, s;
  a = cssPropStr.split('-');
  s = a[0];
  for (i=1; i<a.length; ++i) {
    c = a[i].charAt(0);
    s += a[i].replace(c, c.toUpperCase());
  }
  return s;
}

// xCenter r1, Copyright 2009 Arthur Blake (http://arthur.blake.name)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
/**
 * Center a positioned element within the current client window space.
 * 
 * If w,h not specified, then the existing width and height of e are used.
 * 
 * @param e an existing absolutely positioned dom element (or an id to such an element)
 * @param w (optional) width to resize element to
 * @param h (optional) height  to resize element to
 */
function xCenter(e, w, h)
{
  var ww=xClientWidth(),wh=xClientHeight(),x=0,y=0;
  e = xGetElementById(e);
  if (e)
  {
    w = w || xWidth(e);
    h = h || xHeight(e);

    if (ww < w)
    {
      w = ww;
    }
    else
    {
      x = (ww - w) / 2;
    }
    if (wh < h)
    {
      h = wh;
    }
    else
    {
      y = (wh - h) / 2;
    }

    // adjust for any scrolling
    x += xScrollLeft();
    y += xScrollTop();
    
    xResizeTo(e, w, h);
    xMoveTo(e, x, y);
  }
}

// xClientHeight r6, Copyright 2001-2008 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xClientHeight()
{
  var v=0,d=document,w=window;
  if((!d.compatMode || d.compatMode == 'CSS1Compat') /* && !w.opera */ && d.documentElement && d.documentElement.clientHeight)
    {v=d.documentElement.clientHeight;}
  else if(d.body && d.body.clientHeight)
    {v=d.body.clientHeight;}
  else if(xDef(w.innerWidth,w.innerHeight,d.width)) {
    v=w.innerHeight;
    if(d.width>w.innerWidth) v-=16;
  }
  return v;
}

// xClientWidth r5, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xClientWidth()
{
  var v=0,d=document,w=window;
  if((!d.compatMode || d.compatMode == 'CSS1Compat') && !w.opera && d.documentElement && d.documentElement.clientWidth)
    {v=d.documentElement.clientWidth;}
  else if(d.body && d.body.clientWidth)
    {v=d.body.clientWidth;}
  else if(xDef(w.innerWidth,w.innerHeight,d.height)) {
    v=w.innerWidth;
    if(d.height>w.innerHeight) v-=16;
  }
  return v;
}

// xDef r2, Copyright 2001-2011 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xDef()
{
  for (var i=0, l=arguments.length; i<l; ++i) {
    if (typeof(arguments[i]) === 'undefined')
      return false;
  }
  return true;
}

// xDocSize r1, Copyright 2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xDocSize()
{
  var b=document.body, e=document.documentElement;
  var esw=0, eow=0, bsw=0, bow=0, esh=0, eoh=0, bsh=0, boh=0;
  if (e) {
    esw = e.scrollWidth;
    eow = e.offsetWidth;
    esh = e.scrollHeight;
    eoh = e.offsetHeight;
  }
  if (b) {
    bsw = b.scrollWidth;
    bow = b.offsetWidth;
    bsh = b.scrollHeight;
    boh = b.offsetHeight;
  }
//  alert('compatMode: ' + document.compatMode + '\n\ndocumentElement.scrollHeight: ' + esh + '\ndocumentElement.offsetHeight: ' + eoh + '\nbody.scrollHeight: ' + bsh + '\nbody.offsetHeight: ' + boh + '\n\ndocumentElement.scrollWidth: ' + esw + '\ndocumentElement.offsetWidth: ' + eow + '\nbody.scrollWidth: ' + bsw + '\nbody.offsetWidth: ' + bow);
  return {w:Math.max(esw,eow,bsw,bow),h:Math.max(esh,eoh,bsh,boh)};
}

// xEnableDrag r8, Copyright 2002-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xEnableDrag(id,fS,fD,fE)
{
  var mx = 0, my = 0, el = xGetElementById(id);
  if (el) {
    el.xDragEnabled = true;
    xAddEventListener(el, 'mousedown', dragStart, false);
  }
  // Private Functions
  function dragStart(e)
  {
    if (el.xDragEnabled) {
      var ev = new xEvent(e);
      xPreventDefault(e);
      mx = ev.pageX;
      my = ev.pageY;
      xAddEventListener(document, 'mousemove', drag, false);
      xAddEventListener(document, 'mouseup', dragEnd, false);
      if (fS) {
        fS(el, ev.pageX, ev.pageY, ev);
      }
    }
  }
  function drag(e)
  {
    var ev, dx, dy;
    xPreventDefault(e);
    ev = new xEvent(e);
    dx = ev.pageX - mx;
    dy = ev.pageY - my;
    mx = ev.pageX;
    my = ev.pageY;
    if (fD) {
      fD(el, dx, dy, ev);
    }
    else {
      xMoveTo(el, xLeft(el) + dx, xTop(el) + dy);
    }
  }
  function dragEnd(e)
  {
    var ev = new xEvent(e);
    xPreventDefault(e);
    xRemoveEventListener(document, 'mouseup', dragEnd, false);
    xRemoveEventListener(document, 'mousemove', drag, false);
    if (fE) {
      fE(el, ev.pageX, ev.pageY, ev);
    }
    if (xEnableDrag.drop) {
      xEnableDrag.drop(el, ev);
    }
  }
}

xEnableDrag.drops = []; // static property

// xEvent r11-a, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
// Modified by kp
function xEvent(evt) // object prototype
{
  var e = evt || window.event;
  if (!e) return;
  this.type = e.type;
  this.target = e.target || e.srcElement;

  if (xDef(e.pageX)) {
    this.pageX = e.pageX;
    this.pageY = e.pageY;
  }
  else if (xDef(e.clientX)) {
    this.pageX = e.clientX + xScrollLeft();
    this.pageY = e.clientY + xScrollTop();
  }
  if (xDef(e.offsetX)) {
    this.offsetX = e.offsetX;
    this.offsetY = e.offsetY;
  }
  else {
    this.offsetX = this.pageX - xPageX(this.target);
    this.offsetY = this.pageY - xPageY(this.target);
  }

  this.keyCode = e.keyCode || e.which || 0;
  this.shiftKey = e.shiftKey;
  this.ctrlKey = e.ctrlKey;
  this.altKey = e.altKey;

  if (typeof e.type == 'string') {

    switch (e.type.toLowerCase()) {
      case 'mouseover':
      case 'onmouseover':
        this.relatedTarget = e.relatedTarget || e.fromElement
      case 'mouseout':
      case 'onmouseout':
        this.relatedTarget = e.relatedTarget || e.toElement;
      case 'default':
        this.realatedTarget = null;
      }

    if (e.type.indexOf('click') != -1) {
      this.button = 0;
    }
    else if (e.type.indexOf('mouse') != -1) {

      if (e.which) {
        this.button = e.which;
      } else {
        switch (e.button) {
          case 2:
            this.button = 3;
            break;
          case 4:
            this.button = 2;
            break;
          default:
            this.button = 1;
        }
      }
    }
  }
}

// xGetComputedStyle r7, Copyright 2002-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xGetComputedStyle(e, p, i)
{
  if(!(e=xGetElementById(e))) return null;
  var s, v = 'undefined', dv = document.defaultView;
  if(dv && dv.getComputedStyle){
    s = dv.getComputedStyle(e,'');
    if (s) v = s.getPropertyValue(p);
  }
  else if(e.currentStyle) {
    v = e.currentStyle[xCamelize(p)];
  }
  else return null;
  return i ? (parseInt(v) || 0) : v;
}


// xGetElementById r2, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xGetElementById(e)
{
  if (typeof(e) == 'string') {
    if (document.getElementById) e = document.getElementById(e);
    else if (document.all) e = document.all[e];
    else e = null;
  }
  return e;
}

// xGetElementsByTagName r5, Copyright 2002-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xGetElementsByTagName(t,p)
{
  var list = null;
  t = t || '*';
  p = xGetElementById(p) || document;
  if (typeof p.getElementsByTagName != 'undefined') { // DOM1
    list = p.getElementsByTagName(t);
    if (t=='*' && (!list || !list.length)) list = p.all; // IE5 '*' bug
  }
  else { // IE4 object model
    if (t=='*') list = p.all;
    else if (p.all && p.all.tags) list = p.all.tags(t);
  }
  return list || [];
}

// xHasClass r3, Copyright 2005-2007 Daniel Frechette - modified by Mike Foster
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xHasClass(e, c)
{
  e = xGetElementById(e);
  if (!e || e.className=='') return false;
  var re = new RegExp("(^|\\s)"+c+"(\\s|$)");
  return re.test(e.className);
}

// xHeight r8, Copyright 2001-2010 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xHeight(e,h)
{
  var css, pt=0, pb=0, bt=0, bb=0;
  if(!(e=xGetElementById(e))) return 0;
  if (xNum(h)) {
    if (h<0) h = 0;
    else h=Math.round(h);
  }
  else h=-1;
  css=xDef(e.style);
  if (e == document || e.tagName.toLowerCase() == 'html' || e.tagName.toLowerCase() == 'body') {
    h = xClientHeight();
  }
  else if(css && xDef(e.offsetHeight) && xStr(e.style.height)) {
    if(h>=0) {
      if (document.compatMode=='CSS1Compat') {
        pt=xGetComputedStyle(e,'padding-top',1);
        if (pt !== null) {
          pb=xGetComputedStyle(e,'padding-bottom',1);
          bt=xGetComputedStyle(e,'border-top-width',1);
          bb=xGetComputedStyle(e,'border-bottom-width',1);
        }
        // Should we try this as a last resort?
        // At this point getComputedStyle and currentStyle do not exist.
        else if(xDef(e.offsetHeight,e.style.height)){
          e.style.height=h+'px';
          pt=e.offsetHeight-h;
        }
      }
      h-=(pt+pb+bt+bb);
      if(isNaN(h)||h<0) return;
      else e.style.height=h+'px';
    }
    h=e.offsetHeight;
  }
  else if(css && xDef(e.style.pixelHeight)) {
    if(h>=0) e.style.pixelHeight=h;
    h=e.style.pixelHeight;
  }
  return h;
}

// xHttpRequest r11, Copyright 2006-2011 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xHttpRequest() // object prototype
{
  // Private Properties
  var
    _i = this, // instance object
    _r = null, // XMLHttpRequest object
    _t = null, // timer
    _f = null, // callback function
    _x = false, // XML response pending
    _o = null, // user data object passed to _f
    _c = false; // self-clean after send() completed?
  // Public Properties
  _i.OK = 0;
  _i.NOXMLOBJ = 1;
  _i.REQERR = 2;
  _i.TIMEOUT = 4;
  _i.RSPERR = 8;
  _i.NOXMLCT = 16;
  _i.ABORTED = 32;
  _i.status = _i.OK;
  _i.error = null;
  _i.busy = false;
  // Private Methods
  function _clean()
  {
    _i = null;
    _r = null;
    _t = null;
    _f = null;
    _x = false;
    _o = null;
    _c = false;
  }
  function _clrTimer()
  {
    if (_t) {
      clearTimeout(_t);
    }
    _t = null;
  }
  function _endCall()
  {
    if (_f) {
      _f(_r, _i.status, _o);
    }
    _f = null; _x = false; _o = null;
    _i.busy = false;
    if (_c) {
      _clean();
    }
  }
  function _abort(s)
  {
    _clrTimer();
    try {
      _r.onreadystatechange = function(){};
      _r.abort();
    }
    catch (e) {
      _i.status |= _i.RSPERR;
      _i.error = e;
    }
    _i.status |= s;
    _endCall();
  }
  function _newXHR()
  {
    try { _r = new XMLHttpRequest(); }
    catch (e) { try { _r = new ActiveXObject('Msxml2.XMLHTTP'); }
    catch (e) { try { _r = new ActiveXObject('Microsoft.XMLHTTP'); }
    catch (e) { _r = null; _i.error = e; }}}
    if (!_r) { _i.status |= _i.NOXMLOBJ; }
  }
  // Private Event Listeners
  function _oc() // onReadyStateChange
  {
    var ct;
    if (_r.readyState == 4) {
      _clrTimer();
      try {
        if (_r.status != 200) _i.status |= _i.RSPERR;
        if (_x) {
          ct = _r.getResponseHeader('Content-Type');
          if (ct && ct.indexOf('xml') == -1) { _i.status |= _i.NOXMLCT; }
        }
        delete _r['onreadystatechange']; // _r.onreadystatechange = null;
      }
      catch (e) {
        _i.status |= _i.RSPERR;
        _i.error = e;
      }
      _endCall();
    }
  }
  function _ot() // onTimeout
  {
    _t = null;
    _abort(_i.TIMEOUT);
  }
  // Public Methods
  this.send = function(m, u, d, t, r, x, o, f, c)
  {
    var q, ct;
    if (!_r || _i.busy) { return false; }
    _c = (c ? true : false);
    m = m.toUpperCase();
    q = (u.indexOf('?') >= 0);
    if (m != 'POST') {
      if (d) {
        u += (q ? '&' : '?') + d;
        q = true;
      }
      d = null;
    }
    if (r) {
      u += (q ? '&' : '?') + r + '=' + Math.random();
    }
    _x = (x ? true : false);
    _o = o;
    _f = f;
    _i.busy = true;
    _i.status = _i.OK;
    _i.error = null;
    if (t) { _t = setTimeout(_ot, t); }
    try {
      _r.open(m, u, true);
      if (m == 'GET') {
        _r.setRequestHeader('Cache-Control', 'no-cache');
        ct = 'text/' + (_x ? 'xml':'plain');
        if (_r.overrideMimeType) {_r.overrideMimeType(ct);}
        _r.setRequestHeader('Content-Type', ct);
      }
      else if (m == 'POST') {
        _r.setRequestHeader('Method', 'POST ' + u + ' HTTP/1.1');
        _r.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      }
      _r.onreadystatechange = _oc;
      _r.send(d);
    }
    catch(e) {
      _clrTimer();
      _f = null; _x = false; _o = null;
      _i.busy = false;
      _i.status |= _i.REQERR;
      _i.error = e;
      if (_c) {
        _clean();
      }
      return false;
    }
    return true;
  };
  this.abort = function()
  {
    if (!_r || !_i.busy) { return false; }
    _abort(_i.ABORTED);
    return true;
  };
  this.reinit = function()
  {
    // Halt any HTTP request that may be in progress.
    this.abort();
    // Set all private vars to initial state.
    _clean();
    _i = this;
    // Set all (non-constant) public properties to initial state.
    _i.status = _i.OK;
    _i.error = null;
    _i.busy = false;
    // Create the private XMLHttpRequest object.
    _newXHR();
    return true;
  };
  // Constructor Code
  _newXHR();
}

// xLeft r2, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xLeft(e, iX)
{
  if(!(e=xGetElementById(e))) return 0;
  var css=xDef(e.style);
  if (css && xStr(e.style.left)) {
    if(xNum(iX)) e.style.left=iX+'px';
    else {
      iX=parseInt(e.style.left);
      if(isNaN(iX)) iX=xGetComputedStyle(e,'left',1);
      if(isNaN(iX)) iX=0;
    }
  }
  else if(css && xDef(e.style.pixelLeft)) {
    if(xNum(iX)) e.style.pixelLeft=iX;
    else iX=e.style.pixelLeft;
  }
  return iX;
}

// xMoveTo r1, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xMoveTo(e,x,y)
{
  xLeft(e,x);
  xTop(e,y);
}

// xNum r3, Copyright 2001-2011 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xNum()
{
  for (var i=0, l=arguments.length; i<l; ++i) {
    if (isNaN(arguments[i]) || typeof(arguments[i]) !== 'number')
      return false;
  }
  return true;
}

// xPageX r2, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xPageX(e)
{
  var x = 0;
  e = xGetElementById(e);
  while (e) {
    if (xDef(e.offsetLeft)) x += e.offsetLeft;
    e = xDef(e.offsetParent) ? e.offsetParent : null;
  }
  return x;
}

// xPageY r4, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xPageY(e)
{
  var y = 0;
  e = xGetElementById(e);
  while (e) {
    if (xDef(e.offsetTop)) y += e.offsetTop;
    e = xDef(e.offsetParent) ? e.offsetParent : null;
  }
  return y;
}

// xPreventDefault r1, Copyright 2004-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xPreventDefault(e)
{
  if (e && e.preventDefault) e.preventDefault();
  else if (window.event) window.event.returnValue = false;
}

// xRemoveClass r3, Copyright 2005-2007 Daniel Frechette - modified by Mike Foster
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xRemoveClass(e, c)
{
  if(!(e=xGetElementById(e))) return false;
  e.className = e.className.replace(new RegExp("(^|\\s)"+c+"(\\s|$)",'g'),
    function(str, p1, p2) { return (p1 == ' ' && p2 == ' ') ? ' ' : ''; }
  );
  return true;
}

// xRemoveEventListener r6, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xRemoveEventListener(e,eT,eL,cap)
{
  if(!(e=xGetElementById(e)))return;
  eT=eT.toLowerCase();
  if(e.removeEventListener)e.removeEventListener(eT,eL,cap||false);
  else if(e.detachEvent)e.detachEvent('on'+eT,eL);
  else e['on'+eT]=null;
}

// xResizeTo r2, Copyright 2001-2009 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xResizeTo(e, w, h)
{
  return {
    w: xWidth(e, w),
    h: xHeight(e, h)
  };
}

// xScrollLeft r4, Copyright 2001-2009 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xScrollLeft(e, bWin)
{
  var w, offset=0;
  if (!xDef(e) || bWin || e == document || e.tagName.toLowerCase() == 'html' || e.tagName.toLowerCase() == 'body') {
    w = window;
    if (bWin && e) w = e;
    if(w.document.documentElement && w.document.documentElement.scrollLeft) offset=w.document.documentElement.scrollLeft;
    else if(w.document.body && xDef(w.document.body.scrollLeft)) offset=w.document.body.scrollLeft;
  }
  else {
    e = xGetElementById(e);
    if (e && xNum(e.scrollLeft)) offset = e.scrollLeft;
  }
  return offset;
}

// xScrollTop r4, Copyright 2001-2009 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xScrollTop(e, bWin)
{
  var w, offset=0;
  if (!xDef(e) || bWin || e == document || e.tagName.toLowerCase() == 'html' || e.tagName.toLowerCase() == 'body') {
    w = window;
    if (bWin && e) w = e;
    if(w.document.documentElement && w.document.documentElement.scrollTop) offset=w.document.documentElement.scrollTop;
    else if(w.document.body && xDef(w.document.body.scrollTop)) offset=w.document.body.scrollTop;
  }
  else {
    e = xGetElementById(e);
    if (e && xNum(e.scrollTop)) offset = e.scrollTop;
  }
  return offset;
}

// xStopPropagation r1, Copyright 2004-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xStopPropagation(evt)
{
  if (evt && evt.stopPropagation) evt.stopPropagation();
  else if (window.event) window.event.cancelBubble = true;
}

// xStr r2, Copyright 2001-2011 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xStr(s)
{
  for (var i=0, l=arguments.length; i<l; ++i) {
    if (typeof(arguments[i]) !== 'string')
      return false;
  }
  return true;
}

// xTop r2, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xTop(e, iY)
{
  if(!(e=xGetElementById(e))) return 0;
  var css=xDef(e.style);
  if(css && xStr(e.style.top)) {
    if(xNum(iY)) e.style.top=iY+'px';
    else {
      iY=parseInt(e.style.top);
      if(isNaN(iY)) iY=xGetComputedStyle(e,'top',1);
      if(isNaN(iY)) iY=0;
    }
  }
  else if(css && xDef(e.style.pixelTop)) {
    if(xNum(iY)) e.style.pixelTop=iY;
    else iY=e.style.pixelTop;
  }
  return iY;
}

// xWidth r8, Copyright 2001-2010 Michael Foster (Cross-Browser.com)
// Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
function xWidth(e,w)
{
  var css, pl=0, pr=0, bl=0, br=0;
  if(!(e=xGetElementById(e))) return 0;
  if (xNum(w)) {
    if (w<0) w = 0;
    else w=Math.round(w);
  }
  else w=-1;
  css=xDef(e.style);
  if (e == document || e.tagName.toLowerCase() == 'html' || e.tagName.toLowerCase() == 'body') {
    w = xClientWidth();
  }
  else if(css && xDef(e.offsetWidth) && xStr(e.style.width)) {
    if(w>=0) {
      if (document.compatMode=='CSS1Compat') {
        pl=xGetComputedStyle(e,'padding-left',1);
        if (pl !== null) {
          pr=xGetComputedStyle(e,'padding-right',1);
          bl=xGetComputedStyle(e,'border-left-width',1);
          br=xGetComputedStyle(e,'border-right-width',1);
        }
        // Should we try this as a last resort?
        // At this point getComputedStyle and currentStyle do not exist.
        else if(xDef(e.offsetWidth,e.style.width)){
          e.style.width=w+'px';
          pl=e.offsetWidth-w;
        }
      }
      w-=(pl+pr+bl+br);
      if(isNaN(w)||w<0) return;
      else e.style.width=w+'px';
    }
    w=e.offsetWidth;
  }
  else if(css && xDef(e.style.pixelWidth)) {
    if(w>=0) e.style.pixelWidth=w;
    w=e.style.pixelWidth;
  }
  return w;
}


    KP_ATOMIX.init = init;
    KP_ATOMIX.init_endpoints = init_endpoints;

}());
/*
 * The contents of this file is derived from the KDE project "KAtomic"
 *
 * http://games.kde.org/game.php?game=katomic
 * http://www.gnu.org/licenses/gpl.html
 *
 * KAtomic is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * KAtomic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAtomic; see the file COPYING.  If not, write to
 * the Free Software Foundation, 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
*/

(function () {

  var levelSet =
//<levelSet>
{
  "name": "pack1",
  "credits": "<a href='http://kde-files.org/content/show.php?content=133477'>Andreas Wüst</a>",
  "license": "<a href='http://www.gnu.org/licenses/lgpl.html'>LGPL</a>",
  "levels": [
  {
    "id": "1",
    "name": "Adiponitrile",
    "atoms": {
      "1": ["1", "h"],
      "2": ["1", "d"],
      "3": ["2", "bdfh"],
      "4": ["2", "bH"],
      "5": ["2", "fE"],
      "6": ["4", "F"],
      "7": ["4", "G"]
    },
    "arena": [
      "#####...#####",
      "#.1.#...#...#",
      "#..######.#2#",
      "#..1#......##",
      "##...3.1#3..#",
      "#2#..#4..#..#",
      "#.....#.2...#",
      "###.5.1#..#3#",
      ".#..#.....###",
      ".#.6.#..#3#..",
      ".###....7##..",
      "...#2..#..#..",
      "...########.."
    ],
    "molecule": [
      "......7",
      "....2.5",
      "...2.3.",
      "..2.3.1",
      ".2.3.1.",
      "..3.1..",
      "64.1..."
    ]
  },
  {
    "id": "2",
    "name": "Guanin",
    "atoms": {
      "1": ["3", "C"],
      "2": ["C", ""],
      "3": ["1", "f"],
      "4": ["4", "fB"],
      "5": ["4", "egb"],
      "6": ["1", "h"],
      "7": ["2", "afB"],
      "8": ["2", "ebD"],
      "9": ["2", "gbC"],
      "a": ["2", "cfA"],
      "b": ["1", "a"],
      "c": ["4", "agd"],
      "d": ["4", "bD"],
      "e": ["1", "c"],
      "f": ["A", ""]
    },
    "arena": [
      ".############..",
      "##1..#2.....#..",
      "#.......#3..#..",
      "##..#4.....##..",
      ".#.....#5...###",
      ".#.#6.....#7..#",
      ".#....#8.....##",
      ".##9.....#a...#",
      ".#...#b.....#c#",
      ".#......#5....#",
      ".#..#d.....#e.#",
      "##.....#a.....#",
      "#..#e.....#f..#",
      "#.....######.##",
      "########...###."
    ],
    "molecule": [
      ".......3",
      "...1.48.",
      "...a9.2.",
      ".e5.afc.",
      "..7d...6",
      "e5......",
      ".b......"
    ]
  },
  {
    "id": "3",
    "name": "Kristall 1",
    "atoms": {
      "1": ["o", "bh"],
      "2": ["o", "ceh"],
      "3": ["o", "ce"],
      "4": ["o", "egb"],
      "5": ["o", "df"],
      "6": ["o", "ac"],
      "7": ["o", "fh"],
      "8": ["o", "acf"],
      "9": ["o", "bdfh"],
      "a": ["o", "eg"],
      "b": ["o", "agd"],
      "c": ["o", "ag"],
      "d": ["o", "bd"]
    },
    "arena": [
      ".....#.....",
      "....###....",
      "..###1###..",
      "..#22344#..",
      ".##5...6##.",
      "##78.9.2a##",
      ".##b...8##.",
      "..#8cb4b#..",
      "..###d###..",
      "....###....",
      ".....#....."
    ],
    "molecule": [
      "...5...",
      ".34.2a.",
      ".8b.8b.",
      "d..9..7",
      ".24.24.",
      ".6b.8c.",
      "...1..."
    ]
  },
  {
    "id": "4",
    "name": "Prolin",
    "atoms": {
      "1": ["1", "g"],
      "2": ["2", "acfh"],
      "3": ["1", "a"],
      "4": ["3", "C"],
      "5": ["2", "gbd"],
      "6": ["2", "cfA"],
      "7": ["2", "aceg"],
      "8": ["1", "e"],
      "9": ["1", "d"],
      "a": ["1", "b"],
      "b": ["4", "cfh"],
      "c": ["3", "eb"],
      "d": ["C", ""]
    },
    "arena": [
      "#############",
      "#1.#.1.2.#.3#",
      "##...#.#...##",
      "#.....#.....#",
      "#4.#5...6#.7#",
      "#...#...#...#",
      "##...#8#...##",
      "#...#...#...#",
      "#9.#a...b#.c#",
      "#.....#.....#",
      "##...#.#...##",
      "#7.#.d.3.#.8#",
      "#############"
    ],
    "molecule": [
      "..988.",
      ".4.271",
      ".65.d.",
      "c..b71",
      "3.a.3."
    ]
  },
  {
    "id": "5",
    "name": "Methionin",
    "atoms": {
      "1": ["1", "a"],
      "2": ["4", "ceh"],
      "3": ["3", "D"],
      "4": ["2", "agdh"],
      "5": ["1", "e"],
      "6": ["2", "aeB"],
      "7": ["1", "g"],
      "8": ["1", "c"],
      "9": ["5", "dh"],
      "a": ["1", "d"],
      "b": ["3", "ae"]
    },
    "arena": [
      "...##.....##...",
      "..#..#...#..#..",
      ".#.1..#.#..2.#.",
      "#.3#...#...#4.#",
      "#...#5.5.6#...#",
      ".#..4.....7..#.",
      "..#.........#..",
      "...#8..#..5#...",
      "..#.........#..",
      ".#..8.....5..#.",
      "#...#9.4.8#...#",
      "#.8#...#...#a.#",
      ".#.4..#.#..b.#.",
      "..#..#...#..#..",
      "...##.....##..."
    ],
    "molecule": [
      "a5......",
      "84......",
      "..95.5..",
      "..845b..",
      "...8463.",
      "....84..",
      "......27",
      "......1."
    ]
  },
  {
    "id": "6",
    "name": "Theobromin",
    "atoms": {
      "1": ["4", "ebh"],
      "2": ["1", "e"],
      "3": ["1", "g"],
      "4": ["3", "C"],
      "5": ["2", "aedf"],
      "6": ["1", "b"],
      "7": ["C", ""],
      "8": ["1", "h"],
      "9": ["2", "chC"],
      "a": ["1", "a"],
      "b": ["A", ""],
      "c": ["1", "f"],
      "d": ["2", "dfA"],
      "e": ["3", "B"],
      "f": ["1", "d"],
      "g": ["4", "hB"],
      "h": ["2", "acbf"],
      "i": ["2", "adD"],
      "j": ["4", "egb"]
    },
    "arena": [
      "#####...#####",
      "#.1.#####.2.#",
      "#3...4#.....#",
      "#...#.56#1..#",
      "##.#7....#.##",
      ".#...8....9#.",
      ".##...#.a.##.",
      ".#bc..d..e.#.",
      "##.#f...d#8##",
      "#...#.g.#...#",
      "#h..i.#....i#",
      "#...#####.j.#",
      "#####...#####"
    ],
    "molecule": [
      "..4...2c",
      "f.d...h3",
      ".1.9bj..",
      "ei.d.7..",
      "..1.gi..",
      "..5...8.",
      ".6a8...."
    ]
  },
  {
    "id": "7",
    "name": "Adipinsäurediamid",
    "atoms": {
      "1": ["2", "acfh"],
      "2": ["2", "aeB"],
      "3": ["1", "g"],
      "4": ["2", "cebh"],
      "5": ["3", "B"],
      "6": ["1", "d"],
      "7": ["1", "f"],
      "8": ["1", "e"],
      "9": ["1", "c"],
      "a": ["2", "egbh"],
      "b": ["2", "aeD"],
      "c": ["3", "D"],
      "d": ["1", "a"],
      "e": ["4", "ace"],
      "f": ["4", "aeg"],
      "g": ["2", "agbd"]
    },
    "arena": [
      "....#####....",
      "...#...1.#...",
      "..#2.###.3#..",
      ".#.4.5.....#.",
      "#.6#.#7.#86.#",
      "#.#.9.7.a.#.#",
      "#b#...#...#3#",
      "#.#c...d..#.#",
      "#...#e.#.#f.#",
      ".#d...8....#.",
      "..#..###..#..",
      "...#.9.g.#...",
      "....#####...."
    ],
    "molecule": [
      ".6887.",
      "6.1g.7",
      "9a..43",
      "5b..2c",
      "9f..e3",
      ".d..d."
    ]
  },
  {
    "id": "8",
    "name": "Leucin",
    "atoms": {
      "1": ["1", "d"],
      "2": ["2", "bdfh"],
      "3": ["1", "c"],
      "4": ["1", "h"],
      "5": ["2", "agdh"],
      "6": ["4", "agd"],
      "7": ["1", "f"],
      "8": ["1", "g"],
      "9": ["3", "B"],
      "a": ["1", "e"],
      "b": ["2", "ebD"],
      "c": ["1", "a"],
      "d": ["3", "ad"],
      "e": ["2", "cedh"]
    },
    "arena": [
      "...###.###...",
      "..#1#2#1.3#..",
      ".#..#......#.",
      "#4.#5#...#.6#",
      "#........7###",
      "#4....2..#.8#",
      ".#....#....#.",
      "#9.#..a....4#",
      "###b........#",
      "#3.#...#c#.2#",
      ".#......#..#.",
      "..#4.d#a#e#..",
      "...###.###..."
    ],
    "molecule": [
      "..1a...",
      "..35.7.",
      ".a1.2..",
      "36.2.e8",
      "..2.4c4",
      "9b.4...",
      ".d.....",
      "..4...."
    ]
  },
  {
    "id": "9",
    "name": "Kristall 2",
    "atoms": {
      "1": ["o", "h"],
      "2": ["o", "a"],
      "3": ["o", "acegbdfh"],
      "4": ["o", "f"],
      "5": ["o", "c"],
      "6": ["o", "g"],
      "7": ["o", "d"],
      "8": ["o", "b"],
      "9": ["o", "dh"],
      "a": ["o", "e"]
    },
    "arena": [
      "##.....##",
      "#########",
      ".#1.23.#.",
      ".#.45#6#.",
      ".#75428#.",
      ".#9#6a.#.",
      ".#3a8..#.",
      "#########",
      "##.....##"
    ],
    "molecule": [
      "7a4..",
      "536..",
      "829a4",
      "..536",
      "..821"
    ]
  },
  {
    "id": "10",
    "name": "Phenylalanin",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "cg"],
      "3": ["1", "e"],
      "4": ["1", "f"],
      "5": ["3", "C"],
      "6": ["1", "g"],
      "7": ["1", "a"],
      "8": ["1", "h"],
      "9": ["1", "b"],
      "a": ["2", "agbd"],
      "b": ["2", "ebD"],
      "c": ["2", "gdA"],
      "d": ["2", "acC"],
      "e": ["2", "cfA"],
      "f": ["2", "agC"],
      "g": ["2", "ehB"],
      "h": ["2", "aceg"],
      "i": ["4", "aef"],
      "j": ["2", "cegA"]
    },
    "arena": [
      "....#####....",
      "###.#.#.#.###",
      "#1###...###2#",
      "#34#.....#.5#",
      "#146........#",
      "#7#83.....#.#",
      "##99..#...a##",
      "#.#7.....b#c#",
      "#........def#",
      "#..#.....#gh#",
      "#i###...###j#",
      "###.#.#.#.###",
      "....#####...."
    ],
    "molecule": [
      "..5334..",
      "12jha..4",
      "..i7.gb.",
      ".97.1fd6",
      ".....ec.",
      "....9..8"
    ]
  },
  {
    "id": "11",
    "name": "Fruktose",
    "atoms": {
      "1": ["1", "h"],
      "2": ["2", "aceg"],
      "3": ["1", "c"],
      "4": ["3", "A"],
      "5": ["1", "f"],
      "6": ["3", "af"],
      "7": ["3", "eh"],
      "8": ["2", "aced"],
      "9": ["1", "g"],
      "a": ["2", "agbd"],
      "b": ["3", "gd"],
      "c": ["2", "acgh"],
      "d": ["1", "e"],
      "e": ["1", "b"],
      "f": ["3", "cf"],
      "g": ["2", "cebh"],
      "h": ["1", "d"],
      "i": ["2", "cgC"]
    },
    "arena": [
      "......#####....",
      "..#####..1##...",
      "..#.##2....###.",
      "###.3#..4#5..#.",
      "#.6..78..##..##",
      "##......9.a.###",
      "###b.c..9...#d#",
      "#.9.....##e...#",
      "#...f#.9.#.g..#",
      "###.##.....##.#",
      "#f#....#h...#e#",
      "#...##.##..####",
      "#####.i...3#...",
      "...#....####...",
      "...######......"
    ],
    "molecule": [
      "...h....",
      "3bd.75..",
      ".3cia.f9",
      "...4.g9.",
      "....f29.",
      "...e.89.",
      ".....61.",
      "....e..."
    ]
  },
  {
    "id": "12",
    "name": "Thymol",
    "atoms": {
      "1": ["1", "e"],
      "2": ["2", "gdA"],
      "3": ["3", "dh"],
      "4": ["2", "acbf"],
      "5": ["1", "d"],
      "6": ["2", "egbf"],
      "7": ["1", "f"],
      "8": ["1", "c"],
      "9": ["1", "a"],
      "a": ["2", "afB"],
      "b": ["2", "gbC"],
      "c": ["1", "h"],
      "d": ["2", "acD"],
      "e": ["2", "ehB"],
      "f": ["2", "acgh"],
      "g": ["1", "g"],
      "h": ["1", "b"],
      "i": ["2", "aced"],
      "j": ["2", "ceD"]
    },
    "arena": [
      "..#####....###.",
      "..#.#1######2#.",
      ".##....#..3..#.",
      "##4....5#..5.#.",
      "#...#6.....#.#.",
      "#.7..#.8.1..##.",
      "##......#....#.",
      "#9#a....b#c.d#.",
      "#....#e.....##.",
      "###..f#.c...g##",
      "..#......#g...#",
      "..##..h..1#...#",
      "###...#...#####",
      "#..i..9#j.#....",
      "###########...."
    ],
    "molecule": [
      ".5....17",
      "..3.1.4g",
      "...edb..",
      "51.aj2..",
      "8f6.9.c.",
      ".hig....",
      "..9c...."
    ]
  },
  {
    "id": "13",
    "name": "Arginin",
    "atoms": {
      "1": ["2", "aceg"],
      "2": ["1", "d"],
      "3": ["1", "a"],
      "4": ["1", "b"],
      "5": ["2", "acf"],
      "6": ["4", "ebh"],
      "7": ["1", "f"],
      "8": ["1", "h"],
      "9": ["1", "e"],
      "a": ["2", "agC"],
      "b": ["4", "cfh"],
      "c": ["3", "D"],
      "d": ["4", "dA"],
      "e": ["2", "egB"],
      "f": ["3", "ad"]
    },
    "arena": [
      ".....######....",
      "...###.1.2#....",
      ".###.3..######.",
      "##4.5....6...#.",
      "#6#2#.7...##.##",
      "#...##...8#3..#",
      "#...9#.9#....a#",
      "##.....##3....#",
      ".#.1..b#1.#c.##",
      ".#d##..2..#..#.",
      ".#..#....#...#.",
      ".#....e.f..9##.",
      ".###..#.#####..",
      "...#8.#.#......",
      "...######......"
    ],
    "molecule": [
      ".....2.7.",
      "...9996..",
      ".2.5111ec",
      "2.6.333f.",
      ".ba.....8",
      "4.d......",
      "...8....."
    ]
  },
  {
    "id": "14",
    "name": "Oktan",
    "atoms": {
      "1": ["2", "bdfh"],
      "2": ["1", "h"],
      "3": ["1", "d"],
      "4": ["1", "f"],
      "5": ["1", "b"]
    },
    "arena": [
      "...############",
      "...#1..#.....1#",
      "...#..#2#.#2#.#",
      "####..........#",
      "#1..#3..#3.4#.#",
      "#...3#...#3.2.#",
      "#.#...1...#.#.#",
      "##2.........2##",
      "#.#.#...1...#.#",
      "#.2.3#...#3...#",
      "#.#..3#..3#..1#",
      "#..........####",
      "#.#2#.#2#..#...",
      "#15....#..1#...",
      "############..."
    ],
    "molecule": [
      ".......3.4",
      "......3.1.",
      ".....3.1.2",
      "....3.1.2.",
      "...3.1.2..",
      "..3.1.2...",
      ".3.1.2....",
      "3.1.2.....",
      ".1.2......",
      "5.2......."
    ]
  },
  {
    "id": "15",
    "name": "Kristall 3",
    "atoms": {
      "1": ["o", "eg"],
      "2": ["o", "cg"],
      "3": ["o", "ce"],
      "4": ["o", "ae"],
      "5": ["o", "ag"],
      "6": ["o", "ac"]
    },
    "arena": [
      ".....#.....",
      "....###....",
      "...#####...",
      "..#######..",
      ".##1223.##.",
      "##.2#4#2.##",
      ".##25246##.",
      "..#######..",
      "...#####...",
      "....###....",
      ".....#....."
    ],
    "molecule": [
      "32221",
      "4...4",
      "62225"
    ]
  },
  {
    "id": "16",
    "name": "Dekalin",
    "atoms": {
      "1": ["1", "e"],
      "2": ["1", "g"],
      "3": ["2", "abdf"],
      "4": ["1", "h"],
      "5": ["2", "egbh"],
      "6": ["1", "a"],
      "7": ["2", "ebdh"],
      "8": ["1", "c"],
      "9": ["1", "d"],
      "a": ["2", "adfh"],
      "b": ["2", "cebh"],
      "c": ["1", "f"],
      "d": ["2", "agdf"],
      "e": ["2", "aedf"],
      "f": ["2", "aebh"],
      "g": ["2", "acdf"],
      "h": ["1", "b"],
      "i": ["2", "ebfh"]
    },
    "arena": [
      "...#########.",
      "####1..#..2#.",
      "#.3...4..#.#.",
      "#1##.5#.#2.##",
      "#.6.....6...#",
      "##7.6#1..#.8#",
      "#.9....a.4..#",
      "#..9.b.#cd.##",
      "#e.....c..f.#",
      "##.g#8#..##.#",
      ".#.#.......h#",
      ".#..h#..i####",
      ".#########..."
    ],
    "molecule": [
      ".91.1c.",
      "9.a13.c",
      "85.f.b2",
      "8d.e.g2",
      "h.i67.4",
      ".h6.64."
    ]
  },
  {
    "id": "17",
    "name": "Dichlor-Diphenyl-Trichlorproethan",
    "atoms": {
      "1": ["2", "bdfh"],
      "2": ["2", "ehB"],
      "3": ["1", "c"],
      "4": ["1", "f"],
      "5": ["2", "gbC"],
      "6": ["2", "gdA"],
      "7": ["2", "acbf"],
      "8": ["2", "acD"],
      "9": ["1", "b"],
      "a": ["2", "acC"],
      "b": ["2", "ebD"],
      "c": ["2", "ceD"],
      "d": ["7", "h"],
      "e": ["2", "afB"],
      "f": ["2", "cfA"],
      "g": ["1", "e"],
      "h": ["1", "g"],
      "i": ["7", "f"],
      "j": ["7", "e"],
      "k": ["2", "agC"],
      "l": ["1", "a"],
      "m": ["7", "d"],
      "n": ["7", "g"]
    },
    "arena": [
      "#######...",
      "#.#1#2###.",
      "#345.6.7#.",
      "#89ab#..##",
      "#4#c9d#.e#",
      "####6f9.##",
      "...##ghi2#",
      "....##jk##",
      "....#lmn.#",
      "....######"
    ],
    "molecule": [
      "m.g.4...",
      ".285.ji.",
      ".ec6.7n.",
      "9.l.1..4",
      "...9.2b.",
      "....3kah",
      ".....f6.",
      "....9..d"
    ]
  },
  {
    "id": "18",
    "name": "Tryptophan",
    "atoms": {
      "1": ["4", "ebh"],
      "2": ["1", "a"],
      "3": ["2", "acC"],
      "4": ["2", "aceg"],
      "5": ["1", "d"],
      "6": ["1", "f"],
      "7": ["1", "c"],
      "8": ["1", "e"],
      "9": ["A", ""],
      "a": ["2", "ehB"],
      "b": ["2", "gdA"],
      "c": ["1", "b"],
      "d": ["3", "eh"],
      "e": ["2", "agC"],
      "f": ["1", "h"],
      "g": ["2", "cgC"],
      "h": ["3", "C"],
      "i": ["2", "dfA"],
      "j": ["2", "cfA"],
      "k": ["2", "ebD"]
    },
    "arena": [
      "...#######...",
      "...#1.#2.#...",
      "####34#..####",
      "#..#.#5#6#7.#",
      "##.5..8..9.##",
      "#..a.2..bc.d#",
      "#e..#f.g#.4.#",
      "#.#..#.#h.#b#",
      "#2.#i.#.1#j.#",
      "####..6..####",
      "..#..#.#..#..",
      "..#k..#..2#..",
      "..#########.."
    ],
    "molecule": [
      "5..6.5.6.",
      ".ak..81h.",
      "7e39g44b.",
      ".jb.i22.d",
      "c..1.f..2",
      "...2....."
    ]
  },
  {
    "id": "19",
    "name": "Indigo-Blau",
    "atoms": {
      "1": ["2", "ehB"],
      "2": ["1", "c"],
      "3": ["2", "gbC"],
      "4": ["1", "f"],
      "5": ["4", "cfh"],
      "6": ["2", "fhB"],
      "7": ["1", "d"],
      "8": ["2", "dfA"],
      "9": ["2", "gdA"],
      "a": ["3", "A"],
      "b": ["2", "egA"],
      "c": ["2", "agC"],
      "d": ["2", "ceA"],
      "e": ["1", "h"],
      "f": ["1", "b"],
      "g": ["2", "bhC"],
      "h": ["2", "afB"],
      "i": ["2", "cfA"],
      "j": ["1", "g"],
      "k": ["2", "acC"],
      "l": ["2", "adD"],
      "m": ["3", "C"],
      "n": ["2", "ebD"],
      "o": ["2", "chC"],
      "p": ["4", "gbd"],
      "q": ["2", "bdD"]
    },
    "arena": [
      "######..#####",
      "#1#2.####.3.#",
      "#4....#.#5.6#",
      "#..7.##8##..#",
      "##.9.4.a.4.##",
      "#b.#c##de..#.",
      "#.##f#.gh.i##",
      "#..#.....#.j#",
      "#kf..l#m.##.#",
      "###.####n.o.#",
      "#..p#..#f##.#",
      "#####..#.#q.#",
      ".......######"
    ],
    "molecule": [
      "7..4......",
      ".1n.4.m...",
      "2ckp..8..4",
      ".i9.6q.o3.",
      "f..g..5bdj",
      "...a.f.hl.",
      "......f..e"
    ]
  },
  {
    "id": "20",
    "name": "1,3,5-Trimethyl-Zyklohexan",
    "atoms": {
      "1": ["2", "cegh"],
      "2": ["2", "cegb"],
      "3": ["2", "acdf"],
      "4": ["1", "e"],
      "5": ["1", "d"],
      "6": ["2", "aceg"],
      "7": ["1", "f"],
      "8": ["1", "h"],
      "9": ["1", "a"],
      "a": ["2", "cedh"],
      "b": ["2", "agdf"],
      "c": ["1", "g"],
      "d": ["2", "aebh"],
      "e": ["1", "c"],
      "f": ["2", "egbf"],
      "g": ["1", "b"],
      "h": ["2", "bdfh"]
    },
    "arena": [
      "....#####....",
      "#####1.2#####",
      "#.........3.#",
      "#..4.....5..#",
      "#6...78.9...#",
      "#....a.7.b..#",
      "#.c.d.e.e.5.#",
      "#e...9.c....#",
      "#..........c#",
      "#.f...8.....#",
      "#..9........#",
      "#..g#####g.h#",
      "#####...#####"
    ],
    "molecule": [
      "..547..",
      ".5.d.7.",
      ".e162c.",
      ".eb93c.",
      "ef.h.ac",
      "g9g.898"
    ]
  },
  {
    "id": "21",
    "name": "Kristall 4",
    "atoms": {
      "1": ["o", "cfh"],
      "2": ["A", ""],
      "3": ["o", "h"],
      "4": ["o", "d"],
      "5": ["o", "b"],
      "6": ["C", ""],
      "7": ["o", "f"],
      "8": ["o", "ebh"],
      "9": ["o", "adf"],
      "a": ["o", "acegbdfh"],
      "b": ["o", "gbd"]
    },
    "arena": [
      "..#####..",
      "..#1.2#..",
      "####3####",
      "#4#535#6#",
      "#.47689a#",
      "#2#b34#.#",
      "####7####",
      "..#5.7#..",
      "..#####.."
    ],
    "molecule": [
      "..4.7..",
      "...8...",
      "4.467.7",
      ".12a2b.",
      "5.563.3",
      "...9...",
      "..5.3.."
    ]
  },
  {
    "id": "22",
    "name": "Zyklofruktose",
    "atoms": {
      "1": ["3", "ae"],
      "2": ["1", "e"],
      "3": ["1", "b"],
      "4": ["3", "gd"],
      "5": ["3", "ad"],
      "6": ["2", "bdfh"],
      "7": ["1", "f"],
      "8": ["1", "c"],
      "9": ["2", "aedf"],
      "a": ["3", "cg"],
      "b": ["3", "ag"],
      "c": ["2", "cgbd"],
      "d": ["1", "a"],
      "e": ["3", "af"],
      "f": ["2", "cefh"],
      "g": ["1", "g"],
      "h": ["1", "h"],
      "i": ["3", "gb"],
      "j": ["2", "cebh"]
    },
    "arena": [
      "##########.....",
      "#..1...2.#.....",
      "#.####.#.######",
      "#...3..4##5...#",
      "#.6.#...#7.#6.#",
      "###..8..9...#a#",
      "..#.##8.#.....#",
      "..###b...c#..7#",
      "..#....d.###.##",
      "###..2#...e.f#.",
      "#gh..##.i##.##.",
      "#..#j....##.h#.",
      "###########3.#.",
      "..........####."
    ],
    "molecule": [
      "..2.....",
      "..5.72..",
      "84.6.e7.",
      "..6.jcag",
      ".3.fb.h.",
      ".8i9....",
      "..31h...",
      "...d...."
    ]
  },
  {
    "id": "23",
    "name": "Sebacinsäure",
    "atoms": {
      "1": ["2", "cegf"],
      "2": ["2", "cegd"],
      "3": ["2", "bdfh"],
      "4": ["1", "d"],
      "5": ["1", "f"],
      "6": ["2", "adD"],
      "7": ["2", "afB"],
      "8": ["3", "B"],
      "9": ["1", "b"],
      "a": ["1", "h"],
      "b": ["3", "D"],
      "c": ["1", "a"],
      "d": ["2", "cbfh"],
      "e": ["3", "eb"],
      "f": ["3", "eh"],
      "g": ["2", "gbdh"]
    },
    "arena": [
      "..#########..",
      "..#.1#.#2.#..",
      "..#.#3.3#.#..",
      "###..4#5..###",
      "#6...###...7#",
      "#8...9#a...b#",
      "###.......###",
      "#.5.......4.#",
      "#9.........a#",
      "####c4#5c####",
      "..#de###fg#..",
      "..#..9#a..#..",
      "..#########.."
    ],
    "molecule": [
      "..5....4..",
      ".e......f.",
      "86.5..4.7b",
      "..3.54.3..",
      ".9.d12g.a.",
      "..99ccaa.."
    ]
  },
  {
    "id": "24",
    "name": "Lysin",
    "atoms": {
      "1": ["1", "c"],
      "2": ["2", "cgdh"],
      "3": ["1", "a"],
      "4": ["2", "cdfh"],
      "5": ["1", "g"],
      "6": ["2", "ehB"],
      "7": ["4", "bdh"],
      "8": ["1", "f"],
      "9": ["4", "egb"],
      "a": ["3", "D"],
      "b": ["3", "ae"],
      "c": ["1", "d"]
    },
    "arena": [
      "#############",
      "#.#..1.#2...#",
      "#3..1#.....##",
      "#..4.5..5#6.#",
      "#.#.1.#.....#",
      "#.....7.1.5.#",
      "##8.#5..#.#.#",
      "#..........9#",
      "#2.#2..#.a..#",
      "#....#1.b#..#",
      "##..5...3..##",
      "#...#..#.c.2#",
      "#############"
    ],
    "molecule": [
      "c.8......",
      ".7.......",
      ".125.....",
      "..125....",
      "...125...",
      "....125..",
      "......45.",
      "....19.6a",
      ".....3.b.",
      ".......3."
    ]
  },
  {
    "id": "25",
    "name": "Sulfonylamid",
    "atoms": {
      "1": ["A", ""],
      "2": ["2", "agC"],
      "3": ["1", "h"],
      "4": ["1", "d"],
      "5": ["2", "chC"],
      "6": ["4", "acf"],
      "7": ["1", "e"],
      "8": ["3", "B"],
      "9": ["4", "adf"],
      "a": ["2", "ehB"],
      "b": ["3", "D"],
      "c": ["5", "aeBD"],
      "d": ["2", "gdA"],
      "e": ["1", "g"],
      "f": ["2", "ceA"],
      "g": ["2", "bdD"],
      "h": ["1", "c"],
      "i": ["1", "b"]
    },
    "arena": [
      "#############",
      "#.1..2..3...#",
      "#........4.5#",
      "#..6...7....#",
      "#....8...9..#",
      "#..a...b....#",
      "#.c...#.d...#",
      "#...e.....f.#",
      "#.....g.....#",
      "#....h.....e#",
      "#..3.....i..#",
      "#...........#",
      "#############"
    ],
    "molecule": [
      "...7.",
      "4..6e",
      ".ag..",
      "h2.5e",
      ".f1d.",
      "8cb.3",
      ".9...",
      "i.3.."
    ]
  }
]}
//</levelSet>
;
  var top;
  if (typeof(exports) !== 'undefined') {
    top = exports;
  } else {
    if (!this.KP_ATOMIX) this.KP_ATOMIX = {};
    top = this.KP_ATOMIX;
  }
    top.levelSets = top.levelSets || {}
    top.levelSets[levelSet.name] = levelSet;
}());
/*
 * The contents of this file is derived from the KDE project "KAtomic"
 *
 * http://games.kde.org/game.php?game=katomic
 * http://www.gnu.org/licenses/gpl.html
 *
 * KAtomic is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * KAtomic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAtomic; see the file COPYING.  If not, write to
 * the Free Software Foundation, 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
*/

(function () {

  var levelSet =
//<levelSet>
{
  "name": "original",
  "credits": "<a href='http://kde-files.org/content/show.php?content=116585'>Andreas Wüst</a>",
  "license": "<a href='http://www.gnu.org/licenses/lgpl.html'>LGPL</a>",
  "levels": [
  {
    "id": "1",
    "name": "Water",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "cg"],
      "3": ["1", "g"]
    },
    "arena": [
      "#####......",
      "#...#......",
      "#..3######.",
      "#..#.....#.",
      "#.#......##",
      "#.#..#.##.#",
      "#....#.#2.#",
      "###1#..#..#",
      ".#........#",
      ".##########"
    ],
    "molecule": [
      "123"
    ]
  },
  {
    "id": "2",
    "name": "Methane",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["1", "g"]
    },
    "arena": [
      "#####.........",
      "#...#.........",
      "#...##########",
      "#.#.#.#......#",
      "#.#.#3#..2#..#",
      "#.1#..#####..#",
      "#.......#....#",
      "#.......#....#",
      "#.#####.5....#",
      "#..4#..#######",
      "########......"
    ],
    "molecule": [
      ".2.",
      "135",
      ".4."
    ]
  },
  {
    "id": "3",
    "name": "Methanol",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "cg"],
      "6": ["1", "g"]
    },
    "arena": [
      "####.........",
      "#..#.........",
      "#..#.........",
      "#.2#.........",
      "#..########..",
      "#...#...#.###",
      "#.#.....#...#",
      "#.......#3..#",
      "#.1###5.#.#.#",
      "#...........#",
      "#....4.#6.#.#",
      "######....#.#",
      ".....########"
    ],
    "molecule": [
      ".2..",
      "1356",
      ".4.."
    ]
  },
  {
    "id": "4",
    "name": "Ethylen",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "fhB"],
      "4": ["2", "bdD"],
      "5": ["1", "f"],
      "6": ["1", "h"]
    },
    "arena": [
      "#######....",
      "#.#...#....",
      "#.#2#.#....",
      "#.#...#....",
      "#.#...#....",
      "#...#######",
      "#.........#",
      "#.4##3.5..#",
      "#......####",
      "##..#1....#",
      "#...#.#...#",
      "#..6#...###",
      "########..."
    ],
    "molecule": [
      "1..5",
      ".34.",
      "2..6"
    ]
  },
  {
    "id": "5",
    "name": "Propene",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["2", "agB"],
      "6": ["2", "bdD"],
      "7": ["1", "f"],
      "8": ["1", "h"]
    },
    "arena": [
      "......####....",
      "..#####..#....",
      "..#.....7#....",
      "..#..2########",
      "..#5.....#...#",
      "..####..##.6.#",
      "###1.#..3#...#",
      "#....#....2..#",
      "#.#..#.#######",
      "#............#",
      "#...#....##..#",
      "#8..#....4...#",
      "##############"
    ],
    "molecule": [
      ".22.7",
      "1356.",
      ".4..8"
    ]
  },
  {
    "id": "6",
    "name": "Bonus Section 1",
    "atoms": {
      "1": ["E", ""],
      "2": ["F", ""],
      "3": ["G", ""],
      "4": ["H", ""],
      "5": ["I", ""],
      "6": ["J", ""],
      "7": ["K", ""],
      "8": ["L", ""]
    },
    "arena": [
      "######",
      "#.4.2#",
      "#.78.#",
      "#1..5#",
      "#6.3.#",
      "######"
    ],
    "molecule": [
      "123",
      "456",
      "78."
    ]
  },
  {
    "id": "7",
    "name": "Ethanol",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "cg"],
      "6": ["1", "g"]
    },
    "arena": [
      "###########..",
      "#..2......#..",
      "#.##..#...#..",
      "#.....3#..#..",
      "#...#..#..#..",
      "#.#6#..#3.#..",
      "#........####",
      "#####..#....#",
      ".#...5.#.#..#",
      "##1##....2..#",
      "#.....4.#####",
      "#...4.#.#....",
      "#########...."
    ],
    "molecule": [
      ".22..",
      "13356",
      ".44.."
    ]
  },
  {
    "id": "8",
    "name": "Isopropanol",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "ae"],
      "6": ["1", "g"]
    },
    "arena": [
      "###########.",
      "#.....#.2.#.",
      "#.....#...#.",
      "#..#..5...#.",
      "#.2#4####.##",
      "#..#.#.4#3.#",
      "#.##....#..#",
      "#.......#2.#",
      "#.3#...##..#",
      "#..##..4.6.#",
      "#..1#..3.###",
      "#...#..#...#",
      "############"
    ],
    "molecule": [
      ".222.",
      "13336",
      ".454.",
      "..4.."
    ]
  },
  {
    "id": "9",
    "name": "Ethanal",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["2", "agB"],
      "6": ["3", "D"]
    },
    "arena": [
      "##########",
      "#......6.#",
      "#..###.#.#",
      "#.....2#.#",
      "#.##..3..#",
      "#.1..4.#2#",
      "#.####.#.#",
      "#...5....#",
      "#.#..#####",
      "#.#..#....",
      "######...."
    ],
    "molecule": [
      ".22.",
      "1356",
      ".4.."
    ]
  },
  {
    "id": "10",
    "name": "Acetone",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["2", "cgC"],
      "6": ["3", "A"],
      "7": ["1", "g"]
    },
    "arena": [
      "#############",
      "#..4.#......#",
      "#..#...#.7###",
      "#..4...2....#",
      "#########...#",
      "..#.....#23.#",
      "..#..#......#",
      "###...3.5.#.#",
      "#...#....6#.#",
      "#...1###....#",
      "#......#....#",
      "#############"
    ],
    "molecule": [
      ".2.2.",
      "13537",
      ".464."
    ]
  },
  {
    "id": "11",
    "name": "Formic Acid",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "C"],
      "3": ["2", "cgA"],
      "4": ["3", "cg"],
      "5": ["1", "g"]
    },
    "arena": [
      ".........####",
      "######.###..#",
      "#....#.#....#",
      "#....###..#.#",
      "#.##........#",
      "#........####",
      "####31##....#",
      "#.....2...#.#",
      "##..#.#..#4##",
      "#...#.......#",
      "##....5.#.#.#",
      "#.....#.#...#",
      "#############"
    ],
    "molecule": [
      ".2..",
      "1345"
    ]
  },
  {
    "id": "12",
    "name": "Bonus Section 2",
    "atoms": {
      "1": ["o", "ce"],
      "2": ["o", "ace"],
      "3": ["o", "ac"],
      "4": ["o", "ceg"],
      "5": ["o", "aceg"],
      "6": ["o", "acg"],
      "7": ["o", "eg"],
      "8": ["o", "aeg"],
      "9": ["o", "ag"]
    },
    "arena": [
      "######",
      "#34.5#",
      "#.8..#",
      "#2.97#",
      "#.1.6#",
      "######"
    ],
    "molecule": [
      "147",
      "258",
      "369"
    ]
  },
  {
    "id": "13",
    "name": "Acetic Acid",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "C"],
      "6": ["2", "cgA"],
      "7": ["3", "cg"],
      "8": ["1", "g"]
    },
    "arena": [
      ".#######..",
      ".#..2..###",
      ".#.8#.5..#",
      "##.#..####",
      "#..#.....#",
      "#..3##1..#",
      "#.....#6.#",
      "#.4#.....#",
      "##...#7#.#",
      ".#####...#",
      ".....#####"
    ],
    "molecule": [
      ".25..",
      "13678",
      ".4..."
    ]
  },
  {
    "id": "14",
    "name": "trans-Butene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "bdfh"],
      "4": ["2", "fhB"],
      "5": ["1", "h"],
      "6": ["2", "bdD"],
      "7": ["1", "f"]
    },
    "arena": [
      "##############",
      "#.....#..1...#",
      "#..##1#..##..#",
      "#..5..#3.5..7#",
      "#.#.###.###..#",
      "#.#3..#.#..6.#",
      "#2....#....#.#",
      "#............#",
      "#.###.##.###.#",
      "#.4..........#",
      "#.......#5.1.#",
      "##############"
    ],
    "molecule": [
      "...1.7",
      ".1..3.",
      "1.46.5",
      ".3..5.",
      "2.5..."
    ]
  },
  {
    "id": "15",
    "name": "cis-Butene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "bdfh"],
      "4": ["2", "fhB"],
      "5": ["1", "h"],
      "6": ["2", "bdD"],
      "7": ["1", "f"]
    },
    "arena": [
      "#############",
      "#.........2.#",
      "#..2.1#.....#",
      "##.#..#4##1.#",
      "#.6#....5...#",
      "#.##.#####.5#",
      "#7.#........#",
      "#......#...7#",
      "#.##.#....#.#",
      "#.........3.#",
      "#3.....#....#",
      "#############"
    ],
    "molecule": [
      ".1..7.",
      "1.46.7",
      ".3..3.",
      "2.52.5"
    ]
  },
  {
    "id": "16",
    "name": "Dimethyl ether",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "cg"],
      "6": ["1", "g"]
    },
    "arena": [
      "#######.....",
      "#.....#.....",
      "#1.#..#.....",
      "#..4.3#.....",
      "#..#..######",
      "#..#....5..#",
      "#..#.##....#",
      "#........2.#",
      "#..#....####",
      "#..#.#..2..#",
      "#..#.####.6#",
      "#.3..4.....#",
      "############"
    ],
    "molecule": [
      ".2.2.",
      "13536",
      ".4.4."
    ]
  },
  {
    "id": "17",
    "name": "Butanol",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "ae"],
      "6": ["1", "g"]
    },
    "arena": [
      "##############",
      "#......2.....#",
      "#2.#.3####.4.#",
      "#..#........5#",
      "#.........##6#",
      "#.1##....#...#",
      "#.....#..#.2.#",
      "#.3...4..#####",
      "####..#3...4.#",
      "...#..#.3#...#",
      "...#4.#....2.#",
      "...###########"
    ],
    "molecule": [
      ".2222.",
      "133336",
      ".4445.",
      "....4."
    ]
  },
  {
    "id": "18",
    "name": "Bonus Section 3",
    "atoms": {
      "1": ["E", ""],
      "2": ["F", ""],
      "3": ["G", ""],
      "4": ["H", ""],
      "5": ["I", ""],
      "6": ["J", ""],
      "7": ["K", ""],
      "8": ["L", ""]
    },
    "arena": [
      "######",
      "#8.3.#",
      "#2.6.#",
      "#...5#",
      "#47.1#",
      "######"
    ],
    "molecule": [
      "123",
      "456",
      "78."
    ]
  },
  {
    "id": "19",
    "name": "2-Methyl-2-Propanol",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "d"],
      "3": ["1", "e"],
      "4": ["2", "aceg"],
      "5": ["1", "a"],
      "6": ["2", "abeh"],
      "7": ["3", "ae"],
      "8": ["1", "f"],
      "9": ["1", "g"]
    },
    "arena": [
      "##########...",
      "#.3....7.#...",
      "#.#1.....####",
      "#.#.#...4...#",
      "#..3#.5.5##.#",
      "#.###.#5....#",
      "#2..4.##..8.#",
      "#.###...4####",
      "#.#.#.3#.#...",
      "#.#.#9#..#...",
      "###.#...6#...",
      "....######..."
    ],
    "molecule": [
      ".238.",
      ".363.",
      "14449",
      ".575.",
      "..5.."
    ]
  },
  {
    "id": "20",
    "name": "Glycerin",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "cg"],
      "6": ["1", "g"]
    },
    "arena": [
      "#######....",
      "#..2#.#####",
      "#.#..3....#",
      "#.#.....#.#",
      "#.3...5..6#",
      "#.1#.4#...#",
      "###.5#..#.#",
      "..#....1..#",
      "###3##.5#.#",
      "#..1.6...6#",
      "#..#..#####",
      "#....##....",
      "######....."
    ],
    "molecule": [
      ".2..",
      "1356",
      "1356",
      "1356",
      ".4.."
    ]
  },
  {
    "id": "21",
    "name": "Poly-Tetra-Fluoro-Ethene",
    "atoms": {
      "1": ["6", "e"],
      "2": ["6", "a"],
      "3": ["2", "aceg"]
    },
    "arena": [
      "##############",
      "#..2.....#...#",
      "#.###....#.3.#",
      "#.#....1.....#",
      "#2#.2#..#.##.#",
      "#...##.......#",
      "#.....3.1...2#",
      "##....####...#",
      "#.........3..#",
      "#..#..##..#..#",
      "#..#1..1..#..#",
      "#........3...#",
      "##############"
    ],
    "molecule": [
      "1111",
      "3333",
      "2222"
    ]
  },
  {
    "id": "22",
    "name": "Oxalic Acid",
    "atoms": {
      "1": ["3", "D"],
      "2": ["2", "ceD"],
      "3": ["3", "ae"],
      "4": ["1", "a"],
      "5": ["2", "egB"],
      "6": ["3", "B"]
    },
    "arena": [
      "##############",
      "#...#..#.....#",
      "#...3......6.#",
      "#.##..#.####.#",
      "#....5#.3..#.#",
      "#1........2..#",
      "#..#....###..#",
      "#..##..##.#4.#",
      "#...#..#..#..#",
      "#.#.####..#..#",
      "#.#4#...###..#",
      "#...#...#....#",
      "#####...######"
    ],
    "molecule": [
      "6251",
      ".33.",
      ".44."
    ]
  },
  {
    "id": "23",
    "name": "Formaldehyde",
    "atoms": {
      "1": ["1", "e"],
      "2": ["2", "aeB"],
      "3": ["1", "a"],
      "4": ["3", "D"]
    },
    "arena": [
      "##############",
      "#........3...#",
      "#...#...##...#",
      "#...#........#",
      "###.#..#######",
      "..#1##...#...#",
      "..#......4.#.#",
      "..#..##...##.#",
      "..#..#..#.#..#",
      "..#...2.#....#",
      "..#.....#....#",
      "..############"
    ],
    "molecule": [
      "1.",
      "24",
      "3."
    ]
  },
  {
    "id": "24",
    "name": "Bonus Section 4",
    "atoms": {
      "1": ["o", "ce"],
      "2": ["o", "ace"],
      "3": ["o", "ac"],
      "4": ["o", "ceg"],
      "5": ["o", "aceg"],
      "6": ["o", "acg"],
      "7": ["o", "eg"],
      "8": ["o", "aeg"],
      "9": ["o", "ag"]
    },
    "arena": [
      ".....##.....",
      "....####....",
      "....#4.#....",
      "...##.5##...",
      ".###2...###.",
      "##85.6.5..##",
      "##7.9.6..4##",
      ".###.852###.",
      "...##3.##...",
      "....#1.#....",
      "....####....",
      ".....##....."
    ],
    "molecule": [
      "1447",
      "2558",
      "2558",
      "3669"
    ]
  },
  {
    "id": "25",
    "name": "Acetic acid ethyl ester",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "C"],
      "6": ["2", "cgA"],
      "7": ["3", "cg"],
      "8": ["1", "g"]
    },
    "arena": [
      "##############",
      "#............#",
      "#.3.1..2..#.4#",
      "#.####..###..#",
      "#.4..#.3.....#",
      "###..#.#..####",
      "#...4#..2..5.#",
      "#.#.....###..#",
      "#...##....#..#",
      "#...#.....#..#",
      "#..6..387.2.##",
      "##.##..#.....#",
      ".#############"
    ],
    "molecule": [
      ".25.22.",
      "1367338",
      ".4..44."
    ]
  },
  {
    "id": "26",
    "name": "Ammonia",
    "atoms": {
      "1": ["1", "c"],
      "2": ["4", "bdg"],
      "3": ["1", "f"],
      "4": ["1", "h"]
    },
    "arena": [
      "##############",
      "#............#",
      "#..#..##.....#",
      "####.2....#..#",
      "##....#..#...#",
      "#...#.#.#.#..#",
      "#..##...#.#..#",
      "#...1...#....#",
      "#..####....###",
      "#..#4....3...#",
      "#..#...###...#",
      "#......#.....#",
      "##############"
    ],
    "molecule": [
      "..3",
      "12.",
      "..4"
    ]
  },
  {
    "id": "27",
    "name": "3-Methyl-Pentane",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["1", "d"],
      "6": ["2", "abeh"],
      "7": ["1", "f"],
      "8": ["1", "g"]
    },
    "arena": [
      "#####..#######",
      "#..4#..#3..2.#",
      "#.###..#.2...#",
      "#...#..#.###5#",
      "#4.3#..#6..#.#",
      "#.#.#..#.#3#4#",
      "###.####.#...#",
      "#.......4..###",
      "#...2.####3..#",
      "#.2...2..4...#",
      "#...7.18####3#",
      "#.......#..#.#",
      "#########..###"
    ],
    "molecule": [
      "..527..",
      ".22622.",
      "1333338",
      ".44444."
    ]
  },
  {
    "id": "28",
    "name": "Propanal",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["2", "cgC"],
      "6": ["3", "A"],
      "7": ["1", "g"]
    },
    "arena": [
      "#########..",
      "#..#....#..",
      "#...4.6.#..",
      "#.1###..###",
      "#..#....5.#",
      "#2...#....#",
      "##....#...#",
      "#..#..#3#.#",
      "#.3####.#7#",
      "#...2.#4#.#",
      "#.....#...#",
      "###########"
    ],
    "molecule": [
      ".22..",
      "13357",
      ".446."
    ]
  },
  {
    "id": "29",
    "name": "Propyne",
    "atoms": {
      "1": ["1", "c"],
      "2": ["2", "gF"],
      "3": ["1", "e"],
      "4": ["2", "cH"],
      "5": ["2", "aceg"],
      "6": ["1", "a"],
      "7": ["1", "g"]
    },
    "arena": [
      "##############",
      "#........#...#",
      "#..##.#.3##..#",
      "#..#.......7.#",
      "#..#..2##....#",
      "#.........##.#",
      "######....6.4#",
      ".....#.###.#.#",
      ".....#..#..#.#",
      "....##.......#",
      "....#1..5.####",
      "....#....##...",
      "....######...."
    ],
    "molecule": [
      "...3.",
      "12457",
      "...6."
    ]
  },
  {
    "id": "30",
    "name": "Bonus Section 5",
    "atoms": {
      "1": ["E", ""],
      "2": ["F", ""],
      "3": ["G", ""],
      "4": ["H", ""],
      "5": ["I", ""],
      "6": ["J", ""],
      "7": ["K", ""],
      "8": ["L", ""]
    },
    "arena": [
      "######",
      "#8.3.#",
      "#2.6.#",
      "#...5#",
      "#47.1#",
      "######"
    ],
    "molecule": [
      "123",
      "456",
      "78."
    ]
  }
]}
//</levelSet>
;
  var top;
  if (typeof(exports) !== 'undefined') {
    top = exports;
  } else {
    if (!this.KP_ATOMIX) this.KP_ATOMIX = {};
    top = this.KP_ATOMIX;
  }
    top.levelSets = top.levelSets || {}
    top.levelSets[levelSet.name] = levelSet;
}());
/*
 * The contents of this file is derived from the KDE project "KAtomic"
 *
 * http://games.kde.org/game.php?game=katomic
 * http://www.gnu.org/licenses/gpl.html
 *
 * KAtomic is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * KAtomic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAtomic; see the file COPYING.  If not, write to
 * the Free Software Foundation, 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
*/

(function () {

  var levelSet =
//<levelSet>
{
  "name": "katomic",
  "credits": "<a href='http://games.kde.org/game.php?game=katomic'>Katomic Team</a>",
  "license": "<a href='http://www.gnu.org/licenses/gpl.html'>GPL</a>",
  "levels": [
  {
    "id": "1",
    "name": "Water",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "cg"],
      "3": ["1", "g"]
    },
    "arena" : [
      "###########",
      "#..#......#",
      "#.3#......#",
      "#.##......#",
      "#.#..#.####",
      "#....#.2..#",
      "###.#.....#",
      "#1....#...#",
      "###########"
    ],
    "molecule": [
      "123"
    ]
  },
  {
    "id": "2",
    "name": "Methane",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["1", "g"]
    },
    "arena" : [
      "#############",
      "#...#..#....#",
      "#.#..#5#....#",
      "#..#...#3.#.#",
      "#......####.#",
      "#.......#1#.#",
      "#.2..#..#...#",
      "#.#####....##",
      "#.4.#..#...#.",
      "#####..#...#.",
      ".......#####."
    ],
    "molecule": [
      ".2.",
      "135",
      ".4."
    ]
  },
  {
    "id": "3",
    "name": "Methanol",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "cg"],
      "6": ["1", "g"]
    },
    "arena" : [
      ".........####",
      "####.....#..#",
      "#..#.....#..#",
      "#..##...##.5#",
      "#..4#####...#",
      "#.#.#.3#....#",
      "#......#.6..#",
      "#..#.#...#..#",
      "#2..........#",
      "#...#.#...#.#",
      "######1...#.#",
      ".....########"
    ],
    "molecule": [
      ".2..",
      "1356",
      ".4.."
    ]
  },
  {
    "id": "4",
    "name": "Ethylene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "fhB"],
      "4": ["2", "bdD"],
      "5": ["1", "f"],
      "6": ["1", "h"]
    },
    "arena" : [
      "###.....###",
      "#.#.....#.#",
      "#2#######.#",
      "#......#..#",
      "##..4.#...#",
      "#.#....5#3#",
      "#...#....##",
      "#..#....61#",
      "#.#######.#",
      "#.#.....#.#",
      "###.....###"
    ],
    "molecule": [
      "1..5",
      ".34.",
      "2..6"
    ]
  },
  {
    "id": "5",
    "name": "Propene",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["2", "agB"],
      "6": ["2", "bdD"],
      "7": ["1", "f"],
      "8": ["1", "h"]
    },
    "arena" : [
      ".#########..",
      ".#7#.#.#.#..",
      ".#.#.#.#.###",
      "##4.6#2#.#.#",
      "#2.1...#5#.#",
      "#.#......#.#",
      "#.#3#......#",
      "#.#.#.#...##",
      "###.#8#.#.#.",
      "..#.#.#.#.#.",
      "..#########."
    ],
    "molecule": [
      ".22.7",
      "1356.",
      ".4..8"
    ]
  },
  {
    "id": "6",
    "name": "Ethanol",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "cg"],
      "6": ["1", "g"]
    },
    "arena" : [
      ".....#####..",
      ".....#43.#..",
      ".#####.#.#..",
      ".#......4#..",
      ".###2...##..",
      "...#.#1#.###",
      "...##2...#.#",
      "####.3.....#",
      "#.5....#...#",
      "#####.#..#.#",
      "...#....#.6#",
      "...#########"
    ],
    "molecule": [
      ".22..",
      "13356",
      ".44.."
    ]
  },
  {
    "id": "7",
    "name": "Isopropanol",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "ae"],
      "6": ["1", "g"]
    },
    "arena" : [
      "...###.###...",
      "...#1#.#.#...",
      "...#4.#..#...",
      "####....2####",
      "#...........#",
      "##.........##",
      ".##...#...##.",
      "##.3.3....5##",
      "#.2....3....#",
      "####2...4####",
      "...#6.#.4#...",
      "...#.###.#...",
      "...###.###..."
    ],
    "molecule": [
      ".222.",
      "13336",
      ".454.",
      "..4.."
    ]
  },
  {
    "id": "8",
    "name": "Ethanal",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["2", "agB"],
      "6": ["3", "D"]
    },
    "arena" : [
      "###########",
      "#.........#",
      "#.##...##.#",
      "#2.....3..#",
      "#.###.###.#",
      "#4...1....#",
      "#5###2###.#",
      "#.........#",
      "#.##...##.#",
      "#..6......#",
      "###########"
    ],
    "molecule": [
      ".22.",
      "1356",
      ".4.."
    ]
  },
  {
    "id": "9",
    "name": "Acetone",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["2", "cgC"],
      "6": ["3", "A"],
      "7": ["1", "g"]
    },
    "arena" : [
      "#############",
      "####..##....#",
      "##.....#.#..#",
      "#.....6#.####",
      "#.#3#.4.5..2#",
      "#.#4###..1..#",
      "###..2....###",
      "#.....###.#.#",
      "#.....3.#.#.#",
      "####.#.....7#",
      "#..#.#.....##",
      "#....##..####",
      "#############"
    ],
    "molecule": [
      ".2.2.",
      "13537",
      ".464."
    ]
  },
  {
    "id": "10",
    "name": "Formic Acid",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "C"],
      "3": ["2", "cgA"],
      "4": ["3", "cg"],
      "5": ["1", "g"]
    },
    "arena" : [
      "#############",
      "#...........#",
      "###..###..###",
      "#.....#.....#",
      "#.###...###.#",
      "#.#2..#...#.#",
      "#1435###....#",
      "#.#...#...#.#",
      "#.###...###.#",
      "#.....#.....#",
      "###..###..###",
      "#...........#",
      "#############"
    ],
    "molecule": [
      ".2..",
      "1345"
    ]
  },
  {
    "id": "11",
    "name": "Acetic Acid",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "C"],
      "6": ["2", "cgA"],
      "7": ["3", "cg"],
      "8": ["1", "g"]
    },
    "arena" : [
      "###########",
      "#...#...#.#",
      "#.7.......#",
      "#.#..8#.1.#",
      "#4.3......#",
      "#...#...#.#",
      "#.........#",
      "#.#.6.#...#",
      "#5.....2..#",
      "#...#...#.#",
      "###########"
    ],
    "molecule": [
      ".25..",
      "13678",
      ".4..."
    ]
  },
  {
    "id": "12",
    "name": "trans-Butene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "bdfh"],
      "4": ["2", "fhB"],
      "5": ["1", "h"],
      "6": ["2", "bdD"],
      "7": ["1", "f"]
    },
    "arena" : [
      "#############",
      "#...........#",
      "#...#..6#...#",
      "#4.#.7...#..#",
      "#.#.##.##.#.#",
      "#..2........#",
      "###5##.##.###",
      "#...........#",
      "#.#.##1##.#.#",
      "#..#31...#1.#",
      "#...#..5#...#",
      "#3..5.......#",
      "#############"
    ],
    "molecule": [
      "...1.7",
      ".1..3.",
      "1.46.5",
      ".3..5.",
      "2.5..."
    ]
  },
  {
    "id": "13",
    "name": "cis-Butene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "bdfh"],
      "4": ["2", "fhB"],
      "5": ["1", "h"],
      "6": ["2", "bdD"],
      "7": ["1", "f"]
    },
    "arena" : [
      "#############",
      "#.#..##...#.#",
      "##..#.....#.#",
      "##....#..#..#",
      "#.5..1#.2..7#",
      "###....#.#..#",
      "#..#......###",
      "#.4..3.#6..3#",
      "#.##.##.....#",
      "##......#.#.#",
      "#.7##5.#2.#1#",
      "#.#....#.#..#",
      "#############"
    ],
    "molecule": [
      ".1..7.",
      "1.46.7",
      ".3..3.",
      "2.52.5"
    ]
  },
  {
    "id": "14",
    "name": "Dimethyl ether",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "cg"],
      "6": ["1", "g"]
    },
    "arena" : [
      "#############",
      "#...3.#5...1#",
      "#.###.#.###.#",
      "#.#..4#...#.#",
      "#.#...#...#.#",
      "#.....#.....#",
      "####2....####",
      "#.....#.....#",
      "#.#...#...#.#",
      "#.#..6#..4#.#",
      "#.###.#.###2#",
      "#....3#.....#",
      "#############"
    ],
    "molecule": [
      ".2.2.",
      "13536",
      ".4.4."
    ]
  },
  {
    "id": "15",
    "name": "Butanol",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "ae"],
      "6": ["1", "g"]
    },
    "arena" : [
      "#############",
      "#..42.....3.#",
      "#.#..####.###",
      "###2...#....#",
      "#.6..#.....4#",
      "#.##.#.##.4.#",
      "#.#..#..#.###",
      "###.3#.5.3#.#",
      "#1..........#",
      "#4...#..2...#",
      "#..###2##.#.#",
      "#....#.3..#.#",
      "#############"
    ],
    "molecule": [
      ".2222.",
      "133336",
      ".4445.",
      "....4."
    ]
  },
  {
    "id": "16",
    "name": "2-Methyl-2-Propanol",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "d"],
      "3": ["1", "e"],
      "4": ["2", "aceg"],
      "5": ["1", "a"],
      "6": ["2", "abeh"],
      "7": ["3", "ae"],
      "8": ["1", "f"],
      "9": ["1", "g"]
    },
    "arena" : [
      "#####...#####",
      "#9.6#...#5.2#",
      "#8#.#####.#.#",
      "#5....#.....#",
      "###.#..3#.###",
      "..#.....54#..",
      "..##.....##..",
      "..#4.....3#..",
      "###.#...#.###",
      "#.....#.3..1#",
      "#.#7#####.#.#",
      "#4..#...#...#",
      "#####...#####"
    ],
    "molecule": [
      ".238.",
      ".363.",
      "14449",
      ".575.",
      "..5.."
    ]
  },
  {
    "id": "17",
    "name": "Glycerin",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "cg"],
      "6": ["1", "g"]
    },
    "arena" : [
      "###########",
      "#.#.4#3.#.#",
      "#.5#.#.#..#",
      "##..3....##",
      "#.#.....#.#",
      "#.6....1..#",
      "####5..####",
      "#....15.1.#",
      "#.#....3#.#",
      "##.....6.##",
      "#..#.#.#.2#",
      "#.#.6#..#.#",
      "###########"
    ],
    "molecule": [
      ".2..",
      "1356",
      "1356",
      "1356",
      ".4.."
    ]
  },
  {
    "id": "18",
    "name": "Poly-Tetra-Fluoro-Ethene",
    "atoms": {
      "1": ["6", "e"],
      "2": ["6", "a"],
      "3": ["2", "aceg"]
    },
    "arena" : [
      "#############",
      "#...........#",
      "#....#2#1...#",
      "#3...#.#....#",
      "#.####3####.#",
      "#2..........#",
      "####..#.2####",
      "#1.........1#",
      "#.####.####.#",
      "#..2.#.#....#",
      "#1...#.#...3#",
      "#..3........#",
      "#############"
    ],
    "molecule": [
      "1111",
      "3333",
      "2222"
    ]
  },
  {
    "id": "19",
    "name": "Oxalic Acid",
    "atoms": {
      "1": ["3", "D"],
      "2": ["2", "ceD"],
      "3": ["3", "ae"],
      "4": ["1", "a"],
      "5": ["2", "egB"],
      "6": ["3", "B"]
    },
    "arena" : [
      "###############",
      "###3...#....###",
      "##.....#..6.5##",
      "#...#.....#...#",
      "#...##.2.##...#",
      "#....##.##....#",
      "###1...3....###",
      "#....##.##....#",
      "#...##.4.##...#",
      "#.4.#.....#...#",
      "##.....#.....##",
      "###....#....###",
      "###############"
    ],
    "molecule": [
      "6251",
      ".33.",
      ".44."
    ]
  },
  {
    "id": "20",
    "name": "Formaldehyde",
    "atoms": {
      "1": ["1", "e"],
      "2": ["2", "aeB"],
      "3": ["1", "a"],
      "4": ["3", "D"]
    },
    "arena" : [
      "#############",
      "#4....#....2#",
      "#..##.#.##..#",
      "##.#.....#.##",
      "#..#.....#..#",
      "#.##..#..####",
      "#....###....#",
      "#.##..#..##.#",
      "#..#.....#..#",
      "##.#.....#.##",
      "#..##.#.##..#",
      "#3....#....1#",
      "#############"
    ],
    "molecule": [
      "1.",
      "24",
      "3."
    ]
  },
  {
    "id": "21",
    "name": "Crystal 1",
    "atoms": {
      "1": ["o", "ce"],
      "2": ["o", "ace"],
      "3": ["o", "ac"],
      "4": ["o", "ceg"],
      "5": ["o", "aceg"],
      "6": ["o", "acg"],
      "7": ["o", "eg"],
      "8": ["o", "aeg"],
      "9": ["o", "ag"]
    },
    "arena" : [
      ".....##.....",
      "....####....",
      "....#55#....",
      "...##56##...",
      ".###89..###.",
      "##2....72.##",
      "##.41...5.##",
      ".###34.6###.",
      "...##.8##...",
      "....#..#....",
      "....####....",
      ".....##....."
    ],
    "molecule": [
      "1447",
      "2558",
      "2558",
      "3669"
    ]
  },
  {
    "id": "22",
    "name": "Acetic acid ethyl ester",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "C"],
      "6": ["2", "cgA"],
      "7": ["3", "cg"],
      "8": ["1", "g"]
    },
    "arena" : [
      "#############",
      "#..##.#.##..#",
      "#...#.4.#...#",
      "#....3...3.3#",
      "##..#...#..##",
      "#7.##6..##..#",
      "#8....#.....#",
      "#..##...##..#",
      "##..#22.#4.##",
      "#...2.......#",
      "#..5#..1#...#",
      "#.4##.#.##..#",
      "#############"
    ],
    "molecule": [
      ".25.22.",
      "1367338",
      ".4..44."
    ]
  },
  {
    "id": "23",
    "name": "Ammonia",
    "atoms": {
      "1": ["1", "c"],
      "2": ["4", "bdg"],
      "3": ["1", "f"],
      "4": ["1", "h"]
    },
    "arena" : [
      "#############",
      "##..3#..#..##",
      "#...#..#....#",
      "#........#..#",
      "#1#.#...#..##",
      "##...#....#.#",
      "#....2......#",
      "#.#....#..4##",
      "##..#...#.#.#",
      "#..#........#",
      "#....#..#...#",
      "##..#..#...##",
      "#############"
    ],
    "molecule": [
      "..3",
      "12.",
      "..4"
    ]
  },
  {
    "id": "24",
    "name": "3-Methyl-Pentane",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["1", "d"],
      "6": ["2", "abeh"],
      "7": ["1", "f"],
      "8": ["1", "g"]
    },
    "arena" : [
      "###############",
      "#.2.5..#..3..4#",
      "#2.#...#...#..#",
      "#.###.12..###.#",
      "#.3#....4..#..#",
      "#.......3.....#",
      "###.2..4....###",
      "#....3.......7#",
      "#8.#.......#.6#",
      "#.###....2###.#",
      "#..#4..#4..#..#",
      "#.....3#......#",
      "###############"
    ],
    "molecule": [
      "..527..",
      ".22622.",
      "1333338",
      ".44444."
    ]
  },
  {
    "id": "25",
    "name": "Propanal",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["2", "cgC"],
      "6": ["3", "A"],
      "7": ["1", "g"]
    },
    "arena" : [
      "###########",
      "#.2#.5....#",
      "#.##4..##.#",
      "#.....3.###",
      "#..###....#",
      "#..#....###",
      "#....#....#",
      "####..#...#",
      "#72.....#.#",
      "#..#.31.#.#",
      "#6##..#.#4#",
      "#.....#...#",
      "###########"
    ],
    "molecule": [
      ".22..",
      "13357",
      ".446."
    ]
  },
  {
    "id": "26",
    "name": "Propyne",
    "atoms": {
      "1": ["1", "c"],
      "2": ["2", "gF"],
      "3": ["1", "e"],
      "4": ["2", "cH"],
      "5": ["2", "aceg"],
      "6": ["1", "a"],
      "7": ["1", "g"]
    },
    "arena" : [
      "#############",
      "#...#####...#",
      "#.#.........#",
      "#.#.2#...#..#",
      "####.###6#..#",
      "#......#.##.#",
      "#3#.......#.#",
      "####.####...#",
      "#.#....#....#",
      "#...#.....#.#",
      "#..###.1.##5#",
      "#.7#....##4.#",
      "#############"
    ],
    "molecule": [
      "...3.",
      "12457",
      "...6."
    ]
  },
  {
    "id": "27",
    "name": "Furanal",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "chC"],
      "4": ["2", "dfA"],
      "5": ["A", ""],
      "6": ["3", "bh"],
      "7": ["2", "bgC"],
      "8": ["1", "f"],
      "9": ["2", "ehB"],
      "a": ["1", "a"],
      "b": ["3", "D"]
    },
    "arena" : [
      "..########...",
      "..#..#...#...",
      "###..5...####",
      "#..8###..9..#",
      "#..a...3#...#",
      "####.#..#6..#",
      "#b...#.###..#",
      "#..........4#",
      "##.####....##",
      "#4.#.7...#..#",
      "#.....#..#..#",
      "####.1#.2#.##",
      "...##########"
    ],
    "molecule": [
      "1...8.",
      ".357..",
      ".4.4..",
      "2.6.9b",
      "....a."
    ]
  },
  {
    "id": "28",
    "name": "Pyran",
    "atoms": {
      "1": ["3", "bd"],
      "2": ["1", "e"],
      "3": ["2", "afB"],
      "4": ["2", "ehB"],
      "5": ["1", "a"],
      "6": ["2", "adD"],
      "7": ["2", "beD"],
      "8": ["2", "bdfh"],
      "9": ["1", "f"],
      "a": ["1", "h"]
    },
    "arena" : [
      ".#######.....",
      ".#.....#.....",
      ".#..6..######",
      ".#.##..#9...#",
      ".#.12.7####.#",
      ".###...#....#",
      "##.##..#.4..#",
      "#.....#2.#..#",
      "#........#5.#",
      "#.3a#5.#.##.#",
      "#####..#....#",
      "...#...#8####",
      "...#######..."
    ],
    "molecule": [
      ".22..",
      ".36.9",
      "1..8.",
      ".47.a",
      ".55.."
    ]
  },
  {
    "id": "29",
    "name": "Cyclo-Pentane",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "c"],
      "3": ["2", "begh"],
      "4": ["2", "aceg"],
      "5": ["1", "a"],
      "6": ["2", "bdfh"],
      "7": ["2", "bdeg"],
      "8": ["1", "f"],
      "9": ["1", "h"]
    },
    "arena" : [
      ".########....",
      ".#5.....#####",
      ".##..2#..9..#",
      ".#..6#1###7.#",
      ".#.......####",
      ".#........#..",
      ".##2#.8#..#..",
      "..###.##..#..",
      "###453....###",
      "#1..##..#9..#",
      "#....6..#####",
      "#.#....8#....",
      "#########...."
    ],
    "molecule": [
      ".1.8.",
      "1.6.8",
      "23.6.",
      "247.9",
      ".559."
    ]
  },
  {
    "id": "30",
    "name": "Nitro-Glycerin",
    "atoms": {
      "1": ["3", "cd"],
      "2": ["1", "c"],
      "3": ["4", "beg"],
      "4": ["3", "ah"],
      "5": ["1", "e"],
      "6": ["2", "aceg"],
      "7": ["3", "af"],
      "8": ["A", ""],
      "9": ["4", "beh"],
      "a": ["3", "ae"],
      "b": ["1", "a"],
      "c": ["3", "fg"],
      "d": ["3", "ad"],
      "e": ["1", "g"],
      "f": ["4", "ceh"],
      "g": ["3", "ba"]
    },
    "arena" : [
      "..#########..",
      "..#.......#..",
      "..#.1#f..####",
      "..#6#9#.c...#",
      "..#1#...e..4#",
      "###.###...###",
      "#.....3..a.c#",
      "#.5#2.6.##..#",
      "#.###8#..####",
      "#g##..#b..d7#",
      "#6....##..###",
      "#######5..#..",
      "......#####.."
    ],
    "molecule": [
      "..18c..",
      "...9...",
      "..5a5..",
      ".2666e.",
      "..7bd..",
      "13...fc",
      ".4...g."
    ]
  },
  {
    "id": "31",
    "name": "Ethane",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["1", "e"],
      "4": ["2", "abeh"],
      "5": ["2", "adef"],
      "6": ["1", "a"],
      "7": ["1", "f"],
      "8": ["1", "h"]
    },
    "arena" : [
      "..#########",
      "..#....2..#",
      "..#..#.#..#",
      "..#3.5.####",
      "#####4.#1#.",
      "#...#....#.",
      "#........#.",
      "#.6.###8.##",
      "###7...#..#",
      "#.........#",
      "###########"
    ],
    "molecule": [
      "137",
      ".4.",
      ".5.",
      "268"
    ]
  },
  {
    "id": "32",
    "name": "Crystal 2",
    "atoms": {
      "1": ["o", "bd"],
      "2": ["o", "bdf"],
      "3": ["o", "bdh"],
      "4": ["o", "df"],
      "5": ["o", "bdfh"],
      "6": ["o", "bh"],
      "7": ["o", "dfh"],
      "8": ["o", "bfh"],
      "9": ["o", "fh"]
    },
    "arena" : [
      "...###...",
      "..##8##..",
      ".##..2##.",
      "##7.5..##",
      "#...6..4#",
      "##3..9.##",
      ".##..1##.",
      "..##.##..",
      "...###..."
    ],
    "molecule": [
      "..4..",
      ".2.7.",
      "1.5.9",
      ".3.8.",
      "..6.."
    ]
  },
  {
    "id": "33",
    "name": "Ethylene-Glycol",
    "atoms": {
      "1": ["1", "b"],
      "2": ["1", "d"],
      "3": ["3", "bf"],
      "4": ["2", "bdfh"],
      "5": ["1", "f"],
      "6": ["1", "h"]
    },
    "arena" : [
      "...##########",
      "...#....3#..#",
      "...#.#...#..#",
      "...#..#.....#",
      "####...#...##",
      "#..4#..4#...#",
      "#...2#1..#.##",
      "#..#..#.5#.#.",
      "##1##..#.###.",
      ".#....3...#..",
      ".#....#.6.#..",
      ".######.5.#..",
      "......#####.."
    ],
    "molecule": [
      ".2.5.5",
      "..4.3.",
      ".3.4..",
      "1.1.6."
    ]
  },
  {
    "id": "34",
    "name": "L-Alanine",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "c"],
      "3": ["1", "b"],
      "4": ["2", "cfgh"],
      "5": ["1", "e"],
      "6": ["2", "aceg"],
      "7": ["4", "adf"],
      "8": ["3", "C"],
      "9": ["2", "cgA"],
      "a": ["1", "h"],
      "b": ["3", "cg"],
      "c": ["1", "g"]
    },
    "arena" : [
      "#############",
      "#.b.4...5...#",
      "#.####..##..#",
      "#..3......3.#",
      "##.....7..#.#",
      "#..#.#.#2.#.#",
      "#.1..#.#..#.#",
      "#c#......9..#",
      "#.#....8....#",
      "#....#..#a.##",
      "#..#.#..#...#",
      "#....#6.#...#",
      "#############"
    ],
    "molecule": [
      "1.58..",
      "2469bc",
      "3.7...",
      ".3.a.."
    ]
  },
  {
    "id": "35",
    "name": "Cyanoguanidine",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["4", "cfh"],
      "4": ["2", "cgC"],
      "5": ["4", "eA"],
      "6": ["1", "a"],
      "7": ["1", "e"],
      "8": ["4", "acg"],
      "9": ["2", "gG"],
      "a": ["4", "E"]
    },
    "arena" : [
      "###########",
      "#.#.......#",
      "#9###53##.#",
      "#..2.#..a.#",
      "####....###",
      "#.....8...#",
      "#7..#6###1#",
      "#......4..#",
      "###########"
    ],
    "molecule": [
      "1..7.",
      ".3489",
      "2.5.a",
      "..6.."
    ]
  },
  {
    "id": "36",
    "name": "Prussic Acid (Cyanic Acid)",
    "atoms": {
      "1": ["1", "e"],
      "2": ["2", "aG"],
      "3": ["4", "E"]
    },
    "arena" : [
      "..####.......",
      "..#..####....",
      "..##....####.",
      "..#.3.#.#..#.",
      "..###.#....#.",
      "..#...1...##.",
      "..##2......##",
      "###.....#...#",
      "#.###...#####",
      "#.........#..",
      "###########.."
    ],
    "molecule": [
      "1",
      "2",
      "3"
    ]
  },
  {
    "id": "37",
    "name": "Anthracene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "c"],
      "3": ["1", "b"],
      "4": ["2", "ehB"],
      "5": ["2", "agC"],
      "6": ["2", "cfA"],
      "7": ["2", "beD"],
      "8": ["2", "acC"],
      "9": ["2", "egA"],
      "a": ["2", "faB"],
      "b": ["1", "f"],
      "c": ["2", "bgC"],
      "d": ["2", "ceA"],
      "e": ["2", "aeD"],
      "f": ["2", "adD"],
      "g": ["1", "g"],
      "h": ["1", "h"]
    },
    "arena" : [
      "###############",
      "#..4##.....#..#",
      "#..h##...7###.#",
      "#..d......1...#",
      "#.......b....##",
      "####..c.###d.##",
      "#....ce.g...3.#",
      "#..........##f#",
      "##....#....a#.#",
      "#9.#2.#b5.....#",
      "#.###.####....#",
      "#..#8.b..3....#",
      "#.....#....#..#",
      "#.a..#.....#63#",
      "###############"
    ],
    "molecule": [
      "1..b..",
      ".47.b.",
      "258c.b",
      ".69dc.",
      "3.aedg",
      ".3.af.",
      "..3..h"
    ]
  },
  {
    "id": "38",
    "name": "Thiazole",
    "atoms": {
      "1": ["1", "d"],
      "2": ["2", "ehB"],
      "3": ["C", ""],
      "4": ["4", "aB"],
      "5": ["2", "bdD"],
      "6": ["1", "f"],
      "7": ["5", "fh"],
      "8": ["1", "h"]
    },
    "arena" : [
      ".###########.",
      "##.........##",
      "#..#..###.1.#",
      "#.##..#8..#.#",
      "#3#...#..##.#",
      "#.........5.#",
      "#...###.5####",
      "#.#2#7..#4..#",
      "#6#.#.......#",
      "#.#.....#...#",
      "#......##...#",
      "##.....#...##",
      ".###########."
    ],
    "molecule": [
      "1..6",
      ".25.",
      ".3.7",
      ".45.",
      "...8"
    ]
  },
  {
    "id": "39",
    "name": "Saccharin",
    "atoms": {
      "1": ["1", "c"],
      "2": ["4", "bdg"],
      "3": ["3", "C"],
      "4": ["2", "dfA"],
      "5": ["5", "chCD"],
      "6": ["2", "ehB"],
      "7": ["2", "agB"],
      "8": ["1", "e"],
      "9": ["2", "acD"],
      "a": ["2", "ceD"],
      "b": ["1", "a"],
      "c": ["2", "bgC"],
      "d": ["2", "dgA"],
      "e": ["1", "f"],
      "f": ["1", "h"],
      "g": ["3", "A"],
      "h": ["3", "B"]
    },
    "arena" : [
      "......#####..",
      ".######d#h###",
      ".#4..3#.#.9e#",
      ".#......b...#",
      ".#c#5.......#",
      "##....#.#...#",
      "#.....#8#1..#",
      "#..f###.###.#",
      "#.....2.....#",
      "####...#g.###",
      "...#a...7.#..",
      "...###..6.#..",
      ".....######.."
    ],
    "molecule": [
      "..3....",
      "..4.8.e",
      "12.69c.",
      ".h57ad.",
      "..g.b.f"
    ]
  },
  {
    "id": "40",
    "name": "Styrene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "fhB"],
      "4": ["2", "bdD"],
      "5": ["1", "c"],
      "6": ["1", "f"],
      "7": ["2", "chC"],
      "8": ["2", "egA"],
      "9": ["2", "afB"],
      "a": ["2", "bgC"],
      "b": ["2", "ceA"],
      "c": ["2", "adD"],
      "d": ["1", "g"],
      "e": ["1", "h"]
    },
    "arena" : [
      "###########",
      "#.......#.#",
      "#..b###.8.#",
      "#...16#.#.#",
      "#...2e.6#5#",
      "#..c.3.##.#",
      "#.##2##.7##",
      "#..#4#....#",
      "##9#.#.d..#",
      "#......#.a#",
      "###########"
    ],
    "molecule": [
      "1..6..",
      ".34..6",
      "2..7a.",
      "..58bd",
      "...9c.",
      "..2..e"
    ]
  },
  {
    "id": "41",
    "name": "Melamine",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["4", "acg"],
      "4": ["2", "cgC"],
      "5": ["4", "cA"],
      "6": ["1", "b"],
      "7": ["4", "gec"],
      "8": ["2", "aceg"],
      "9": ["4", "adf"],
      "a": ["4", "gA"],
      "b": ["1", "h"],
      "c": ["1", "g"]
    },
    "arena" : [
      "####.....####",
      "#..#######3.#",
      "#9....#....6#",
      "##.#.4#..#.##",
      ".#.........#.",
      ".#....2..2.#.",
      ".###.13c.###.",
      ".#7........#.",
      ".#....4...a#.",
      "##.#..#5.#.##",
      "#.....#8....#",
      "#b.#######..#",
      "####.....####"
    ],
    "molecule": [
      ".2...2.",
      "134743c",
      "..58a..",
      "...9...",
      "..6.b.."
    ]
  },
  {
    "id": "42",
    "name": "Cyclobutane",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["1", "g"]
    },
    "arena" : [
      "########...",
      "#...1..##..",
      "##5.##.4###",
      ".##.3.....#",
      "..#.#.....#",
      ".##1##..#3#",
      ".#...5#2###",
      ".#......#.#",
      ".#3...3..2#",
      ".#4.#.###.#",
      ".######.###"
    ],
    "molecule": [
      ".22.",
      "1335",
      "1335",
      ".44."
    ]
  },
  {
    "id": "43",
    "name": "Nicotine",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "c"],
      "3": ["2", "ehB"],
      "4": ["2", "agC"],
      "5": ["4", "cA"],
      "6": ["1", "e"],
      "7": ["2", "aeD"],
      "8": ["2", "acC"],
      "9": ["2", "egA"],
      "a": ["1", "a"],
      "b": ["1", "b"],
      "c": ["2", "abeg"],
      "d": ["4", "ace"],
      "e": ["2", "adef"],
      "f": ["2", "abdf"],
      "g": ["2", "bdeg"],
      "h": ["1", "h"],
      "i": ["1", "f"],
      "j": ["2", "bdfh"]
    },
    "arena" : [
      "..###########",
      ".##.h..#a..i#",
      ".#3...6#...##",
      "##..#..6...#.",
      "#...2..j#d.##",
      "#.a#....a#.e#",
      "#.#g..c...#.#",
      "##h..8.9..h##",
      "#......1....#",
      "##7f#5.#.6.##",
      ".#..b#.#4###.",
      ".##..i####...",
      "..#####......"
    ],
    "molecule": [
      "1.6.6i.",
      ".376f.i",
      "248c.j.",
      ".59dg.h",
      "..aeah.",
      "..bah.."
    ]
  },
  {
    "id": "44",
    "name": "Acetyle salicylic acid",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "B"],
      "3": ["1", "b"],
      "4": ["1", "e"],
      "5": ["2", "aceg"],
      "6": ["2", "aeD"],
      "7": ["3", "ae"],
      "8": ["2", "acC"],
      "9": ["2", "egA"],
      "a": ["2", "afB"],
      "b": ["1", "g"],
      "c": ["2", "bgC"],
      "d": ["2", "ceA"],
      "e": ["2", "adD"],
      "f": ["2", "cfA"],
      "g": ["1", "h"],
      "h": ["3", "cg"],
      "i": ["3", "C"]
    },
    "arena" : [
      "........#####",
      "#########9..#",
      "#.#..f..#..##",
      "#..#2#.5.#1.#",
      "#7...8..b...#",
      "###6#.#.#bh.#",
      "..#.e#.1.#d.#",
      "..##c..g.b###",
      "..#.#.#.4i#..",
      ".##...3#..#..",
      ".#..a#..###..",
      ".########...."
    ],
    "molecule": [
      ".4....",
      "15b...",
      "26.i..",
      ".7.fhb",
      ".8c...",
      "19db..",
      ".ae...",
      "3..g.."
    ]
  },
  {
    "id": "45",
    "name": "Meta-Di-Nitro-Benzene",
    "atoms": {
      "1": ["3", "B"],
      "2": ["1", "d"],
      "3": ["4", "bD"],
      "4": ["3", "D"],
      "5": ["2", "ehB"],
      "6": ["2", "afB"],
      "7": ["1", "e"],
      "8": ["2", "acD"],
      "9": ["2", "ceD"],
      "a": ["1", "a"],
      "b": ["2", "bgC"],
      "c": ["2", "dgA"],
      "d": ["1", "f"],
      "e": ["4", "hB"]
    },
    "arena" : [
      ".####.......",
      ".#..####....",
      ".#e2..4#....",
      ".#.#..8#....",
      "##b...##....",
      "#.#....####.",
      "#....c....#.",
      "#7..##.6d.##",
      "#5#..9.#...#",
      "#.#1...#...#",
      "#....3.a.#.#",
      "####.#.#####",
      "...#####...."
    ],
    "molecule": [
      ".2.7.d.",
      "..58b..",
      "..69c..",
      "13.a.e4"
    ]
  },
  {
    "id": "46",
    "name": "Propyne",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "e"],
      "3": ["2", "abeh"],
      "4": ["2", "aG"],
      "5": ["2", "eE"],
      "6": ["1", "a"],
      "7": ["1", "f"]
    },
    "arena" : [
      ".##########",
      ".#........#",
      ".#.1#.....#",
      "##.##.6#.##",
      "#.....##7#.",
      "##.....####",
      "#4.#......#",
      "###5.#.3#.#",
      "..#..####.#",
      "..#..#..#.#",
      "..#..#..#.#",
      "..####..#2#",
      "........###"
    ],
    "molecule": [
      "127",
      ".3.",
      ".4.",
      ".5.",
      ".6."
    ]
  },
  {
    "id": "47",
    "name": "Malonic Acid",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "dg"],
      "3": ["2", "chC"],
      "4": ["3", "C"],
      "5": ["1", "e"],
      "6": ["2", "aceg"],
      "7": ["1", "a"],
      "8": ["2", "dgA"],
      "9": ["3", "ch"],
      "a": ["1", "g"],
      "b": ["3", "A"]
    },
    "arena" : [
      "#########..",
      "#.#2....#..",
      "#.#...6.#..",
      "#..4.##5#..",
      "#..b..#7#..",
      "#..#.1a.###",
      "#..##.....#",
      "#.39.8....#",
      "#.........#",
      "###..#....#",
      "..#########"
    ],
    "molecule": [
      "12.54..",
      "..368..",
      "..b7.9a"
    ]
  },
  {
    "id": "48",
    "name": "2,2-Dimethylpropane",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "c"],
      "3": ["1", "b"],
      "4": ["2", "cfgh"],
      "5": ["1", "e"],
      "6": ["2", "abeh"],
      "7": ["2", "aceg"],
      "8": ["2", "adfe"],
      "9": ["1", "a"],
      "a": ["1", "f"],
      "b": ["2", "bcdg"],
      "c": ["1", "h"],
      "d": ["1", "g"]
    },
    "arena" : [
      ".....######..",
      "####.#..1a#..",
      "#5.#.##.3c#..",
      "#9.###6#..#..",
      "##...47b..###",
      "#.#..#8.....#",
      "#...#..#..#.#",
      "#.#...#.....#",
      "#..#....#.1a#",
      "#.2d......3c#",
      "#....#..#####",
      "##....#.#....",
      ".########...."
    ],
    "molecule": [
      ".15a.",
      "1.6.a",
      "247bd",
      "3.8.c",
      ".39c."
    ]
  },
  {
    "id": "49",
    "name": "Ethyl-Benzene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "ehB"],
      "4": ["2", "afB"],
      "5": ["1", "e"],
      "6": ["2", "abeh"],
      "7": ["2", "acD"],
      "8": ["2", "ceD"],
      "9": ["1", "a"],
      "a": ["1", "f"],
      "b": ["2", "bgC"],
      "c": ["2", "dgA"],
      "d": ["1", "h"]
    },
    "arena" : [
      "#####......",
      "#15a#######",
      "#.6......8#",
      "##9.c#...##",
      "#.#.#7...3#",
      "#........4#",
      "#..1##...##",
      "#..2.#.a..#",
      "###..b#d..#",
      "..#...##..#",
      "..#.#.....#",
      "..#.#.16a.#",
      "..#########"
    ],
    "molecule": [
      ".15a.",
      ".16a.",
      "1.6.a",
      ".37b.",
      ".48c.",
      "2.9.d"
    ]
  },
  {
    "id": "50",
    "name": "L-Asparagine",
    "atoms": {
      "1": ["3", "B"],
      "2": ["1", "d"],
      "3": ["1", "b"],
      "4": ["1", "e"],
      "5": ["3", "ae"],
      "6": ["2", "acD"],
      "7": ["1", "c"],
      "8": ["4", "cfh"],
      "9": ["2", "aceg"],
      "a": ["2", "agC"],
      "b": ["4", "bdg"],
      "c": ["1", "g"],
      "d": ["1", "f"],
      "e": ["1", "h"],
      "f": ["3", "A"]
    },
    "arena" : [
      ".#########.",
      ".#44#.1f5#.",
      "##c.#..####",
      "#7......#b#",
      "#2........#",
      "#3........#",
      "#d#.#.....#",
      "#e..##..#8#",
      "##..#...###",
      ".#...699a#.",
      ".#########."
    ],
    "molecule": [
      ".4...",
      ".54.d",
      "169b.",
      "279ce",
      ".8a..",
      "3.f.."
    ]
  },
  {
    "id": "51",
    "name": "1,3,5,7-Cyclooctatetraene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "bhC"],
      "4": ["2", "dfA"],
      "5": ["2", "fhB"],
      "6": ["2", "bdD"],
      "7": ["1", "f"],
      "8": ["1", "h"]
    },
    "arena" : [
      "########..###",
      "#1....1####5#",
      "#..#5.#4....#",
      "#..##.##...4#",
      "#..........##",
      "#.#3..#3.#7#.",
      "#.##..##.###.",
      "#7.........##",
      "##.#8.#8...6#",
      ".#.##.##6#2.#",
      ".#.......##.#",
      ".#2.###.....#",
      ".####.#######"
    ],
    "molecule": [
      ".1..7.",
      "1.56.7",
      ".3..3.",
      ".4..4.",
      "2.56.8",
      ".2..8."
    ]
  },
  {
    "id": "52",
    "name": "Vanillin",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "C"],
      "3": ["2", "dgA"],
      "4": ["1", "b"],
      "5": ["2", "ehB"],
      "6": ["2", "afB"],
      "7": ["1", "e"],
      "8": ["2", "acD"],
      "9": ["2", "ceD"],
      "a": ["1", "a"],
      "b": ["2", "bgC"],
      "c": ["1", "d"],
      "d": ["3", "bf"],
      "e": ["3", "dh"],
      "f": ["2", "bdfh"],
      "g": ["1", "h"],
      "h": ["1", "f"]
    },
    "arena" : [
      ".......######",
      "#########.c.#",
      "#d.....f##h.#",
      "#.....b.4..g#",
      "#.#.........#",
      "##...#..87.##",
      "#5...3#2#1.6#",
      "##.#...#9..3#",
      "#..#..#e#..#.",
      "#.a#.#g..#.#.",
      "####...#...#.",
      "...#...#...#.",
      "...#########."
    ],
    "molecule": [
      ".....c.h",
      ".2....f.",
      "13.7.d.g",
      "..58b...",
      "..693...",
      ".4.a.e..",
      "......g."
    ]
  },
  {
    "id": "53",
    "name": "Crystal 3",
    "atoms": {
      "1": ["o", "bc"],
      "2": ["o", "bcf"],
      "3": ["o", "bcg"],
      "4": ["o", "bcfg"],
      "5": ["o", "cf"],
      "6": ["o", "bg"],
      "7": ["o", "cfg"],
      "8": ["o", "bfg"],
      "9": ["o", "fg"]
    },
    "arena" : [
      "#########",
      "#8.1.4..#",
      "#.6.4.4.#",
      "#8.3.4.9#",
      "#.3.2.7.#",
      "#..2.7.5#",
      "#########"
    ],
    "molecule": [
      "...5779",
      "..2448.",
      ".2448..",
      "1336..."
    ]
  },
  {
    "id": "54",
    "name": "Uric Acid",
    "atoms": {
      "1": ["1", "d"],
      "2": ["3", "B"],
      "3": ["1", "b"],
      "4": ["4", "ceh"],
      "5": ["2", "aeD"],
      "6": ["4", "acf"],
      "7": ["2", "egA"],
      "8": ["2", "acC"],
      "9": ["2", "dgA"],
      "a": ["4", "bdg"],
      "b": ["4", "beh"],
      "c": ["1", "a"],
      "d": ["1", "f"],
      "e": ["2", "fhB"],
      "f": ["3", "D"],
      "g": ["3", "C"]
    },
    "arena" : [
      "###########..",
      "#.#.#.#...#..",
      "#...#.#...#..",
      "#.47.8#..g#..",
      "###...#.#####",
      "#.5....b#9..#",
      "#.....cf....#",
      "#..2#.d....6#",
      "#####1#...###",
      "..#...#.....#",
      "..#...#.#...#",
      "..#..3#e#a#.#",
      "..###########"
    ],
    "molecule": [
      "1.g...",
      ".47.d.",
      "258a..",
      ".69.ef",
      "3..b..",
      "...c.."
    ]
  },
  {
    "id": "55",
    "name": "Thymine",
    "atoms": {
      "1": ["1", "c"],
      "2": ["4", "bdg"],
      "3": ["1", "b"],
      "4": ["3", "C"],
      "5": ["2", "cfA"],
      "6": ["2", "fhB"],
      "7": ["2", "bdD"],
      "8": ["1", "f"],
      "9": ["2", "cdeh"],
      "a": ["1", "a"],
      "b": ["1", "g"],
      "c": ["1", "h"],
      "d": ["3", "D"]
    },
    "arena" : [
      "####....#####",
      "#31#....#.7.#",
      "#..######.#.#",
      "#..##28##.#.#",
      "#..........b#",
      "#.##....##a.#",
      "#..6.##..5#.#",
      "#4##....##..#",
      "#....##...#.#",
      "#.#.c.....#.#",
      "#.6##..##...#",
      "#....d2....9#",
      "#############"
    ],
    "molecule": [
      "..4.8.",
      "..52..",
      "12..6d",
      "..67..",
      ".3..9b",
      "....ac"
    ]
  },
  {
    "id": "56",
    "name": "Aniline",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "d"],
      "3": ["4", "beg"],
      "4": ["1", "a"],
      "5": ["2", "ehB"],
      "6": ["2", "agC"],
      "7": ["2", "cfA"],
      "8": ["1", "e"],
      "9": ["2", "adD"],
      "a": ["A", ""],
      "b": ["2", "chC"],
      "c": ["2", "dgA"],
      "d": ["1", "g"],
      "e": ["1", "h"]
    },
    "arena" : [
      "......#######",
      "#######c#...#",
      "#..34#......#",
      "#.......##7##",
      "#...##d1a###.",
      "##6..##....#.",
      "###...e.#..#.",
      "#1.........#.",
      "#..##...##.#.",
      "#..b##...####",
      "#..8......5.#",
      "#######9#..2#",
      "......#######"
    ],
    "molecule": [
      ".2.8..",
      "..59..",
      ".16.bd",
      "..7ac.",
      "13...e",
      ".4...."
    ]
  },
  {
    "id": "57",
    "name": "Chloroform",
    "atoms": {
      "1": ["7", "d"],
      "2": ["7", "b"],
      "3": ["2", "bdfh"],
      "4": ["1", "f"],
      "5": ["7", "h"]
    },
    "arena" : [
      "....#######",
      "....#...#.#",
      "#######.#.#",
      "#...##....#",
      "###1#.....#",
      "#......4#.#",
      "#..#...#3.#",
      "#.2########",
      "#..5.#.....",
      "####.#.....",
      "#....#.....",
      "######....."
    ],
    "molecule": [
      "1.4",
      ".3.",
      "2.5"
    ]
  },
  {
    "id": "58",
    "name": "Carbonic acid",
    "atoms": {
      "1": ["1", "e"],
      "2": ["3", "ad"],
      "3": ["3", "be"],
      "4": ["1", "a"],
      "5": ["2", "fhB"],
      "6": ["3", "D"]
    },
    "arena" : [
      "##########.",
      "#...#.1..##",
      "#...#.#...#",
      "#2.##.##..#",
      "##.6......#",
      "##........#",
      "#...##.##.#",
      "#....#5#..#",
      "###...3#..#",
      "..#....4..#",
      "..#########"
    ],
    "molecule": [
      "1..",
      "2..",
      ".56",
      "3..",
      "4.."
    ]
  },
  {
    "id": "59",
    "name": "Crystal 4",
    "atoms": {
      "1": ["o", "be"],
      "2": ["o", "ad"],
      "3": ["o", "bdf"],
      "4": ["o", "bdh"],
      "5": ["o", "cf"],
      "6": ["o", "ceh"],
      "7": ["o", "acf"],
      "8": ["o", "ch"],
      "9": ["o", "dg"],
      "a": ["o", "beg"],
      "b": ["o", "adg"],
      "c": ["o", "bg"],
      "d": ["o", "dfh"],
      "e": ["o", "bfh"],
      "f": ["o", "eh"],
      "g": ["o", "af"]
    },
    "arena" : [
      "##.......##",
      "###########",
      ".#.56..d.#.",
      ".#...ba..#.",
      ".#97...f.#.",
      ".#.......#.",
      ".#2...e.3#.",
      ".#..84...#.",
      ".#c...g.1#.",
      "###########",
      "##.......##"
    ],
    "molecule": [
      "..59..",
      ".3..d.",
      "1.6a.f",
      "2.7b.g",
      ".4..e.",
      "..8c.."
    ]
  },
  {
    "id": "60",
    "name": "Acrylo-Nitril",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "bhC"],
      "4": ["2", "dfA"],
      "5": ["2", "fF"],
      "6": ["1", "h"],
      "7": ["4", "H"]
    },
    "arena" : [
      "#####.#####",
      "#..1#.#5..#",
      "#..#####7.#",
      "#..#.3.#..#",
      "#..#...#..#",
      "#.........#",
      "##..###..##",
      ".#...4...#.",
      ".#.2.#.6.#.",
      ".#.#.#.#.#.",
      ".#########."
    ],
    "molecule": [
      "1.57",
      ".3..",
      ".4..",
      "2.6."
    ]
  },
  {
    "id": "61",
    "name": "Furan",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "bhC"],
      "4": ["2", "cfA"],
      "5": ["3", "df"],
      "6": ["A", ""],
      "7": ["2", "dgA"],
      "8": ["1", "f"],
      "9": ["1", "h"]
    },
    "arena" : [
      "#############",
      "#1..#...#..8#",
      "#.#3.4#7.3#.#",
      "#..#.#.#.#..#",
      "#...#.5.#...#",
      "##.........##",
      "#...##.##...#",
      "#2.#..6..#.9#",
      "#############"
    ],
    "molecule": [
      "1.5.8",
      ".3.3.",
      ".467.",
      "2...9"
    ]
  },
  {
    "id": "62",
    "name": "l-Lactic acid",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["3", "dh"],
      "4": ["2", "bdfh"],
      "5": ["1", "h"],
      "6": ["3", "C"],
      "7": ["2", "cfA"],
      "8": ["3", "cg"],
      "9": ["1", "g"]
    },
    "arena" : [
      "###############",
      "###############",
      "##1.#.......4##",
      "##..#3..#....##",
      "##.....#...####",
      "##.....#...4.##",
      "##.#..7#6....##",
      "##..#######..##",
      "##....9#8..#.##",
      "##.1...#.....##",
      "####...#.....##",
      "##....#..5#..##",
      "##2.......#.5##",
      "###############",
      "###############"
    ],
    "molecule": [
      "1..6..",
      ".3.789",
      "1.4...",
      ".4.5..",
      "2.5..."
    ]
  },
  {
    "id": "63",
    "name": "Maleic Acid",
    "atoms": {
      "1": ["3", "B"],
      "2": ["3", "be"],
      "3": ["2", "adD"],
      "4": ["2", "beD"],
      "5": ["3", "ad"],
      "6": ["1", "f"],
      "7": ["2", "bhC"],
      "8": ["2", "dfA"],
      "9": ["1", "h"]
    },
    "arena" : [
      "#############",
      "#6...8#5...7#",
      "#.#..###..#.#",
      "##6...#...9##",
      ".#..#1#3#..#.",
      ".##..###..##.",
      ".#....#....#.",
      "##1...#...2##",
      "#.#.......#.#",
      "#4.........9#",
      "#############"
    ],
    "molecule": [
      "..6.",
      ".2..",
      "13.6",
      "..7.",
      "..8.",
      "14.9",
      ".5..",
      "..9."
    ]
  },
  {
    "id": "64",
    "name": "meso-Tartaric acid",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "dg"],
      "3": ["2", "chC"],
      "4": ["3", "C"],
      "5": ["1", "e"],
      "6": ["2", "aceg"],
      "7": ["3", "ae"],
      "8": ["1", "a"],
      "9": ["2", "dgA"],
      "a": ["3", "ch"],
      "b": ["1", "g"],
      "c": ["3", "A"]
    },
    "arena" : [
      ".......###...",
      ".#######2####",
      ".#.3....5...#",
      "##.a#.....#.#",
      "#..#4#...#7.#",
      "#.#...#.#.5.#",
      "#...........#",
      "#...#.....#.#",
      "#87#6#...#..#",
      "#.#..6#.#bc.#",
      "#....#...#8.#",
      "###.......#.#",
      "..#..9......#",
      "..####1######",
      ".....###....."
    ],
    "molecule": [
      "....5...",
      "12.574..",
      "..3669..",
      "..c78.ab",
      "...8...."
    ]
  },
  {
    "id": "65",
    "name": "Crystal 5",
    "atoms": {
      "1": ["o", "cd"],
      "2": ["o", "bc"],
      "3": ["o", "de"],
      "4": ["o", "adg"],
      "5": ["o", "cfh"],
      "6": ["o", "beg"],
      "7": ["o", "ab"],
      "8": ["o", "beh"],
      "9": ["o", "adf"],
      "a": ["o", "ef"],
      "b": ["o", "acf"],
      "c": ["o", "bdg"],
      "d": ["o", "ceh"],
      "e": ["o", "ah"],
      "f": ["o", "fg"],
      "g": ["o", "gh"],
      "h": ["o", "abcdefgh"]
    },
    "arena" : [
      "#########",
      "#########",
      "##..3.a##",
      "##19c6b##",
      "##d.7.f##",
      "##ghe58##",
      "##2...4##",
      "#########",
      "#########"
    ],
    "molecule": [
      ".3.a.",
      "148bf",
      ".5hc.",
      "269dg",
      ".7.e."
    ]
  },
  {
    "id": "66",
    "name": "Formic acid ethyl ester",
    "atoms": {
      "1": ["1", "c"],
      "2": ["2", "bgC"],
      "3": ["3", "A"],
      "4": ["1", "d"],
      "5": ["3", "bf"],
      "6": ["2", "bdfh"],
      "7": ["1", "h"],
      "8": ["1", "f"]
    },
    "arena" : [
      "....#########",
      ".####.....2.#",
      ".#..#..###.6#",
      ".#.4.......##",
      "##.###1.#...#",
      "#..........6#",
      "#.......4...#",
      "#8#.#.###...#",
      "###........##",
      "..#.....7#3#.",
      "..#7###.####.",
      "..#.5...#....",
      "..#######...."
    ],
    "molecule": [
      "...4.8",
      "..4.6.",
      "...6.7",
      "..5.7.",
      "12....",
      ".3...."
    ]
  },
  {
    "id": "67",
    "name": "1,4-Cyclohexadiene",
    "atoms": {
      "1": ["1", "h"],
      "2": ["1", "d"],
      "3": ["2", "bdfh"],
      "4": ["1", "b"],
      "5": ["2", "fhB"],
      "6": ["2", "bdD"],
      "7": ["1", "f"]
    },
    "arena" : [
      "..#######..",
      "..#2...4#..",
      "###..#..###",
      "#6#..#..#5#",
      "#.4.....1.#",
      "##..7#5..##",
      "#.3.....3.#",
      "#6#..#..#7#",
      "###..#..###",
      "..#2...1#..",
      "..#######.."
    ],
    "molecule": [
      ".2..7.",
      "2.56.7",
      ".3..3.",
      "4.56.1",
      ".4..1."
    ]
  },
  {
    "id": "68",
    "name": "Squaric acid",
    "atoms": {
      "1": ["3", "C"],
      "2": ["2", "ceA"],
      "3": ["2", "egA"],
      "4": ["2", "afB"],
      "5": ["2", "adD"],
      "6": ["1", "c"],
      "7": ["3", "bg"],
      "8": ["3", "ch"],
      "9": ["1", "g"]
    },
    "arena" : [
      "###########",
      "#5..#.....#",
      "#9.3#....2#",
      "#.....#####",
      "#.........#",
      "#.........#",
      "#...#.....#",
      "#.1.#4#...#",
      "#.###1#...#",
      "#.87####..#",
      "#...#.....#",
      "#.6.#.....#",
      "###########"
    ],
    "molecule": [
      "..11..",
      "..23..",
      "..45..",
      "67..89",
      "......"
    ]
  },
  {
    "id": "69",
    "name": "Ascorbic acid",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "d"],
      "3": ["1", "f"],
      "4": ["3", "bf"],
      "5": ["1", "g"],
      "6": ["2", "bdfh"],
      "7": ["1", "b"],
      "8": ["2", "afB"],
      "9": ["2", "bdD"],
      "a": ["2", "fhB"],
      "b": ["3", "df"],
      "c": ["2", "befh"],
      "d": ["3", "D"],
      "e": ["3", "ch"],
      "f": ["3", "bg"]
    },
    "arena" : [
      "###############",
      "#d376.........#",
      "#2#........c#.#",
      "#a.#6......#b.#",
      "#...#.....#..7#",
      "#....#...#8..f#",
      "#.....#.#....5#",
      "#......#.....3#",
      "#.....#.#.....#",
      "#....#...#....#",
      "#...#.....#1..#",
      "#..#.......#..#",
      "#.#.........#9#",
      "#.4........e47#",
      "###############"
    ],
    "molecule": [
      ".2.3.3..",
      "..6.4...",
      ".4.6.b..",
      "7.7.c.ad",
      "...789..",
      "..1f..e5"
    ]
  },
  {
    "id": "70",
    "name": "Phosgene",
    "atoms": {
      "1": ["3", "C"],
      "2": ["2", "dfA"],
      "3": ["7", "b"],
      "4": ["7", "h"]
    },
    "arena" : [
      "#############",
      "#....4#23...#",
      "#.....#....1#",
      "#...#####...#",
      "#.....#.....#",
      "#.....#.....#",
      "#...........#",
      "#...........#",
      "#...........#",
      "#############"
    ],
    "molecule": [
      ".1.",
      ".2.",
      "3.4"
    ]
  },
  {
    "id": "71",
    "name": "Thiophene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "b"],
      "3": ["2", "dfA"],
      "4": ["2", "chC"],
      "5": ["1", "f"],
      "6": ["1", "h"],
      "7": ["A", ""],
      "8": ["2", "bgC"],
      "9": ["5", "bh"]
    },
    "arena" : [
      "##############",
      "#..3#....6#..#",
      "#..5#........#",
      "#...#........#",
      "#...#........#",
      "#...#...#.####",
      "#78..........#",
      "##.......3#..#",
      "#.4....#....##",
      "#.#..#.#.....#",
      "#91..#.#2....#",
      "##############"
    ],
    "molecule": [
      "1...5",
      ".478.",
      ".3.3.",
      "2.9.6"
    ]
  },
  {
    "id": "72",
    "name": "Urea",
    "atoms": {
      "1": ["1", "c"],
      "2": ["1", "e"],
      "3": ["4", "adg"],
      "4": ["1", "a"],
      "5": ["4", "beg"],
      "6": ["2", "fhB"],
      "7": ["3", "D"]
    },
    "arena" : [
      "##########...",
      "##1..3#..#...",
      "#.#...#..####",
      "#.#.........#",
      "#.##..#.#.#.#",
      "#2........#.#",
      "#.######.##.#",
      "#.15.......7#",
      "#4.#.#...####",
      "#..#.#...#...",
      "#..#.#...#...",
      "#.6#.#...#...",
      "#..#.#...#...",
      "###......#...",
      "..########..."
    ],
    "molecule": [
      ".2..",
      "13..",
      "..67",
      "15..",
      ".4.."
    ]
  },
  {
    "id": "73",
    "name": "Pyruvic Acid",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "D"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["3", "cg"],
      "6": ["1", "g"],
      "7": ["2", "ceA"],
      "8": ["2", "aeB"],
      "9": ["3", "C"]
    },
    "arena" : [
      "###############",
      "#...........62#",
      "###...#....7###",
      "..#...#....#..#",
      "..#...#....#..#",
      "..#.###....#..#",
      "..#..1#....#..#",
      "..#####.......#",
      "..#89#........#",
      "..#..#........#",
      "###..#........#",
      "#5#.......##..#",
      "#43.......6####",
      "###########...."
    ],
    "molecule": [
      ".9..",
      ".756",
      ".82.",
      "136.",
      ".4.."
    ]
  },
  {
    "id": "74",
    "name": "Ethylene oxide",
    "atoms": {
      "1": ["2", "cdfh"],
      "2": ["2", "bdfg"],
      "3": ["A", ""],
      "4": ["3", "bh"],
      "5": ["1", "h"],
      "6": ["1", "b"],
      "7": ["1", "f"],
      "8": ["1", "d"]
    },
    "arena" : [
      "##############",
      "#..4#..#....6#",
      "#.........#..#",
      "#..#5........#",
      "#3..7#.#.....#",
      "##...##...#..#",
      "##...#.#..#..#",
      "#............#",
      "#............#",
      "#.#..........#",
      "#.2.8#..#1...#",
      "##############"
    ],
    "molecule": [
      "8...7",
      ".132.",
      "6.4.5"
    ]
  },
  {
    "id": "75",
    "name": "Phosphoric Acid",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "cg"],
      "3": ["1", "g"],
      "4": ["9", "cegA"],
      "5": ["1", "a"],
      "6": ["3", "C"],
      "7": ["3", "ae"]
    },
    "arena" : [
      "###############",
      "#.#...........#",
      "#........#.#..#",
      "#.........#...#",
      "#.#......#.#..#",
      "#6#...........#",
      "###...........#",
      "#......#.#....#",
      "#.......#.....#",
      "#......#.#...2#",
      "##31..........#",
      "#.....7.#...25#",
      "#....##.##..###",
      "#..##.###....4#",
      "#####....######"
    ],
    "molecule": [
      "..6..",
      "12423",
      "..7..",
      "..5.."
    ]
  },
  {
    "id": "76",
    "name": "Diacetyl",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "h"],
      "3": ["1", "f"],
      "4": ["1", "b"],
      "5": ["3", "D"],
      "6": ["2", "ehB"],
      "7": ["2", "afB"],
      "8": ["2", "bdfh"]
    },
    "arena" : [
      "#######........",
      "#5...5########.",
      "#.....#.....3#.",
      "#.....#......#.",
      "#.....#......#.",
      "#............#.",
      "#.........####.",
      "#....4....718.#",
      "######.......4#",
      "..#8..........#",
      "..#.......#...#",
      "..#.......#...#",
      "..#16....2#...#",
      "..#########...#",
      "..........#####"
    ],
    "molecule": [
      "1.3.",
      ".8..",
      "4.65",
      "1.75",
      ".8..",
      "4.2."
    ]
  },
  {
    "id": "77",
    "name": "trans-Dichloroethene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "h"],
      "3": ["2", "fhB"],
      "4": ["2", "bdD"],
      "5": ["7", "f"],
      "6": ["7", "b"]
    },
    "arena" : [
      "#############",
      "#..6#.#.....#",
      "#..4##...#..#",
      "#...#...3#..#",
      "#.....#..####",
      "#........#...",
      "#........#...",
      "#...#....#...",
      "#...#....#...",
      "#...#....#...",
      "#...#.#.5#...",
      "#...#..#2#...",
      "#..1#...##...",
      "##########..."
    ],
    "molecule": [
      "1..5",
      ".34.",
      "6..2"
    ]
  },
  {
    "id": "78",
    "name": "Allylisothiocyanate",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "h"],
      "3": ["1", "b"],
      "4": ["2", "fhB"],
      "5": ["2", "bdD"],
      "6": ["4", "fB"],
      "7": ["2", "BD"],
      "8": ["5", "D"]
    },
    "arena" : [
      "###############",
      "#58.4#.......6#",
      "#..#####......#",
      "#...........#.#",
      "#......###..#.#",
      "#...#.......###",
      "#...#.......#.#",
      "#.#.#.....#.#.#",
      "#.#.......#...#",
      "###.......#...#",
      "#.#..###......#",
      "#.#...........#",
      "#......#####..#",
      "#......23#1.7.#",
      "###############"
    ],
    "molecule": [
      "1..678",
      ".45...",
      "3..2.."
    ]
  },
  {
    "id": "79",
    "name": "Diketene",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "f"],
      "3": ["2", "aceg"],
      "4": ["1", "a"],
      "5": ["1", "g"],
      "6": ["2", "acC"],
      "7": ["3", "ce"],
      "8": ["3", "A"],
      "9": ["2", "egA"],
      "a": ["2", "bhC"]
    },
    "arena" : [
      "###############",
      "##49........6##",
      "#..1........a.#",
      "#..#.#...#.#..#",
      "#...#.....#...#",
      "#5.#.#...#.#..#",
      "#8............#",
      "##...........##",
      "#7............#",
      "#..#.#...#.#..#",
      "#...#.....#...#",
      "#..#.#...#.#..#",
      "#3............#",
      "##.2.........##",
      "###############"
    ],
    "molecule": [
      "1.2",
      ".a.",
      "79.",
      "635",
      "84."
    ]
  },
  {
    "id": "80",
    "name": "Acroleine",
    "atoms": {
      "1": ["1", "f"],
      "2": ["1", "h"],
      "3": ["1", "b"],
      "4": ["2", "dfA"],
      "5": ["2", "bhC"],
      "6": ["2", "bdD"],
      "7": ["3", "B"]
    },
    "arena" : [
      "###############",
      "#.....1#1.....#",
      "#.....###.....#",
      "#.............#",
      "#.............#",
      "#7#.........#2#",
      "###.........###",
      "###.........###",
      "#6#.........#3#",
      "#.............#",
      "#.............#",
      "#.....###.....#",
      "#.....5#4.....#",
      "###############"
    ],
    "molecule": [
      "..1.",
      "76.1",
      "..5.",
      "..4.",
      ".3.2"
    ]
  },
  {
    "id": "81",
    "name": "Malonic Acid",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "g"],
      "3": ["1", "b"],
      "4": ["3", "A"],
      "5": ["3", "C"],
      "6": ["3", "cg"],
      "7": ["2", "chC"],
      "8": ["2", "bdfh"],
      "9": ["2", "cfA"]
    },
    "arena" : [
      "###############",
      "##584.......2##",
      "#2#.........#6#",
      "#............9#",
      "#.............#",
      "#.............#",
      "#.....#.#.....#",
      "#......#......#",
      "#.....#.#.....#",
      "#.............#",
      "#.............#",
      "#.............#",
      "#7#.........#1#",
      "##.........63##",
      "###############"
    ],
    "molecule": [
      "..5..",
      "1.962",
      ".8...",
      "3.762",
      "..4.."
    ]
  },
  {
    "id": "82",
    "name": "Uracil",
    "atoms": {
      "1": ["1", "a"],
      "2": ["3", "D"],
      "3": ["1", "b"],
      "4": ["2", "dfA"],
      "5": ["2", "bhC"],
      "6": ["4", "ceh"],
      "7": ["2", "afB"],
      "8": ["3", "C"],
      "9": ["1", "g"],
      "a": ["1", "d"],
      "b": ["4", "beh"]
    },
    "arena" : [
      "###############",
      "#3..........48#",
      "#1..........4.#",
      "#.#..#....#...#",
      "#.#..#........#",
      "#.#..#.#......#",
      "#..##2....#...#",
      "#.............#",
      "#....#79...#..#",
      "#..#.#6###....#",
      "#....###.#....#",
      "#.#...........#",
      "#...#.....#.a5#",
      "#............b#",
      "###############"
    ],
    "molecule": [
      "..8..",
      "a.4..",
      ".5.69",
      ".4.72",
      "3.b..",
      "..1.."
    ]
  },
  {
    "id": "83",
    "name": "Caffeine",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "e"],
      "3": ["1", "f"],
      "4": ["2", "abeh"],
      "5": ["1", "c"],
      "6": ["2", "adgh"],
      "7": ["3", "C"],
      "8": ["4", "adf"],
      "9": ["4", "ceh"],
      "a": ["2", "Acg"],
      "b": ["2", "bCg"],
      "c": ["2", "cCh"],
      "d": ["1", "g"],
      "e": ["3", "B"],
      "f": ["2", "acD"],
      "g": ["4", "ceg"],
      "h": ["A", ""],
      "i": ["4", "Ag"],
      "j": ["2", "adef"],
      "k": ["1", "b"],
      "l": ["1", "a"],
      "m": ["1", "h"]
    },
    "arena" : [
      "###############",
      "#1#.....#...3d#",
      "#2.........####",
      "#......#.....4#",
      "#e#...........#",
      "#.#.#.a#.#....#",
      "###...#...#...#",
      "#5.#..........#",
      "#..9...#...#..#",
      "#.6.#...lm....#",
      "####.......j..#",
      "#12#.#kc.#....#",
      "#ab..#..#######",
      "#hi.....78..fg#",
      "###############"
    ],
    "molecule": [
      "....123.",
      "12...4..",
      "56.7.8..",
      "..9ab.cd",
      ".efgahi.",
      "...j....",
      "..klm..."
    ]
  }
]}
//</levelSet>
;
  var top;
  if (typeof(exports) !== 'undefined') {
    top = exports;
  } else {
    if (!this.KP_ATOMIX) this.KP_ATOMIX = {};
    top = this.KP_ATOMIX;
  }
    top.levelSets = top.levelSets || {}
    top.levelSets[levelSet.name] = levelSet;
}());

(function () {

  var levelSet =
//<levelSet>
{
  "name": "mystery",
  "levels": [
  {
    "id": "1",
    "name": "Adrien 1",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "cg"],
      "3": ["1", "g"]
    },
    "arena": [
      "################",
      "###..........###",
      "##............##",
      "#....#2........#",
      "#...####.......#",
      "#...#........###",
      "##..#.........##",
      "###3#1........##",
      "################"
    ],
    "molecule": [
      "123"
    ]
  },
  {
    "id": "2",
    "name": "Adrien 2",
    "atoms": {
      "1": ["1", "d"],
      "2": ["4", "fhc"],
      "3": ["3", "cg"],
      "4": ["1", "g"],
      "5": ["1", "b"]
    },
    "arena": [
      "################",
      "#........#.....#",
      "#.5.#..2.#.4...#",
      "#........#.....#",
      "#......1.####..#",
      "######.........#",
      "#.........####.#",
      "#.3............#",
      "################"
    ],
    "molecule": [
      "1...",
      ".234",
      "5..."
    ]
  },
  {
    "id": "3",
    "name": "Adrien 3",
    "atoms": {
      "1": ["1", "e"],
      "2": ["1", "c"],
      "3": ["3", "cg"],
      "4": ["2", "aceg"],
      "5": ["1", "g"],
      "6": ["1", "a"]
    },
    "arena": [
      ".#.#.#.#.#.#.#.#",
      "#.#.#.#.#.#1#.#.",
      ".#.............#",
      "#.....6.......#.",
      ".#..3..##.4....#",
      "#.............#.",
      ".#.5.........2.#",
      "#.#.#.#.#.#.#.#.",
      ".#.#.#.#.#.#.#.#"
    ],
    "molecule": [
      "..1.",
      "2345",
      "..6."
    ]
  },
  {
    "id": "4",
    "name": "Adrien 4",
    "atoms": {
      "1": ["7", "d"],
      "2": ["1", "e"],
      "3": ["4", "cfh"],
      "4": ["2", "aceg"],
      "5": ["1", "g"],
      "6": ["1", "b"],
      "7": ["1", "a"]
    },
    "arena": [
      "##################",
      "##..#............#",
      "##....#2...##..#.#",
      "#4........1.#....#",
      "###...........4..#",
      "#........#..#..#7#",
      "##....7.##.5.....#",
      "##...........#.#.#",
      "#.6.....##.#..3..#",
      "##...#...2...#...#",
      "##################"
    ],
    "molecule": [
      "1.22.",
      ".3445",
      "6.77."
    ]
  },
  {
    "id": "5",
    "name": "Adrien 5",
    "atoms": {
      "1": ["1", "e"],
      "2": ["1", "c"],
      "3": ["2", "aceg"],
      "4": ["4", "ceg"],
      "5": ["1", "g"],
      "6": ["1", "a"]
    },
    "arena": [
      "##################",
      "##..............##",
      "#5.........4.....#",
      "#..#..........#..#",
      "#....6..##......1#",
      "##...#.####.#.3.##",
      "#.......##..6....#",
      "#..#1..3......#.6#",
      "##........2.....##",
      "##################"
    ],
    "molecule": [
      ".11..",
      "23345",
      ".666."
    ]
  },
  {
    "id": "6",
    "name": "Adrien 6",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "f"],
      "3": ["2", "Bfh"],
      "4": ["2", "bdD"],
      "5": ["1", "b"],
      "6": ["1", "h"]
    },
    "arena": [
      "################",
      "#.......6......#",
      "#.#####.....3..#",
      "#..1...........#",
      "#.....#####..5.#",
      "#.........4....#",
      "#.2.#....#####.#",
      "#...#..........#",
      "################"
    ],
    "molecule": [
      "1..2",
      ".34.",
      "5..6"
    ]
  },
  {
    "id": "7",
    "name": "Adrien 7",
    "atoms": {
      "1": ["1", "d"],
      "2": ["3", "cf"],
      "3": ["1", "g"],
      "4": ["3", "Bfh"],
      "5": ["3", "bdD"],
      "6": ["1", "b"],
      "7": ["1", "h"]
    },
    "arena": [
      "##################",
      "#...#...2....#...#",
      "#.#1....##.....#.#",
      "#................#",
      "##.....#3.#...5.##",
      "#.....#....#.....#",
      "##.....#..#.....##",
      "#...4............#",
      "#.#.....##.....#7#",
      "#...#......6.#...#",
      "##################"
    ],
    "molecule": [
      "1..23",
      ".45..",
      "6..7."
    ]
  },
  {
    "id": "8",
    "name": "Adrien 8",
    "atoms": {
      "1": ["1", "e"],
      "2": ["1", "d"],
      "3": ["3", "ae"],
      "4": ["4", "cfh"],
      "5": ["3", "cg"],
      "6": ["4", "aeg"],
      "7": ["1", "b"],
      "8": ["1", "a"]
    },
    "arena": [
      "##################",
      "####.........3####",
      "##..............##",
      "#..##2.......##..#",
      "#..4###....###.1.#",
      "#.......##.......#",
      "#......8##6......#",
      "#..#....##....#..#",
      "#..5#........#...#",
      "#....########7...#",
      "##################"
    ],
    "molecule": [
      "...1",
      "2..3",
      ".456",
      "7..8"
    ]
  },
  {
    "id": "9",
    "name": "Adrien 9",
    "atoms": {
      "1": ["1", "d"],
      "2": ["7", "f"],
      "3": ["2", "bCh"],
      "4": ["2", "Adf"],
      "5": ["3", "be"],
      "6": ["1", "h"],
      "7": ["1", "a"]
    },
    "arena": [
      "##################",
      "#.#..............#",
      "#............#2#.#",
      "#.....7.......#.##",
      "#.........#..4...#",
      "#..........#.....#",
      "#....#..1.#......#",
      "#..3......#....5.#",
      "#........6.......#",
      "#.........#......#",
      "##################"
    ],
    "molecule": [
      "1.2",
      ".3.",
      ".4.",
      "5.6",
      "7.."
    ]
  },
  {
    "id": "10",
    "name": "Adrien 10",
    "atoms": {
      "1": ["1", "e"],
      "2": ["1", "c"],
      "3": ["3", "cg"],
      "4": ["2", "aceg"],
      "5": ["1", "g"],
      "6": ["1", "a"]
    },
    "arena": [
      "################",
      "###............#",
      "#1#.#4#..#1#.#3#",
      "#.#.###..###6#.#",
      "#..............#",
      "#.#.###..###.#.#",
      "#2#.#.#..#5#.#4#",
      "#...........6..#",
      "################"
    ],
    "molecule": [
      "..11.",
      "23445",
      "..66."
    ]
  },
  {
    "id": "11",
    "name": "Adrien 11",
    "atoms": {
      "1": ["1", "f"],
      "2": ["3", "B"],
      "3": ["2", "bdD"],
      "4": ["1", "h"]
    },
    "arena": [
      "##################",
      "#................#",
      "#.#.........4..#.#",
      "#....3...........#",
      "#......####......#",
      "#..........#..2..#",
      "#......###.#.....#",
      "#.....#..........#",
      "#.........1....#.#",
      "#................#",
      "##################"
    ],
    "molecule": [
      "..1",
      "23.",
      "..4"
    ]
  },
  {
    "id": "12",
    "name": "Marbles 1",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "########",
      "#.5..#.#",
      "###....#",
      "#..6.#6#",
      "####.#.#",
      "...#.#.#",
      "...#.5.#",
      "...#####"
    ],
    "molecule": [
      "56",
      "65"
    ]
  },
  {
    "id": "13",
    "name": "Marbles 2",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "##########",
      "#..1..8..#",
      "#.##..##.#",
      "#.#....#.#",
      "#...8....#",
      "#..####..#",
      "#..#..#..#",
      "#9.#..#.1#",
      "####..####"
    ],
    "molecule": [
      "..1.",
      "1898"
    ]
  },
  {
    "id": "14",
    "name": "Marbles 3",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      ".#########.",
      ".#......4#.",
      ".#...##.###",
      ".#..4#....#",
      "###.##..#.#",
      "#.......#.#",
      "#6.....##.#",
      "########6.#",
      ".......####"
    ],
    "molecule": [
      ".4.",
      "6.6",
      ".4."
    ]
  },
  {
    "id": "15",
    "name": "Marbles 4",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "##########",
      "#.......1#",
      "#......###",
      "##.#..##3#",
      "#..#...#.#",
      "#.3#.....#",
      "#.....#..#",
      "#.#..##..#",
      "#.#..#...#",
      "##########"
    ],
    "molecule": [
      "313"
    ]
  },
  {
    "id": "16",
    "name": "Marbles 5",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "#########",
      "#2#.#7..#",
      "#...##..#",
      "###.#..##",
      "#.......#",
      "#8#.#.###",
      "###...#2#",
      "#7......#",
      "#########"
    ],
    "molecule": [
      ".2.",
      "787",
      ".2."
    ]
  },
  {
    "id": "17",
    "name": "Marbles 6",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "#####.....",
      "#.8.#.....",
      "#.#.#.....",
      "#.#.#.....",
      "#.#.#.....",
      "#...######",
      "###......#",
      "#9..####7#",
      "###......#",
      "#......###",
      "#7#.....9#",
      "##########"
    ],
    "molecule": [
      "7.9",
      ".8.",
      "9.7"
    ]
  },
  {
    "id": "18",
    "name": "Marbles 7",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "##########",
      "#5......2#",
      "###.#4#..#",
      "#3#.#.#..#",
      "#........#",
      "#.#.#.#..#",
      "#.#5..#..#",
      "#.###.#..#",
      "#.....3..#",
      "##########"
    ],
    "molecule": [
      "..5.",
      "2343",
      "..5."
    ]
  },
  {
    "id": "19",
    "name": "Marbles 8",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      ".####.",
      "##..##",
      "#3113#",
      "#2112#",
      "#2112#",
      "#3113#",
      "##..##",
      ".####."
    ],
    "molecule": [
      "2112",
      "1331",
      "1331",
      "2112"
    ]
  },
  {
    "id": "20",
    "name": "Marbles 9",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "....#########",
      "....#.....9.#",
      "....#.#####7#",
      "....#.......#",
      "###.#..#..#.#",
      "#7###.7#..#.#",
      "#.....#...#.#",
      "#..........8#",
      "#.####8######",
      "#.#..#.#.....",
      "#7####.#.....",
      "#.9....#.....",
      "########....."
    ],
    "molecule": [
      "..99..",
      "77..77",
      "..88.."
    ]
  },
  {
    "id": "21",
    "name": "Marbles 10",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "############",
      "#7.....#..9#",
      "#.#.#1.#..##",
      "#.#........#",
      "#.#....#..##",
      "#9....7#..8#",
      "############"
    ],
    "molecule": [
      "8...",
      "7997",
      "..1."
    ]
  },
  {
    "id": "22",
    "name": "Marbles 11",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "########",
      "#..1...#",
      "#.###.##",
      "#..2...#",
      "#.###.##",
      "#..1...#",
      "#...#..#",
      "########"
    ],
    "molecule": [
      "..1",
      ".2.",
      "1.."
    ]
  },
  {
    "id": "23",
    "name": "Marbles 12",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "....###....",
      "....#3#....",
      "....#.#....",
      "#####.#####",
      "#.........#",
      "#8#######8#",
      "#.#.....#.#",
      "#9#######9#",
      "#....3....#",
      "###########"
    ],
    "molecule": [
      ".9.",
      ".8.",
      ".8.",
      "393"
    ]
  },
  {
    "id": "24",
    "name": "Marbles 13",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "######.......",
      "#5...#.......",
      "#.##5#.......",
      "#.1#.########",
      "#..#.#.....1#",
      "#..5...#5..##",
      "#.##.#......#",
      "#.1#.#......#",
      "#...5#5...#5#",
      "########.1#.#",
      ".......#.##.#",
      ".......#..5.#",
      ".......######"
    ],
    "molecule": [
      ".5.5.",
      "5.1.5",
      ".1.1.",
      "5.1.5",
      ".5.5."
    ]
  },
  {
    "id": "25",
    "name": "Marbles 14",
    "atoms": {
      "1": ["F", ""],
      "2": ["G", ""],
      "3": ["H", ""],
      "4": ["I", ""],
      "5": ["J", ""],
      "6": ["K", ""],
      "7": ["L", ""],
      "8": ["M", ""],
      "9": ["E", ""]
    },
    "arena": [
      "########",
      "#122221#",
      "#31..13#",
      "#121121#",
      "#121121#",
      "#31..13#",
      "#122221#",
      "########"
    ],
    "molecule": [
      ".1111.",
      "122221",
      "123321",
      "123321",
      "122221",
      ".1111."
    ]
  },
  {
    "id": "26",
    "name": "Unitopia 1",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "cg"],
      "3": ["1", "g"]
    },
    "arena": [
      "################",
      "#..#.........#1#",
      "#..#.........#.#",
      "#.3#...........#",
      "#..#.#..########",
      "#....#.........#",
      "#....#....2....#",
      "################"
    ],
    "molecule": [
      "123"
    ]
  },
  {
    "id": "27",
    "name": "Unitopia 2",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "f"],
      "3": ["1", "a"],
      "4": ["4", "beh"]
    },
    "arena": [
      "################",
      "#..1#.....#...2#",
      "#.###..#..#..#.#",
      "#...#..........#",
      "#...###.######.#",
      "###..3#.....4#.#",
      "#..............#",
      "################"
    ],
    "molecule": [
      "1.2",
      ".4.",
      ".3."
    ]
  },
  {
    "id": "28",
    "name": "Unitopia 3",
    "atoms": {
      "1": ["1", "e"],
      "2": ["1", "c"],
      "3": ["2", "aceg"],
      "4": ["1", "g"],
      "5": ["1", "a"]
    },
    "arena": [
      "################",
      "#.......###...##",
      "#5.......#....##",
      "####......4##2.#",
      "#..1......####.#",
      "####....####.3.#",
      "#######........#",
      "################"
    ],
    "molecule": [
      ".1.",
      "234",
      ".5."
    ]
  },
  {
    "id": "29",
    "name": "Unitopia 4",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "dg"],
      "3": ["2", "Bfh"],
      "4": ["3", "D"],
      "5": ["3", "bg"]
    },
    "arena": [
      "################",
      "#1.#........##.#",
      "##.#..####....2#",
      "#..#....##..####",
      "#.......1##...##",
      "#..####...#...##",
      "#5......3.#...4#",
      "################"
    ],
    "molecule": [
      "12..",
      "..34",
      "15.."
    ]
  },
  {
    "id": "30",
    "name": "Unitopia 5",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "f"],
      "3": ["2", "Bfh"],
      "4": ["2", "bdD"],
      "5": ["1", "b"],
      "6": ["1", "h"]
    },
    "arena": [
      "################",
      "#.4#........1..#",
      "#.......2#.##..#",
      "######...#....##",
      "#........#.....#",
      "#.##.....5...#.#",
      "#.3#..........6#",
      "################"
    ],
    "molecule": [
      "1..2",
      ".34.",
      "5..6"
    ]
  },
  {
    "id": "31",
    "name": "Unitopia 6",
    "atoms": {
      "1": ["1", "e"],
      "2": ["1", "c"],
      "3": ["2", "aceg"],
      "4": ["3", "gc"],
      "5": ["1", "g"],
      "6": ["1", "a"]
    },
    "arena": [
      "################",
      "#.4.........#2.#",
      "#####....#..##.#",
      "##.1....6#1....#",
      "##.....###...6.#",
      "##...5......####",
      "######...3...3.#",
      "################"
    ],
    "molecule": [
      ".11..",
      "23345",
      ".66.."
    ]
  },
  {
    "id": "32",
    "name": "Unitopia 7",
    "atoms": {
      "1": ["1", "e"],
      "2": ["1", "f"],
      "3": ["2", "aBe"],
      "4": ["2", "bdD"],
      "5": ["1", "a"],
      "6": ["2", "Bfh"],
      "7": ["1", "b"],
      "8": ["1", "h"]
    },
    "arena": [
      "################",
      "#...6#...#2..###",
      "#........#...#4#",
      "#............#.#",
      "#7.#..#####4.2.#",
      "####..#3.....###",
      "#1...8##...5####",
      "################"
    ],
    "molecule": [
      "1.2..",
      "34..2",
      "5.64.",
      ".7..8"
    ]
  },
  {
    "id": "33",
    "name": "Unitopia 8",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "dg"],
      "3": ["3", "B"],
      "4": ["5", "BdDh"],
      "5": ["3", "D"],
      "6": ["3", "ch"],
      "7": ["1", "g"]
    },
    "arena": [
      "################",
      "#....1....#....#",
      "######.....2.###",
      "#........###75.#",
      "##4#.#6....#.#.#",
      "####.#.....#...#",
      "####.#....3....#",
      "################"
    ],
    "molecule": [
      "12...",
      ".345.",
      "...67"
    ]
  },
  {
    "id": "34",
    "name": "Unitopia 9",
    "atoms": {
      "1": ["1", "e"],
      "2": ["1", "c"],
      "3": ["2", "aceg"],
      "4": ["1", "g"],
      "5": ["1", "a"],
      "6": ["3", "ae"]
    },
    "arena": [
      "################",
      "##.....#3......#",
      "#5.1...#####..6#",
      "##.......5.#.###",
      "#3.......#4#..1#",
      "######...###3..#",
      "###2...#1.5....#",
      "################"
    ],
    "molecule": [
      ".111.",
      "23334",
      ".565.",
      "..5.."
    ]
  },
  {
    "id": "35",
    "name": "Unitopia 10",
    "atoms": {
      "1": ["1", "e"],
      "2": ["1", "c"],
      "3": ["2", "aceg"],
      "4": ["2", "Bdg"],
      "5": ["3", "D"],
      "6": ["1", "a"],
      "7": ["3", "ch"],
      "8": ["1", "g"]
    },
    "arena": [
      "################",
      "#.....####1..#3#",
      "#.....#4.##..#.#",
      "#2.......#6....#",
      "####.###7#..####",
      "#..#.#...#...#.#",
      "#5...#.......8##",
      "################"
    ],
    "molecule": [
      ".1...",
      "2345.",
      ".6.78"
    ]
  },
  {
    "id": "36",
    "name": "Kai - Wasser",
    "atoms": {
      "1": ["3", "bd"],
      "2": ["1", "f"],
      "3": ["1", "h"]
    },
    "arena": [
      "...#######",
      ".###.....#",
      ".#...###.#",
      "##....#..#",
      "#...#..2.#",
      "#.###.####",
      "#3......1#",
      "####.....#",
      "...####..#",
      "......####"
    ],
    "molecule": [
      ".2",
      "1.",
      ".3"
    ]
  },
  {
    "id": "37",
    "name": "Kai - Methan",
    "atoms": {
      "1": ["2", "aceg"],
      "2": ["1", "a"],
      "3": ["1", "c"],
      "4": ["1", "e"],
      "5": ["1", "g"]
    },
    "arena": [
      "######.....",
      "#..1.#####.",
      "#.##...#3#.",
      "#..#...#.##",
      "#..##..#..#",
      "##.......##",
      "#..#..#..5#",
      "##4#....###",
      ".#.###....#",
      ".#.....####",
      ".#..#.2#...",
      ".#######..."
    ],
    "molecule": [
      ".4.",
      "315",
      ".2."
    ]
  },
  {
    "id": "38",
    "name": "Kai - Methanol",
    "atoms": {
      "1": ["2", "aceg"],
      "2": ["3", "cg"],
      "3": ["1", "c"],
      "4": ["1", "e"],
      "5": ["1", "g"],
      "6": ["1", "a"]
    },
    "arena": [
      "####.........",
      "#..#.........",
      "#..#.........",
      "#.4#.........",
      "#..########..",
      "#...#...#.###",
      "#.#.....#...#",
      "#.......#1..#",
      "#.3###2.#.#.#",
      "#...........#",
      "#....6.#5.#.#",
      "######....#.#",
      ".....########"
    ],
    "molecule": [
      ".4..",
      "3125",
      ".6.."
    ]
  },
  {
    "id": "39",
    "name": "Kai - Ethan",
    "atoms": {
      "1": ["2", "aceg"],
      "2": ["1", "c"],
      "3": ["1", "g"],
      "4": ["1", "a"],
      "5": ["1", "e"]
    },
    "arena": [
      "#########..",
      "#..1#...###",
      "#.###...4.#",
      "#.4...#.#.#",
      "#.###.#.#.#",
      "#.....#.2.#",
      "#5........#",
      "#..#......#",
      "##.#..###.#",
      "#.1#..5.#.#",
      "#.....#..3#",
      "###########"
    ],
    "molecule": [
      ".55.",
      "2113",
      ".44."
    ]
  },
  {
    "id": "40",
    "name": "Kai - Ammoniak",
    "atoms": {
      "1": ["4", "bdg"],
      "2": ["1", "c"],
      "3": ["1", "f"],
      "4": ["1", "h"]
    },
    "arena": [
      "..########.",
      "###....4.##",
      "#...####..#",
      "#....#....#",
      "#.#.......#",
      "#3.....##.#",
      "#...#..#2.#",
      "#...#..#..#",
      "#...#.....#",
      "##......###",
      ".#1..##.#..",
      ".#......#..",
      ".########.."
    ],
    "molecule": [
      "..3",
      "21.",
      "..4"
    ]
  },
  {
    "id": "41",
    "name": "Kai - cis-Buten-2",
    "atoms": {
      "1": ["2", "hbdf"],
      "2": ["2", "hBf"],
      "3": ["2", "bdD"],
      "4": ["1", "b"],
      "5": ["1", "f"],
      "6": ["1", "d"],
      "7": ["1", "h"]
    },
    "arena": [
      "..###########",
      "..#...7.....#",
      "..#.###.2##4#",
      "###.1.#.....#",
      "#.....#.##..#",
      "#5##.6#.#..6#",
      "#.#...#..7..#",
      "#...........#",
      "#.###.##.##.#",
      "#...6..7....#",
      "#.1....#..3.#",
      "#############"
    ],
    "molecule": [
      "...6.5",
      ".6..1.",
      "6.23.7",
      ".1..7.",
      "4.7..."
    ]
  },
  {
    "id": "42",
    "name": "Kai - trans-Buten-2",
    "atoms": {
      "1": ["2", "hbdf"],
      "2": ["2", "bdD"],
      "3": ["2", "hBf"],
      "4": ["1", "d"],
      "5": ["1", "f"],
      "6": ["1", "b"],
      "7": ["1", "h"]
    },
    "arena": [
      "....#######..",
      "....#5..#.###",
      ".####..7....#",
      ".#....3.#..6#",
      ".#.#..#..4..#",
      ".#..7.#1..###",
      ".#.####.....#",
      "##6.....#...#",
      "#...##1.#...#",
      "#.#.....5.###",
      "#4#2.######..",
      "#...##.......",
      "#####........"
    ],
    "molecule": [
      ".4..5.",
      "4.32.5",
      ".1..1.",
      "6.76.7"
    ]
  },
  {
    "id": "43",
    "name": "Kai - Propionaldehyd",
    "atoms": {
      "1": ["2", "bdg"],
      "2": ["1", "c"],
      "3": ["1", "h"],
      "4": ["3", "f"],
      "5": ["2", "aceg"],
      "6": ["1", "e"],
      "7": ["1", "a"]
    },
    "arena": [
      ".........######",
      "..########..#.#",
      "..#.2#....1...#",
      "..#.##.#.##.#.#",
      "..#.#.5.....#3#",
      "###......7....#",
      "#.6.##..#..##.#",
      "#.#.#...#..#..#",
      "###.#.####.6.##",
      "#4.......7.#.#.",
      "#####.5#.....#.",
      "....##########."
    ],
    "molecule": [
      ".66.4",
      "2551.",
      ".77.3"
    ]
  },
  {
    "id": "44",
    "name": "Kai - Dichlormethan",
    "atoms": {
      "1": ["2", "aceg"],
      "2": ["7", "a"],
      "3": ["7", "e"],
      "4": ["1", "c"],
      "5": ["1", "g"]
    },
    "arena": [
      "..####.....",
      "..#..#####.",
      "###.4....#.",
      "#.1.#..#5#.",
      "#.#......##",
      "#......#..#",
      "##.......##",
      ".#..#.##..#",
      ".##2....3.#",
      ".#..#####.#",
      ".####...#.#",
      "........###"
    ],
    "molecule": [
      ".3.",
      "415",
      ".2."
    ]
  },
  {
    "id": "45",
    "name": "Kai - tertiär-Butanol",
    "atoms": {
      "1": ["2", "aceg"],
      "2": ["2", "habe"],
      "3": ["1", "e"],
      "4": ["1", "a"],
      "5": ["3", "ae"],
      "6": ["1", "g"],
      "7": ["1", "c"],
      "8": ["1", "f"],
      "9": ["1", "d"]
    },
    "arena": [
      "........#####",
      "....#####...#",
      "#####..2.6#.#",
      "#7..#.#...#.#",
      "#..3..#.#.1.#",
      "#.......#...#",
      "#.9.......5.#",
      "#.....#.4...#",
      "#.##4.#....3#",
      "#........##.#",
      "##..#3.1.#1.#",
      "#...#....#.##",
      "###.8..#.4.#.",
      "..##########."
    ],
    "molecule": [
      ".938.",
      ".323.",
      "71116",
      ".454.",
      "..4.."
    ]
  },
  {
    "id": "46",
    "name": "Kai - Nicotinsäure",
    "atoms": {
      "1": ["1", "g"],
      "2": ["2", "Adf"],
      "3": ["2", "adf"],
      "4": ["2", "hbC"],
      "5": ["4", "hb"],
      "6": ["3", "C"],
      "7": ["3", "hc"],
      "8": ["1", "h"],
      "9": ["1", "e"],
      "a": ["1", "d"],
      "b": ["1", "b"]
    },
    "arena": [
      "......########",
      "..#####b.#.3.#",
      "..#2.#...7...#",
      "###..#.#.##.##",
      "#...........#.",
      "#a#.2.#..8..#.",
      "#.##...6.#..#.",
      "#.#......#.2##",
      "#.5..#####...#",
      "#.##.#...##.1#",
      "#..4.#...#9..#",
      "#4.###...#####",
      "####.........."
    ],
    "molecule": [
      "..9.6..",
      "a.3.2..",
      ".4.4.71",
      ".2.2...",
      "b.5.8.."
    ]
  },
  {
    "id": "47",
    "name": "Kai - Harnstoff",
    "atoms": {
      "1": ["3", "B"],
      "2": ["2", "bdD"],
      "3": ["4", "hce"],
      "4": ["4", "acf"],
      "5": ["1", "e"],
      "6": ["1", "g"],
      "7": ["1", "a"]
    },
    "arena": [
      "......#######",
      "#######..1..#",
      "#.......##..#",
      "#.##.6...#.##",
      "#.#.....7..#.",
      "#....###...#.",
      "#..6......##.",
      "#..#..4....#.",
      "#.3#....#..#.",
      "#..##..##.##.",
      "##.......5#..",
      ".#..2.#####..",
      ".######......"
    ],
    "molecule": [
      "..5.",
      "..46",
      "12..",
      "..36",
      "..7."
    ]
  },
  {
    "id": "48",
    "name": "Kai - Benzol",
    "atoms": {
      "1": ["2", "a3f"],
      "2": ["2", "hbC"],
      "3": ["2", "Adf"],
      "4": ["2", "h2e"],
      "5": ["2", "ad4"],
      "6": ["2", "1be"],
      "7": ["1", "e"],
      "8": ["1", "d"],
      "9": ["1", "b"],
      "a": ["1", "a"],
      "b": ["1", "h"],
      "c": ["1", "f"]
    },
    "arena": [
      ".....########.",
      ".###.#.6.9..#.",
      ".#.###..###.#.",
      ".#.....3..#.#.",
      ".#.a......5.##",
      ".###.b#.#7...#",
      ".#....#...#..#",
      ".#4.......#..#",
      "##..#1....#.##",
      "#...#.....#.#.",
      "#.8.##c##...#.",
      "#....#....2.#.",
      "#############."
    ],
    "molecule": [
      "..7..",
      "8.1.c",
      ".2.6.",
      ".3.5.",
      "9.4.b",
      "..a.."
    ]
  },
  {
    "id": "49",
    "name": "Kai - Kohlensäuredichlorid",
    "atoms": {
      "1": ["3", "D"],
      "2": ["2", "hBf"],
      "3": ["7", "d"],
      "4": ["7", "b"]
    },
    "arena": [
      "#######......",
      "#..#..#######",
      "#..#...#.#..#",
      "#.4#..2#.#3.#",
      "#.##..##.##.#",
      "#...........#",
      "#...........#",
      "####........#",
      "...####.....#",
      "...#.1#.....#",
      "...#......###",
      "...########.."
    ],
    "molecule": [
      "3..",
      ".21",
      "4.."
    ]
  },
  {
    "id": "50",
    "name": "Kai - Tartronsäure",
    "atoms": {
      "1": ["2", "aceg"],
      "2": ["3", "cg"],
      "3": ["1", "g"],
      "4": ["1", "c"],
      "5": ["2", "1be"],
      "6": ["2", "ad4"],
      "7": ["3", "3"],
      "8": ["3", "2"],
      "9": ["3", "hc"],
      "a": ["3", "cf"]
    },
    "arena": [
      "#########.....",
      "#..#....######",
      "#.3##.#8...1.#",
      "#.##2.#..#.#.#",
      "#.#.....##...#",
      "#....3....9.##",
      "##..####...##.",
      "#......#.#..#.",
      "#.3.....4#.a#.",
      "##.#...#...##.",
      ".#....7...5#..",
      ".#6##..#####..",
      ".#.....#......",
      ".#######......"
    ],
    "molecule": [
      "7.a3",
      ".5..",
      "4123",
      ".6..",
      "8.93"
    ]
  },
  {
    "id": "51",
    "name": "Kai - 2-Amino-ethansulfonsäure",
    "atoms": {
      "1": ["5", "h2B3f"],
      "2": ["2", "hbdf"],
      "3": ["4", "beg"],
      "4": ["1", "d"],
      "5": ["3", "D"],
      "6": ["3", "4"],
      "7": ["3", "1"],
      "8": ["1", "h"],
      "9": ["1", "a"],
      "a": ["1", "c"]
    },
    "arena": [
      "#####..#######.",
      "#...####.....#.",
      "#........###.#.",
      "#.#4#.##a###.#.",
      "#.4.4.1#.9...##",
      "#.#.#.##.#.#..#",
      "#.8.8.....2...#",
      "#.#.#...6#.#..#",
      "#.....##......#",
      "#####.3#5#.#..#",
      "....#.##..2..##",
      "....#...7#.#.#.",
      "....####.....#.",
      ".......#.....#.",
      ".......#######."
    ],
    "molecule": [
      "...4.6",
      "..4.15",
      ".4.2.7",
      "..2.8.",
      "a3.8..",
      ".9...."
    ]
  },
  {
    "id": "52",
    "name": "Kai - Milchsäure",
    "atoms": {
      "1": ["2", "aceg"],
      "2": ["2", "1be"],
      "3": ["3", "cg"],
      "4": ["3", "cf"],
      "5": ["3", "3"],
      "6": ["1", "g"],
      "7": ["1", "c"],
      "8": ["1", "a"]
    },
    "arena": [
      "...##########",
      "...#.......3#",
      "####5.##7.#.#",
      "#.....#..#..#",
      "#.##....#.6.#",
      "#.1..6....#.#",
      "#.##..#..##.#",
      "#.7..#.6....#",
      "#.#.#.....4.#",
      "#.#...#.#.###",
      "#.2##8#1#.#..",
      "#.........#..",
      "###########.."
    ],
    "molecule": [
      "5.46",
      ".2..",
      "7136",
      "716.",
      ".8.."
    ]
  },
  {
    "id": "53",
    "name": "Kai - Glycerin",
    "atoms": {
      "1": ["2", "aceg"],
      "2": ["3", "cg"],
      "3": ["1", "c"],
      "4": ["1", "g"],
      "5": ["1", "a"],
      "6": ["1", "e"]
    },
    "arena": [
      "######.....###.",
      "#..2.#######.#.",
      "#.##.......3.#.",
      "#6...3##1#...#.",
      "#####..4...#.##",
      "....#.2..#.#.4#",
      "..###.##......#",
      "..#2#.4..5...##",
      "..#...#......#.",
      "..#####1.#...#.",
      "......#..#.#1#.",
      "......##...###.",
      "......#.3..#...",
      "......######..."
    ],
    "molecule": [
      ".6..",
      "3124",
      "3124",
      "3124",
      ".5.."
    ]
  },
  {
    "id": "54",
    "name": "Kai - Essigsäureethylester",
    "atoms": {
      "1": ["2", "aceg"],
      "2": ["2", "2dg"],
      "3": ["3", "hc"],
      "4": ["3", "4"],
      "5": ["1", "e"],
      "6": ["1", "a"],
      "7": ["1", "c"],
      "8": ["1", "g"]
    },
    "arena": [
      "#######........",
      "#.....#...####.",
      "#.##..#...#..#.",
      "#..#5.#####.4#.",
      "#.6#..8.1....#.",
      "#..###....#..#.",
      "#2......####.#.",
      "#.#5..1...7..##",
      "#....#..#.....#",
      "###.3...5..#.6#",
      "..####.......##",
      "....#..#.#.#.#.",
      "....#.1..6...#.",
      "....##########."
    ],
    "molecule": [
      ".5.4...",
      "712.55.",
      ".6.3118",
      "....66."
    ]
  },
  {
    "id": "55",
    "name": "Kai - Leucopterin",
    "atoms": {
      "1": ["2", "Adf"],
      "2": ["1", "d"],
      "3": ["2", "a3f"],
      "4": ["1", "c"],
      "5": ["1", "a"],
      "6": ["2", "hbC"],
      "7": ["2", "h2e"],
      "8": ["4", "hbe"],
      "9": ["4", "beg"],
      "a": ["4", "1b"],
      "b": ["4", "adf"],
      "c": ["3", "1"],
      "d": ["3", "4"],
      "e": ["3", "C"],
      "f": ["1", "e"]
    },
    "arena": [
      "......######...",
      "......#....####",
      "#######.#1...3#",
      "#....#....#...#",
      "#c...#.#.d#.8.#",
      "#..#9.a1.#..#.#",
      "#..#.#...#3...#",
      "##....2..8.#..#",
      "##.7#.#.f..#.##",
      "#.....#.##4..#.",
      "#.e....5.#...#.",
      "##..##.....#5#.",
      "#.b.#..6.#.#.#.",
      "########.....#.",
      ".......#######."
    ],
    "molecule": [
      "...e.f..",
      ".2.1.b.d",
      "..8.6.7.",
      "..3.1.3.",
      "49.a.8.c",
      ".5...5.."
    ]
  },
  {
    "id": "56",
    "name": "Kai - Guanin",
    "atoms": {
      "1": ["2", "b3f"],
      "2": ["3", "1"],
      "3": ["2", "1be"],
      "4": ["2", "a3f"],
      "5": ["2", "Adf"],
      "6": ["4", "hcf"],
      "7": ["4", "Af"],
      "8": ["2", "bCg"],
      "9": ["4", "hbe"],
      "a": ["4", "bC"],
      "b": ["1", "d"],
      "c": ["1", "f"],
      "d": ["4", "beg"],
      "e": ["1", "c"],
      "f": ["1", "a"]
    },
    "arena": [
      "########.......",
      "#...#..#######.",
      "#.f...7....4.#.",
      "##.##..#####.#.",
      ".#.#.e..a.#..#.",
      ".#.#.##....c.##",
      ".#.#.....2...d#",
      ".#..5.##...#..#",
      ".##.#.#...##..#",
      "..#...#.#.8...#",
      "..#1...3.....6#",
      "..#..#...#.#..#",
      "..##.##.##.#.##",
      "...#.9..b..f.#.",
      "...###########."
    ],
    "molecule": [
      "...b..c",
      "....68.",
      "...1.7.",
      "..a.3..",
      "..5.4..",
      "ed.9.2.",
      ".f.f..."
    ]
  },
  {
    "id": "57",
    "name": "Kai - Acetylsalicylsäure",
    "atoms": {
      "1": ["2", "a3f"],
      "2": ["1", "e"],
      "3": ["2", "hbC"],
      "4": ["1", "g"],
      "5": ["2", "Adf"],
      "6": ["2", "h2e"],
      "7": ["1", "d"],
      "8": ["2", "ad4"],
      "9": ["1", "b"],
      "a": ["2", "1be"],
      "b": ["1", "a"],
      "c": ["3", "C"],
      "d": ["3", "hc"],
      "e": ["3", "hd"],
      "f": ["3", "2"],
      "g": ["2", "hc4"],
      "h": ["2", "aceg"]
    },
    "arena": [
      "....#####......",
      "..###f#g###....",
      "..#b.....5#....",
      ".###.2#.#.#....",
      "##.6.....a#....",
      "#b...#d...#....",
      "#.##...########",
      "#.#.2....h..#.#",
      "#4..#.#.7..c..#",
      "#....8.....#.4#",
      "###.##.##5.#.##",
      "..#1..3....9..#",
      "..####..e#.####",
      ".....#######..."
    ],
    "molecule": [
      "..2.c...",
      "7.1.5...",
      ".3.a.d4.",
      ".5.8....",
      "9.6.e.2.",
      "..b..gh4",
      "....f.b."
    ]
  }
]}

//</levelSet>
;
  var top;
  if (typeof(exports) !== 'undefined') {
    top = exports;
  } else {
    if (!this.KP_ATOMIX) this.KP_ATOMIX = {};
    top = this.KP_ATOMIX;
  }
    top.levelSets = top.levelSets || {}
    top.levelSets[levelSet.name] = levelSet;
}());
(function () {

  var levelSet =
//<levelSet>
{
  "name": "draknek",
  "credits": "<a href='http://www.draknek.org/'>Alan Hazelden</a>",
  "license": "<a href='http://www.wtfpl.net/txt/copying/'>WTFPL</a>",
  "levels": [
  {
    "name": "Formaldehyde",
    "id": "1",
    "atoms": {
      "1": ["3", "B"],
      "2": ["2", "Dbd"],
      "3": ["1", "f"],
      "4": ["1", "h"]
    },
    "arena": [
      "...####",
      "...#.3#",
      "####..#",
      "#..2.1#",
      "####..#",
      "...#.4#",
      "...####"
    ],
    "molecule": [
      "..3",
      "12.",
      "..4"
    ]
  },
  {
    "name": "Hyponitrous acid",
    "id": "2",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "gd"],
      "3": ["4", "hB"],
      "4": ["4", "Dd"],
      "5": ["3", "hc"],
      "6": ["1", "g"]
    },
    "arena": [
      "########",
      "#3....1#",
      "#..##..#",
      "#..52..#",
      "#..##..#",
      "#6....4#",
      "########"
    ],
    "molecule": [
      "12....",
      "..34..",
      "....56"
    ]
  },
  {
    "name": "Carbonic acid",
    "id": "3",
    "atoms": {
      "1": ["3", "C"],
      "2": ["1", "d"],
      "3": ["2", "Adf"],
      "4": ["1", "f"],
      "5": ["3", "bh"]
    },
    "arena": [
      "#######",
      "#2...4#",
      "#..3..#",
      "#..1..#",
      "#5...5#",
      "#######"
    ],
    "molecule": [
      "..1..",
      "2.3.4",
      ".5.5."
    ]
  },
  {
    "name": "Methanol",
    "id": "4",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "e"],
      "3": ["3", "hc"],
      "4": ["2", "aceg"],
      "5": ["1", "g"],
      "6": ["1", "a"]
    },
    "arena": [
      "#######",
      "#..2..#",
      "#..3..#",
      "#.5#1.#",
      "#..4..#",
      "#..6..#",
      "#######"
    ],
    "molecule": [
      "1.2.",
      ".345",
      "..6."
    ]
  },
  {
    "name": "Ethylene",
    "id": "5",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "f"],
      "3": ["2", "Bfh"],
      "4": ["2", "Ddb"],
      "5": ["1", "b"],
      "6": ["1", "h"]
    },
    "arena": [
      "#######",
      "#1...2#",
      "#..3..#",
      "#..4..#",
      "#5...6#",
      "#######"
    ],
    "molecule": [
      "1..2",
      ".34.",
      "5..6"
    ]
  },
  {
    "name": "Hydrogen Peroxide",
    "id": "6",
    "atoms": {
      "1": ["1", "d"],
      "2": ["3", "hc"],
      "3": ["3", "gd"],
      "4": ["1", "h"]
    },
    "arena": [
      "#######",
      "#1...2#",
      "#.....#",
      "#3...4#",
      "#######"
    ],
    "molecule": [
      "1...",
      ".23.",
      "...4"
    ]
  },
  {
    "name": "Carbon Dioxide",
    "id": "7",
    "atoms": {
      "1": ["3", "B"],
      "2": ["2", "BD"],
      "3": ["3", "D"]
    },
    "arena": [
      "..###..",
      "..#.#..",
      "###.###",
      "#.321.#",
      "#######"
    ],
    "molecule": [
      "123"
    ]
  },
  {
    "name": "Ammonia",
    "id": "8",
    "atoms": {
      "1": ["1", "e"],
      "2": ["4", "adf"],
      "3": ["1", "b"],
      "4": ["1", "h"]
    },
    "arena": [
      "..###..",
      ".##1##.",
      "##...##",
      "#4...3#",
      "##...##",
      ".##2##.",
      "..###.."
    ],
    "molecule": [
      ".1.",
      ".2.",
      "3.4"
    ]
  },
  {
    "name": "Methane",
    "id": "9",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "f"],
      "3": ["2", "bdfh"],
      "4": ["1", "b"],
      "5": ["1", "h"]
    },
    "arena": [
      "#######",
      "#1...2#",
      "#.....#",
      "#.#3#.#",
      "#..#..#",
      "#4...5#",
      "#######"
    ],
    "molecule": [
      "1.2",
      ".3.",
      "4.5"
    ]
  },
  {
    "name": "Hydroxylamine",
    "id": "10",
    "atoms": {
      "1": ["1", "e"],
      "2": ["3", "ae"],
      "3": ["4", "adf"],
      "4": ["1", "b"],
      "5": ["1", "h"]
    },
    "arena": [
      "#######",
      "#..#..#",
      "#..1..#",
      "#..3..#",
      "#4#2#5#",
      "#######"
    ],
    "molecule": [
      ".1.",
      ".2.",
      ".3.",
      "4.5"
    ]
  },
  {
    "name": "Water",
    "id": "11",
    "atoms": {
      "1": ["1", "c"],
      "2": ["3", "gd"],
      "3": ["1", "h"]
    },
    "arena": [
      "#######",
      "#1..###",
      "#....##",
      "#....##",
      "#2..#3#",
      "#######"
    ],
    "molecule": [
      "12.",
      "..3"
    ]
  },
  {
    "name": "Water 2",
    "id": "12",
    "atoms": {
      "1": ["1", "d"],
      "2": ["3", "hd"],
      "3": ["1", "h"]
    },
    "arena": [
      "#######",
      "##1..##",
      "#.....#",
      "#..2..#",
      "#....3#",
      "##...##",
      "#######"
    ],
    "molecule": [
      "1..",
      ".2.",
      "..3"
    ]
  },
  {
    "name": "Propadiene",
    "id": "13",
    "atoms": {
      "1": ["1", "e"],
      "2": ["2", "aeB"],
      "3": ["2", "BD"],
      "4": ["2", "Dae"],
      "5": ["1", "a"]
    },
    "arena": [
      "#########",
      "#.1.#.1.#",
      "#...#...#",
      "#.4.3.2.#",
      "#...#...#",
      "#.5.#.5.#",
      "#########"
    ],
    "molecule": [
      "1.1",
      "234",
      "5.5"
    ]
  },
  {
    "name": "Methane 2",
    "id": "14",
    "atoms": {
      "1": ["1", "d"],
      "2": ["1", "f"],
      "3": ["2", "bdfh"],
      "4": ["1", "b"],
      "5": ["1", "h"]
    },
    "arena": [
      "#######",
      "#1#.#2#",
      "#.....#",
      "#..3..#",
      "#.....#",
      "#4#.#5#",
      "#######"
    ],
    "molecule": [
      "1.2",
      ".3.",
      "4.5"
    ]
  }
]}
//</levelSet>
;
  if (typeof(exports) !== 'undefined'){
      exports.levelSet = levelSet;
  }

  if (typeof(KP_ATOMIX) === 'undefined') {
    KP_ATOMIX = {
        levelSets: {}
    }
  }
  KP_ATOMIX.levelSets[levelSet.name] = levelSet;
}());
