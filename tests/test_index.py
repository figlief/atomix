from __future__ import unicode_literals

from stuff import (
    try_ok,
)


class Test_Index(object):
    def test_index(self):
        url = "/"
        yield try_ok, url
