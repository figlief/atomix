
from __future__ import unicode_literals
from stuff import (
    kinds,
    try_ok,
)


class Test_Recent(object):

    def test_recent(self):
        for trial in kinds:
            url = "/recent.%s" % trial
            yield try_ok, url
