from __future__ import unicode_literals

from stuff import (
    try_ok,
)


class test_action_submit():

    def test_ok(self):

        form = dict(
            user='*test*',
            levelSet="katomic",
            level="1",
            history="cccbcbfbfbfefebebebjfhfgfgbgbgbihbhfhfgfgfgjgjfjfjfgfgbgbgbh"
        )
        url = '/submit-solution'
        yield try_ok, url, form
