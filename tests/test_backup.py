from __future__ import unicode_literals

from stuff import (
    try_ok,
)


class Test_Backup(object):

    def test_backup(self):
        url = "/backup.csv"
        yield try_ok, url
