from __future__ import unicode_literals

from stuff import (
    kinds,
    try_ok,
)


class Test_Users(object):
    def test_users(self):
        for trial in kinds:
            url = "/users.%s" % trial
            yield try_ok, url
