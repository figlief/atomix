from nose.tools import (
    assert_equal,
)

from kp_atomix.atomix import (
    Atomix,
    Ctx,
)

from os.path import (
    abspath,
    join,
    split,
)


def bRead(path):
    with open(path, 'rb') as fp:
        return fp.read()


def uRead(path):
    return unicode(bRead(path))


site_root = split(abspath(join(__file__, '..')))[0]

levels_dir = join(site_root, 'levels')
tests_dir = join(site_root, 'tests')


def null_log(*args, **kw):
    pass

config = Ctx(
    browserTitle='figlief',
    dbName=site_root + '/data/atomix.sqlite',
    link_prefix='/atomix',
    log=null_log,
    siteURL='http://figlief.pythonanywhere.com',
    site_root=site_root,
    version='0.0.0'
)
app = Atomix(config)

kinds = (
    'html',
    'json',
    'txt',
    'csv',
    'xml',
)

levelSetNames = (
    'draknek',
    'katomic',
    'original',
    'mystery',
    'pack1',
)


def rtuple(i):
    return tuple(i for i in xrange(1, i + 1))

levelSets = (
    ('draknek', rtuple(14)),
    ('katomic', rtuple(83)),
    ('mystery', rtuple(57)),
    ('original', rtuple(30)),
    ('pack1', rtuple(25)),
)

levelSet_kind = (
    (levelSet, kind)
    for kind in kinds
    for levelSet in levelSetNames
)


def try_ok(url, form={}):
    page = app.main(url, form)
    page.compose()
    assert_equal(page.httpStatus, 200)


def try_404(url):
    page = app.main(url, {})
    page.compose()
    assert_equal(page.httpStatus, 404)
