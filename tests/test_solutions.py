from __future__ import unicode_literals

from stuff import (
    levelSet_kind,
    try_404,
    try_ok,
)


class Test_Solutions(object):

    def test_solutions(self):

        yield try_404, "/solutions/junk.html"
        yield try_404, "/solutions/katomic.xxx"
        for trial in levelSet_kind:
            url = "/solutions/%s.%s" % trial
            yield try_ok, url
