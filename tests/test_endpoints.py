from __future__ import unicode_literals

from stuff import (
    levelSets,
    try_ok,
)


class Test_Endpoints(object):

    def __init__(self):
        trials = []
        for levelSet, levs in levelSets:
            for level in levs:
                trials.append((levelSet, level))
        self.trials = tuple(trials)

    def print_trials(self):
        for i in self.trials:
            print i

    def test_endpoints(self):
        for trial in self.trials:
            url = "/endpoints/%s/%s.html" % trial
            yield try_ok, url
