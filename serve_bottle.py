#!/usr/bin/python

import bottle
from bottle_app import app

bottle.run(app=app, port=5088)
