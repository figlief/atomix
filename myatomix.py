import sys
import os
import random


def join(*args):
    return os.path.join(*args)


site_root = os.path.split(os.path.abspath(__file__))[0]

# sys.path.insert(0, site_root + '/cgi')

from kp_atomix.atomix import Atomix, Ctx


def null_log(*args, **kw):
    pass
log = null_log

if 1:
    import logging
    logging.basicConfig(
        level=logging.DEBUG,
        filename=site_root + '/data/myatomix.log',
        filemode='w'
    )
    logging.info('==============================')
    logging.info('site_root: ' + site_root)

    logging.info('python: %s', sys.version_info)

    log = logging.debug


config = Ctx(
    browserTitle='figlief',
    dbName=site_root + '/data/atomix.sqlite',
    link_prefix='/atomix',
    log=log,
    siteURL='http://figlief.pythonanywhere.com',
    site_root=site_root,
    version=random.randint(10000, 99999),
)
atomix = Atomix(config)

log('Version: %s', atomix.version)
