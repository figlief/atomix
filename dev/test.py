#! /usr/bin/python
import sys
import random

import atomixlib

from os.path import (
    abspath,
    join,
    split,
)

site_root = split(abspath(join(__file__, '..')))[0]


sys.path.insert(0, site_root + '/cgi')
import atomix


def null_log(*args, **kw):
    pass
log = null_log

if 1:
    import logging
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(levelname)s %(message)s',
        #    filename=site_root + '/data/myatomix.log',
        #    filemode='w'
    )
    logging.info('==============================')
    logging.info('site_root test: ' + site_root)

    logging.info('python test: %s', sys.version_info)

    log = logging.debug


def myatomix():
    aa = atomix.Atomix(
        log=log,
        site_root=site_root
    )

    aa.site_title_prefix = 'kp-atomix'

    aa.pageCount = 0

    aa.maxUserLength = 32
    aa.maxHistoryLength = 4000

    if 1:
        aa.dbName = site_root + '/data/atomix.sqlite'
        aa.siteURL = 'http://figlief.pythonanywhere.com'
        aa.browserTitle = 'figlief'
        aa.useCombined = 1
        aa.useCompressed = 0
        aa.debugCode = ''

    aa.allowedLevelSets = ('katomic', 'original', 'pack1', 'mystery')

    # <cache.breaker>
    aa.version = random.randint(10000, 99999)

    log('Version-test: %s', aa.version)

    return aa

app = myatomix()


levelSets = app.allowedLevelSets
kinds = ['html', 'json', 'xml', 'csv', 'txt']

baseDir = atomixlib.baseDir

log = app.log


def app_for_anywhere(app):
    app.siteURL = 'http://figlief.pythonanywhere.com'
    app.link_prefix = ''
    return app


def app_for_ledger(app):
    app.siteURL = 'http:/ledger.sdf.org/atomix'
    app.link_prefix = ''
    return app


def app_for_static(app):
    app.siteURL = 'http:/ledger.sdf.org/atomix'
    app.link_prefix = '..'
    app.headbarTemplate = atomix.alt_headbarTemplate
    app.footerTemplate = atomix.alt_footerTemplate
    app.indexFile = 'index.html'
    app.cssFolder = ''
    # atomix.pageTemplatae =
    return app


def save_solution(app, url=''):
    from urlparse import parse_qs

    qs = 'user=TEST&levelSet=katomic&level=1&history=cccbcbfbfbfefebebebjfhfgfgbgbgbihbhfhfgfgfgjgjfjfjfgfgbgbgbh'
    url = app.siteURL + url

    form = parse_qs(qs)
    for k in form:
        form[k] = form[k][0]

    page = app.main('/submit-solution', form=form)
    page.compose()
    return page


def error_404(app, files=False, brk=True):
    page = app.main('/recent/cm91dGU=.html', form={})
    page.compose()
    return page


def recent_page(app, files=False, brk=True):
    page = app.main('/recent.xml', form={'limit': 5})
    page.compose()
    return page


def users_page(app, files=False, brk=True):
    page = app.main('/users.xml', form={})
    page.compose()
    return page


def write_file(path, fileName, txt):
    dirPath = baseDir + path
    atomixlib.make_dirs(dirPath)
    with open(dirPath + fileName, 'wb') as fp:
            fp.write(txt)


def solution_files(app, files=False, brk=True):
    for levelSet in levelSets:
        for kind in kinds:
            page = app.main('/solutions/%s.%s' % (levelSet, kind))
            log('%s.%s, %s', levelSet, kind, page)

            page.compose()

            if files:
                write_file(
                    '/dev/output/solutions/%s/' % (levelSet),
                    '%s.%s' % (levelSet, kind),
                    page.contents
                )
            if brk:
                break
        if brk:
            break
    return page


def index_page(app, files=False, brk=True):
    page = app.main('/')
    page.compose()
    return page


def endpoint_files(app, files=False, brk=True):
    levelSets = atomixlib.get_levelSets()
    for levelSet, levels in levelSets.iteritems():
        levels = levels.levels

        for level in levels:

            page = app.main('/endpoints/%s/%s.html' % (levelSet, level.id))
            page.compose()
            if files:
                write_file(
                    '/dev/output/endpoints/%s/' % (levelSet),
                    '/%s.html' % (level.id),
                    page.contents
                )
            if brk:
                break
        if brk:
            break


def apage(app, url, files=False, brk=True):
    page = app.main(url)
    page.compose()
    return page


def xtest():
    app_for_anywhere(app)

    solution_files(app, files=True, brk=True)
    users_page(app, 1.1)
    recent_page(app, 1.1)
    index_page(app, 1.1)
    error_404(app, 1.1)


def test():
    app_for_anywhere(app)
    # page = apage(app,'/endpoints/katomic/1.html')
    page = recent_page(app)
    print page.contents

if __name__ == "__main__":
    xtest()
