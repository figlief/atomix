import atomixlib as my

levelSets = my.get_levelSets()

dups = set()
unique = []
for item in my.get_solutions():
    if item.history in dups:
        continue
    else:
        dups.add(item.history)
        unique.append(item)

print len(dups)
