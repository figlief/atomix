import os
import sys

import requests


def log(*args):
    print args


def nolog(*args):
    pass

local_url = 'http://localhost:5088/atomix/backup.csv'
live_url = 'http://figlief.pythonanywhere.com/backup.csv'


base = os.path.split(os.path.abspath(__file__))[0]


fn = os.path.join(base, 'solutions.txt')
log('write to', fn)


def _prep(r):
    items = r.content.rstrip()
    items = items.split('\r\n')
    nItems = len(items) - 1
    log('nItems:', nItems)
    return items


def _join(items):
    return '\r\n'.join(items) + '\r\n'


def get_full_solutions(url, limit=0):
    log('full backup')
    r = fetch_solutions(url, '', limit)
    r.raise_for_status()
    items = _prep(r)
    text = _join(items)
    with open(fn, 'wb') as fp:
        fp.write(text)

    return


def fetch_solutions(url, last_id='', limit=0):

    params = {}
    if last_id:
        params['start'] = last_id
    if limit:
        params['limit'] = limit

    r = requests.get(url, params=params)
    r.raise_for_status()
    return r


def _last_id(items):
    # log('in _last_id', items[-1])
    return items[-1].split(',')[0]


def get_last_id():

    with open(fn, 'rb') as fp:
        items = fp.read()
    items = items.rstrip().split('\n')
    if len(items):
        last_id = _last_id(items)
        log('last_id:', last_id)
    else:
        last_id = ''
        log('solutions file is empty')

    return last_id


def update_solutions(url, limit=0):

    log('updating solutions')

    last_id = get_last_id()
    r = fetch_solutions(url, last_id, limit)
    # log(r.content)
    items = _prep(r)[1:]

    if not items:
        log('no new items')
        return
    log('new last_id', _last_id(items))
    text = _join(items)
    with open(fn, 'ab') as fp:
        fp.write(text)


if __name__ == "__main__":
    args = sys.argv
    url = live_url
    if '-q' in args:
        log = nolog
    if 'local' in args:
        url = local_url
    if 'full' in args:
        get_full_solutions(url)
    else:
        update_solutions(url)
