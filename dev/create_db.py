from __future__ import unicode_literals

import sys
import os
import json

import atomixlib as my

from ctx import Ctx


DB_PATH = my.join(my.baseDir, 'data', 'atomix.sqlite')

print 'db:', DB_PATH


def createNewDb(dbPath):

    if os.path.exists(dbPath):
        os.remove(dbPath)

    db = my.DB(dbPath)
    c = db.con.cursor()

    sql = """
        CREATE TABLE solutions (

            uid string,
            date string,
            levelSet string,
            level string,
            user string,
            nMoves integer,
            history string,
            prevBest string,
            nextBest string,
            endpoint string,
            prevEp string,
            nextEp string,

            PRIMARY KEY (uid)
        );
        CREATE TABLE levels (

            levelSet string,
            level string,
            order_ integer,
            name string,
            atoms sting,
            arena string,
            molecule string,
            endpoints string,

            PRIMARY KEY (levelSet, level)
        );

    """
    c.executescript(sql)
    db.commit()
    db.close()
    os.chmod(dbPath, 0766)


def create_levels_table(db, levelSets):

    c = db.c

    q = (
        "insert into levels values"
        + " (:levelSet, :level, :order_, :name, :atoms,"
        + " :arena, :molecule, :endpoints)")

    for levelSet in levelSets.itervalues():
        print 'CREATE', levelSet.name
        for i, level in enumerate(levelSet.levels):
            d = Ctx()
            d.levelSet = levelSet.name
            d.level = level.id
            d.order_ = i + 1
            d.name = level.name
            d.atoms = json.dumps(level.atoms)
            d.arena = '\n'.join(level.arena)
            d.molecule = '\n'.join(level.molecule)
            ep = my.get_endpoints_for_level(level)
            d.endpoints = '\n'.join(ep)
            c.execute(q, d)


def create_solutions_table(db, items):

    c = db.c

    q = (
        ":uid, :date, :levelSet, :level, :user, :nMoves, :history"
        + ", :prevBest, :nextBest, :endpoint, :prevEp, :nextEp")

    q = "insert into solutions values (%s)" % q

    def refuid(s):

        ss = i[s]
        try:
            i[s] = ss.uid
        except AttributeError:
            if ss in ['X', 'F', 'L']:
                i[s] = ss
            else:
                i[s] = ''

    for i in items:

        refuid('prevBest')
        refuid('nextBest')

        refuid('nextEp')
        refuid('prevEp')
        c.execute(q, i)


def main(dbPath):

    # full empty levels

    k = 'full'
    if len(sys.argv) > 1:
        k = sys.argv[1]
        if k != 'new':
            print "[%] is unknown. Use 'full' or 'new'"
            return

    print 'pwd', os.getcwd()
    print 'kind', k

    createNewDb(dbPath)

    levelSets = my.get_levelSets()
    db = None

    db = my.DB(dbPath)

    create_levels_table(db, levelSets)

    if k == 'full':
        items = list(my.get_solutions())

        my.thread_data(items, levelSets)

        create_solutions_table(db, items)

    if db:
        print 'closing db'
        db.commit()
        db.close()

bestOf = {}
if __name__ == "__main__":
    main(DB_PATH)
