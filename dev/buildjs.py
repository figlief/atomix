import atomixlib as a

JS = a.joinbase('js')
XJS = a.joinbase('xjs')

print JS
print XJS

atomix = a.readbase('js', 'atomix.js')
xlib = a.readbase('xjs', 'xlib.js')
game = a.readbase('js', 'game.js')
dialog = a.readbase('js', 'dialog.js')

text = '\n' + '\n'.join((dialog, game, xlib))
text = atomix.replace('//INCLUDE_XLIB', text)

a.write(
    a.joinbase('xjs', 'atomix.js'),
    text
)

for levelSet in ('pack1', 'original', 'katomic', 'mystery', 'draknek'):
    text += a.readbase('levels', levelSet + '.js')

a.write(
    a.joinbase('xjs', 'combined.js'),
    text
)
