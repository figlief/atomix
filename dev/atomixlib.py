from __future__ import unicode_literals

import re
import os
import csv
import json
import glob
import sqlite3

from ctx import Ctx


class LevelSet(object):

    def __init__(self, jsonLevelSet):

        for k in (
            "name",
            "credits",
            "license",
            "levels"
        ):
            setattr(self, k, jsonLevelSet[k])
        self.levels = [
            Level(lvl, order, self.name)
            for order, lvl in enumerate(self.levels)
        ]


class Level(object):

    def __init__(self, level, order, levelSetName):

        for k in (
            "id",
            "name",
            "atoms",
            "arena",
            "molecule",
        ):
            setattr(self, k, level[k])

        self.order = order
        self.levelSetName = levelSetName
        self.arena_mask = flood_fill(self.arena)

    def __str__(self):
        name = "%s %s: %s\n" % (
            self.levelSetName,
            self.id,
            self.name,
        )
        name += "\n".join(self.arena) + '\n'
        return name.encode("utf-8")


class Solution(Ctx):

    def __init__(self, solution):

        Ctx.__init__(self, solution)

        self.nMoves = len(self.history)//4

        self.prevBest = 'X'
        self.nextBest = 'X'
        self.prevEp = 'X'
        self.nextEp = 'X'

        self.endpoint = verify_solution(self)


def chunk(lst, n):
    i = 0
    while i < len(lst):
        yield lst[i:i+n]
        i += n


def read(fn):
    with open(fn, 'rb') as fp:
        txt = fp.read()
    return txt.decode('utf-8')


def readbase(*args):
    return read(joinbase(*args))


def write(fn, text):
    with open(fn, 'wb') as fp:
        fp.write(text.encode('utf-8'))


def join(*args):
    return os.path.join(*args)


def joinbase(*args):
    return join(baseDir, *args)


def make_dirs(path):
        dir = os.path.dirname(path)
        if not os.path.exists(dir):
                os.makedirs(dir)

baseDir = os.path.split(os.path.abspath(join(__file__, '..')))[0]
print ('baseDir:%s' % baseDir)

levelsDir = join(baseDir, 'levels')

fnSolutions = join(baseDir, 'dev', 'solutions.txt')

moveEncoder = list('abcdefghijklmnopqrstuvwxyz')
moveDecoder = {}

for i, c in enumerate(moveEncoder):
    moveDecoder[c] = i


class DB:

    def __init__(self, db):

        self.con = sqlite3.connect(db)
        self.con.row_factory = sqlite3.Row

        self.c = self.con.cursor()
        print 'DB new connection'

    def commit(self):
        print 'DB.commit'
        return self.con.commit()

    def close(self):
        print 'DB.close'
        return self.con.close()


def get_levelSets():
    return levelSets or load_levelSets()


def load_levelSets():
    global levelSets
    levelSets = {}
    for fn in glob.glob(join(baseDir, 'levels', '*.json')):
        js = json.loads(read(fn))
        name = js['name']
        levelSets[name] = LevelSet(js)
        for level in js['levels']:
            level['levelSetName'] = name

    return levelSets


def get_solutions():
    with open(fnSolutions, 'rb') as fp:
        reader = csv.DictReader(fp, dialect=csv.excel)
        for item in reader:
            item = Solution(item)
            item.user = item.user.decode('utf-8')
            yield item


def verify_solution(s):

    level = get_level(s.levelSet, s.level)

    def _assert(p, msg='Invalid'):
        if not (p):
            raise Exception('FAILED to veryify solution (%s).' % msg)

    arena = level.arena
    arenaWidth = len(arena[0])

    atoms = level.atoms.keys()
    molecule = level.molecule
    moleculeWidth = len(molecule[0])

    pad = '.' * (arenaWidth - moleculeWidth)
    molecule = pad.join(molecule)

    grid = [list(row) for row in arena]

    h = [moveDecoder[m] for m in s.history]

    for m in chunk(h, 4):

        startRow, startCol, endRow, endCol = m
        row, col = startRow, startCol

        atom = grid[startRow][startCol]
        _assert(atom in atoms)

        data = grid[row]

        if startRow == endRow:
            _assert(startCol != endCol)

            if startCol > endCol:
                while data[col - 1] == '.':
                    col -= 1
            else:
                while data[col + 1] == '.':
                    col += 1

        else:
            _assert(startCol == endCol)

            if startRow > endRow:
                while grid[row - 1][col] == '.':
                    row -= 1
            else:
                while grid[row + 1][col] == '.':
                    row += 1

        _assert(row == endRow and col == endCol)

        grid[startRow][startCol] = '.'
        grid[endRow][endCol] = atom

    grid = ''.join(''.join(row) for row in grid).replace('#', '.')

    endpoint = grid.find(molecule)
    _assert(endpoint >= 0, 'incomplete')

    row, col = endpoint // arenaWidth, endpoint % arenaWidth
    return moveEncoder[row] + moveEncoder[col]


def each_level():
    for name, levels in each_levelSet():
        for level in levels:
            yield level


def each_levelSet():
    for levelSet in get_levelSets().values():
        yield levelSet.name, levelSet.levels


def get_level(levelSet, level):
    return get_levelSets()[levelSet].levels[int(level) - 1]


def flood_fill(arena):

    def _flood(x, y):
        if x < aw and y < ah and x >= 0 and y >= 0:
            if a[y][x] in '#+':
                return
            a[y][x] = "+"
            _flood(x+1, y)
            _flood(x-1, y)
            _flood(x, y+1)
            _flood(x, y-1)

    a = [list(row) for row in arena]

    aw = len(a[0])
    ah = len(a)

    for r, row in enumerate(arena):
        for c, item in enumerate(row):
            if item in ".+#":
                continue
            _flood(c, r)

    return [''.join(row) for row in a]


def get_endpoints_for_level(level, raw=False):

    assert isinstance(level, Level)

    arenaWidth = len(level.arena[0])
    molWidth = len(level.molecule[0])

    arena = ''.join(level.arena_mask)
    arenaSize = len(arena)
    arena = re.sub('[^#.]', ' ', arena)

    pad = '.' * (arenaWidth - molWidth)

    mol = [
        re.sub('[^\.]', ' ', row)
        for row in level.molecule
    ]

    mol = (pad).join(mol)

    endpoints = []
    for i in range(arenaSize - len(mol)):
        if re.match(mol, arena[i:]):
            if raw:
                endpoints.append(i)
            else:
                r = i // arenaWidth
                c = i % arenaWidth
                endpoints.append(moveEncoder[r] + moveEncoder[c])

    return endpoints


def add_endpoints(levelSets):
    for level in each_level():
        level.endpoints = get_endpoints_for_level(level)

########################


def thread_item(s, levelSets, bestOf):

    k = s.levelSet + s.level
    best = bestOf.get(k, None)

    if best is None:
        s.prevBest = 'F'
        s.nextBest = 'L'
        bestOf[k] = s
    else:
        if best.nMoves > s.nMoves:
            best.nextBest = s
            s.prevBest = best
            s.nextBest = 'L'
            bestOf[k] = s

    k += s.endpoint
    bestEp = bestOf.get(k, None)

    if bestEp is None:
        s.prevEp = 'F'
        s.nextEp = 'L'
        bestOf[k] = s
    else:
        if bestEp.nMoves > s.nMoves:
            bestEp.nextEp = s
            s.prevEp = bestEp
            s.nextEp = 'L'
            bestOf[k] = s


def thread_data(solutions, levelSets, bestOf=None):
    if bestOf is None:
        bestOf = {}

    for s in solutions:
        thread_item(s, levelSets, bestOf)


def main():

    get_levelSets()
    maxWidth = 0
    maxHeight = 0
    maxAtoms = 0

    for name, levels in each_levelSet():
        for level in levels:
            height = len(level.arena)
            width = len(level.arena[0])

            maxWidth = max(maxWidth, width)
            maxHeight = max(maxHeight, height)
            maxAtoms = max(maxAtoms, len(level.atoms))

            if height * width > 225:
                print name, level.id

    print maxWidth, maxHeight
    print maxWidth * maxHeight, maxAtoms

    # get_best_ep()

levelSets = None
